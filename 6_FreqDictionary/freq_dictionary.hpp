#ifndef FreqDictionary_hpp
#define FreqDictionary_hpp

#include "node.hpp"
#include <utility>
#include <string>
#include <ostream>


class FreqDictionary {
public:
    FreqDictionary();
    FreqDictionary(const FreqDictionary&);
    FreqDictionary(FreqDictionary&&);
    ~FreqDictionary();
    
    void add(const std::string&);
    unsigned frequency(const std::string&) const noexcept;
    bool del(const std::string&);
    
    void clear();
    
    unsigned long long size() const noexcept;
    bool empty() const noexcept;
    
    FreqDictionary& operator =(const FreqDictionary&);
    FreqDictionary& operator =(FreqDictionary&&);
    
    friend std::ostream& operator <<(std::ostream&, const FreqDictionary&);

private:
    Node* root_;
    unsigned long long size_;
    
    void add(Node*&, const std::string&);
    const Node* find(const Node*, const std::string&) const noexcept;
    bool del(Node*&, const std::string&);
    Node* detachGreatest(Node* prev, Node* curr);
    Node* copy(const Node*);
    void del(Node*&);
};


#endif
