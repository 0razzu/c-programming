#include "freq_dictionary.hpp"


FreqDictionary::FreqDictionary() {
    root_ = nullptr;
    size_ = 0;
}


FreqDictionary::FreqDictionary(const FreqDictionary& o) {
    root_ = o.root_? FreqDictionary::copy(o.root_) : nullptr;
    size_ = o.size_;
}


FreqDictionary::FreqDictionary(FreqDictionary&& o) {
    root_ = o.root_;
    size_ = o.size_;
    
    o.root_ = nullptr;
    o.size_ = 0;
}


FreqDictionary::~FreqDictionary() {
    clear();
}


void FreqDictionary::add(const std::string& word) {
    add(root_, word);
    size_++;
}


unsigned FreqDictionary::frequency(const std::string& word) const noexcept {
    const Node* node = find(root_, word);
    
    if (node)
        return node->frequency_;
    
    return 0;
}


bool FreqDictionary::del(const std::string& word) {
    bool deleted = del(root_, word);
    
    if (deleted)
        size_--;
    
    return deleted;
}


void FreqDictionary::clear() {
    if (root_)
        del(root_);
    
    size_ = 0;
}


unsigned long long FreqDictionary::size() const noexcept {
    return size_;
}


bool FreqDictionary::empty() const noexcept {
    return !size_;
}


FreqDictionary& FreqDictionary::operator =(const FreqDictionary& o) {
    if (root_)
        del(root_);
    
    if (o.root_)
        root_ = FreqDictionary::copy(o.root_);
    
    size_ = o.size_;
    
    return *this;
}


FreqDictionary& FreqDictionary::operator =(FreqDictionary&& o) {
    std::swap(root_, o.root_);
    std::swap(size_, o.size_);
    
    return *this;
}


void FreqDictionary::add(Node*& node, const std::string& word) {
    if (!node)
        node = new Node(word);
    
    else if (word < node->word_)
        add(node->left_, word);
        
    else if (word > node->word_)
        add(node->right_, word);
        
    else
        node->frequency_++;
}


const Node* FreqDictionary::find(const Node* node, const std::string& word) const noexcept {
    if (!node)
        return node;
    
    else if (word < node->word_)
        return find(node->left_, word);
        
    else if (word > node->word_)
        return find(node->right_, word);
    
    return node;
}


bool FreqDictionary::del(Node*& node, const std::string& word) {
    if (!node)
        return 0;
    
    else if (word < node->word_)
        return del(node->left_, word);
        
    else if (word > node->word_)
        return del(node->right_, word);
        
    if (node->frequency_ == 1) {
        Node* newNode;
        
        if (!node->left_)
            newNode = node->right_;
        
        else if (!node->right_)
            newNode = node->left_;
        
        else {
            if (!node->left_->right_)
                newNode = node->left_;
            
            else {
                newNode = detachGreatest(node, node->left_);
                newNode->left_ = node->left_;
            }
                
            newNode->right_ = node->right_;
        }
            
        delete node;
        node = newNode;
    }
        
    else
        node->frequency_--;
        
    return 1;
}


Node* FreqDictionary::detachGreatest(Node* prev, Node* curr) {
    while (curr->right_) {
        prev = curr;
        curr = curr->right_;
    }
    
    Node* greatest = curr;
    
    prev->right_ = curr->left_;
    
    return greatest;
}


Node* FreqDictionary::copy(const Node* node) {
    Node* copy = new Node(node->word_, node->frequency_);
    
    if (node->left_)
        copy->left_ = FreqDictionary::copy(node->left_);
    
    if (node->right_)
        copy->right_ = FreqDictionary::copy(node->right_);
    
    return copy;
}


void FreqDictionary::del(Node*& node) {
    if (node->left_)
        del(node->left_);
    
    if (node->right_)
        del(node->right_);
    
    delete node;
    node = nullptr;
}


void out(std::ostream& os, const Node* node) {
    if (node->left_)
        out(os, node->left_);
    
    os << node->word_ << " (" << node->frequency_ << ")" << std::endl;
    
    if (node->right_)
        out(os, node->right_);
}


std::ostream& operator <<(std::ostream& os, const FreqDictionary& dict) {
    if (!dict.root_)
        os << std::endl;
    
    else
        out(os, dict.root_);
    
    return os;
}
