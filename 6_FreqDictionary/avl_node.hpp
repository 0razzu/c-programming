#ifndef node_hpp
#define node_hpp

#include <string>


struct AvlNode {
    std::string word_;
    unsigned frequency_;
    
    AvlNode* left_;
    AvlNode* right_;
    short balance_;
    
    
    AvlNode(const std::string& word, unsigned frequency) {
        word_ = word;
        frequency_ = frequency;
        left_ = nullptr;
        right_ = nullptr;
        balance_ = 0;
    }
    
    
    AvlNode(const std::string& word): AvlNode(word, 1) {
    }
};


#endif
