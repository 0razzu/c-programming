#ifndef test_freq_dictionary_hpp
#define test_freq_dictionary_hpp

#include "../freq_dictionary.hpp"
#include <cxxtest/TestSuite.h>
#include <string>
#include <utility>
#include <sstream>


class TestFreqDictionary: public CxxTest::TestSuite {
public:
    void testConstr() {
        FreqDictionary dict;
        
        TS_ASSERT_EQUALS(0, dict.size());
        TS_ASSERT(dict.empty());
    }
    
    
    void testCopyConstr() {
        FreqDictionary dict1;
        FreqDictionary dict2;
        
        dict1.add("a");    //   ┌—––––– a (1) ––––––┐
        dict1.add("A");    // A (1)           ┌—– ab (2) ––┐
        dict1.add("ab");   //               aa (3)       b (2) –┐
        dict1.add("aa");   //                                 c (1)
        dict1.add("b");
        dict1.add("b");
        dict1.add("ab");
        dict1.add("aa");
        dict1.add("aa");
        dict1.add("c");
        
        FreqDictionary dict3(dict1);
        FreqDictionary dict4(dict2);
        
        TS_ASSERT_EQUALS(dict1.size(), dict3.size());
        TS_ASSERT_EQUALS(dict1.frequency("a"), dict3.frequency("a"));
        TS_ASSERT_EQUALS(dict1.frequency("A"), dict3.frequency("A"));
        TS_ASSERT_EQUALS(dict1.frequency("ab"), dict3.frequency("ab"));
        TS_ASSERT_EQUALS(dict1.frequency("aa"), dict3.frequency("aa"));
        TS_ASSERT_EQUALS(dict1.frequency("b"), dict3.frequency("b"));
        TS_ASSERT_EQUALS(dict1.frequency("c"), dict3.frequency("c"));
        
        TS_ASSERT_EQUALS(dict2.size(), dict4.size());
    }
    
    
    void testMoveConstr() {
        FreqDictionary dict1;
        FreqDictionary dict2;
        
        dict1.add("a");    //   ┌—––––– a (1) ––––––┐
        dict1.add("A");    // A (1)           ┌—– ab (2) ––┐
        dict1.add("ab");   //               aa (3)       b (2) –┐
        dict1.add("aa");   //                                 c (1)
        dict1.add("b");
        dict1.add("b");
        dict1.add("ab");
        dict1.add("aa");
        dict1.add("aa");
        dict1.add("c");
        
        FreqDictionary dict3(std::move(dict1));
        FreqDictionary dict4(std::move(dict2));
        
        TS_ASSERT_EQUALS(10, dict3.size());
        TS_ASSERT_EQUALS(1, dict3.frequency("a"));
        TS_ASSERT_EQUALS(1, dict3.frequency("A"));
        TS_ASSERT_EQUALS(2, dict3.frequency("ab"));
        TS_ASSERT_EQUALS(3, dict3.frequency("aa"));
        TS_ASSERT_EQUALS(2, dict3.frequency("b"));
        TS_ASSERT_EQUALS(1, dict3.frequency("c"));
        
        TS_ASSERT(dict1.empty());
        
        TS_ASSERT(dict4.empty());
        TS_ASSERT(dict2.empty());
    }
    
    
    void testAdd() {
        FreqDictionary dict;
        
        dict.add("cat");    //     ┌—––––– cat (2) ––––––┐
        dict.add("dog");    // canary (1)             dog (2) –––––┐
        dict.add("cat");    //                            ┌—––– wolf (1) ––––┐
        dict.add("wolf");   //                         fox (1) –––┐      zebra (1)
        dict.add("fox");    //                                  ox (2)
        dict.add("dog");
        dict.add("canary");
        dict.add("ox");
        dict.add("zebra");
        dict.add("ox");
        
        TS_ASSERT_EQUALS(10, dict.size());
        TS_ASSERT_EQUALS(2, dict.frequency("cat"));
        TS_ASSERT_EQUALS(2, dict.frequency("dog"));
        TS_ASSERT_EQUALS(1, dict.frequency("wolf"));
        TS_ASSERT_EQUALS(1, dict.frequency("fox"));
        TS_ASSERT_EQUALS(1, dict.frequency("canary"));
        TS_ASSERT_EQUALS(2, dict.frequency("ox"));
        TS_ASSERT_EQUALS(1, dict.frequency("zebra"));
        TS_ASSERT_EQUALS(0, dict.frequency("pigeon"));
    }
    
    
    void testDel() {
        FreqDictionary dict;
        
        dict.add("cat");    //     ┌—––––– cat (2) ––––––┐
        dict.add("dog");    // canary (1)             dog (2) –––––┐
        dict.add("cat");    //                            ┌—––– wolf (1) ––––┐
        dict.add("wolf");   //                         fox (1) –––┐      zebra (1)
        dict.add("fox");    //                                  ox (2) ––┐
        dict.add("dog");    //                                   ┌—– pigeon (1)
        dict.add("canary"); //                               panda (1)
        dict.add("ox");
        dict.add("zebra");
        dict.add("ox");
        dict.add("pigeon");
        dict.add("panda");
        
        TS_ASSERT(dict.del("wolf"));
        
        TS_ASSERT_EQUALS(11, dict.size());
        TS_ASSERT_EQUALS(2, dict.frequency("dog"));
        TS_ASSERT_EQUALS(0, dict.frequency("wolf"));
        TS_ASSERT_EQUALS(1, dict.frequency("fox"));
        TS_ASSERT_EQUALS(2, dict.frequency("ox"));
        TS_ASSERT_EQUALS(1, dict.frequency("zebra"));
        TS_ASSERT_EQUALS(1, dict.frequency("pigeon"));
        TS_ASSERT_EQUALS(1, dict.frequency("panda"));
        
        TS_ASSERT(dict.del("panda"));
        
        TS_ASSERT_EQUALS(10, dict.size());
        TS_ASSERT_EQUALS(1, dict.frequency("fox"));
        TS_ASSERT_EQUALS(2, dict.frequency("ox"));
        TS_ASSERT_EQUALS(1, dict.frequency("zebra"));
        TS_ASSERT_EQUALS(1, dict.frequency("pigeon"));
        TS_ASSERT_EQUALS(0, dict.frequency("panda"));
        
        TS_ASSERT(dict.del("ox"));
        
        TS_ASSERT_EQUALS(9, dict.size());
        TS_ASSERT_EQUALS(1, dict.frequency("fox"));
        TS_ASSERT_EQUALS(1, dict.frequency("ox"));
        TS_ASSERT_EQUALS(1, dict.frequency("zebra"));
        TS_ASSERT_EQUALS(1, dict.frequency("pigeon"));
        TS_ASSERT_EQUALS(0, dict.frequency("panda"));
        
        TS_ASSERT(dict.del("cat"));
        TS_ASSERT(dict.del("cat"));
        TS_ASSERT(!dict.del("cat"));
        
        TS_ASSERT_EQUALS(7, dict.size());
        TS_ASSERT_EQUALS(0, dict.frequency("cat"));
        TS_ASSERT_EQUALS(2, dict.frequency("dog"));
        TS_ASSERT_EQUALS(1, dict.frequency("fox"));
        TS_ASSERT_EQUALS(1, dict.frequency("canary"));
        TS_ASSERT_EQUALS(1, dict.frequency("zebra"));
        TS_ASSERT_EQUALS(1, dict.frequency("ox"));
        TS_ASSERT_EQUALS(1, dict.frequency("pigeon"));
    }
    
    
    void testClear() {
        FreqDictionary dict1;
        FreqDictionary dict2;
        
        dict1.add("cat");
        dict1.add("dog");
        dict1.add("cat");
        dict1.add("wolf");
        dict1.add("fox");
        dict1.add("dog");
        dict1.add("canary");
        dict1.add("ox");
        dict1.add("zebra");
        dict1.add("ox");
        dict1.add("pigeon");
        dict1.add("panda");
        
        dict1.clear();
        dict2.clear();
        
        TS_ASSERT(dict1.empty());
        TS_ASSERT(dict2.empty());
    }
    
    
    void testOperatorAssign() {
        FreqDictionary dict1;
        FreqDictionary dict2;
        FreqDictionary dict3(dict1);
        FreqDictionary dict4(dict2);
        
        dict1.add("d");    //   ┌—––––––– d (1)
        dict1.add("A");    // A (1) ––––┐
        dict1.add("ab");   //     ┌—– ab (2) ––┐
        dict1.add("aa");   //   aa (3)         b (2) –┐
        dict1.add("b");    //                        c (1)
        dict1.add("b");
        dict1.add("ab");
        dict1.add("aa");
        dict1.add("aa");
        dict1.add("c");
        
        dict3.add("k");
        dict3.add("m");
        
        dict4.add("8");
        
        dict3 = dict1;
        dict4 = dict2;
        
        TS_ASSERT_EQUALS(dict1.size(), dict3.size());
        TS_ASSERT_EQUALS(dict1.frequency("d"), dict3.frequency("d"));
        TS_ASSERT_EQUALS(dict1.frequency("A"), dict3.frequency("A"));
        TS_ASSERT_EQUALS(dict1.frequency("ab"), dict3.frequency("ab"));
        TS_ASSERT_EQUALS(dict1.frequency("aa"), dict3.frequency("aa"));
        TS_ASSERT_EQUALS(dict1.frequency("b"), dict3.frequency("b"));
        TS_ASSERT_EQUALS(dict1.frequency("c"), dict3.frequency("c"));
        
        TS_ASSERT_EQUALS(dict2.size(), dict4.size());
    }
    
    
    void testOperatorMove() {
        FreqDictionary dict1;
        FreqDictionary dict2;
        FreqDictionary dict3(dict1);
        FreqDictionary dict4(dict2);
        
        dict1.add("d");    //   ┌—––––––– d (1)
        dict1.add("A");    // A (1) ––––┐
        dict1.add("ab");   //     ┌—– ab (2) ––┐
        dict1.add("aa");   //   aa (3)         b (2) –┐
        dict1.add("b");    //                        c (1)
        dict1.add("b");
        dict1.add("ab");
        dict1.add("aa");
        dict1.add("aa");
        dict1.add("c");
        
        dict3.add("k");
        dict3.add("m");
        
        dict4.add("8");
        
        dict3 = std::move(dict1);
        dict4 = std::move(dict2);
        
        TS_ASSERT_EQUALS(10, dict3.size());
        TS_ASSERT_EQUALS(1, dict3.frequency("d"));
        TS_ASSERT_EQUALS(1, dict3.frequency("A"));
        TS_ASSERT_EQUALS(2, dict3.frequency("ab"));
        TS_ASSERT_EQUALS(3, dict3.frequency("aa"));
        TS_ASSERT_EQUALS(2, dict3.frequency("b"));
        TS_ASSERT_EQUALS(1, dict3.frequency("c"));
        
        TS_ASSERT(dict4.empty());
    }
    
    
    void testOperatorOut1() {
        std::ostringstream os;
        FreqDictionary dict;
        const std::string str = "\n";
        
        os << dict;
        
        TS_ASSERT_EQUALS(str, os.str());
    }
    
    
    void testOperatorOut2() {
        std::ostringstream os;
        FreqDictionary dict;
        const std::string str =
            "canary (1)\n"
            "cat (2)\n"
            "dog (2)\n"
            "fox (1)\n"
            "ox (3)\n"
            "panda (1)\n"
            "pigeon (1)\n"
            "wolf (1)\n"
            "zebra (1)\n";
        
        dict.add("cat");    //     ┌—––––– cat (2) ––––––┐
        dict.add("dog");    // canary (1)             dog (2) –––––┐
        dict.add("cat");    //                            ┌—––– wolf (1) ––––┐
        dict.add("wolf");   //                         fox (1) –––┐      zebra (1)
        dict.add("fox");    //                                  ox (3) ––┐
        dict.add("dog");    //                                   ┌—– pigeon (1)
        dict.add("canary"); //                               panda (1)
        dict.add("ox");
        dict.add("ox");
        dict.add("zebra");
        dict.add("ox");
        dict.add("pigeon");
        dict.add("panda");
        
        os << dict;
        
        TS_ASSERT_EQUALS(str, os.str());
    }
};


#endif
