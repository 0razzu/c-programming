#ifndef avl_freq_dictionary_hpp
#define avl_freq_dictionary_hpp

#include "avl_node.hpp"
#include <utility>
#include <string>
#include <ostream>


class AvlFreqDictionary {
public:
    AvlFreqDictionary();
    AvlFreqDictionary(const AvlFreqDictionary&);
    AvlFreqDictionary(AvlFreqDictionary&&);
    ~AvlFreqDictionary();
    
    void add(const std::string&);
    unsigned frequency(const std::string&) const noexcept;
    bool del(const std::string&);
    
    void clear();
    
    unsigned long long size() const noexcept;
    bool empty() const noexcept;
    
    AvlFreqDictionary& operator =(const AvlFreqDictionary&);
    AvlFreqDictionary& operator =(AvlFreqDictionary&&);
    
    friend std::ostream& operator <<(std::ostream&, const AvlFreqDictionary&);

private:
    AvlNode* root_;
    unsigned long long size_;
    
    bool add(AvlNode*&, const std::string&);
    bool balanceAfterLeftAdd(AvlNode*&);
    bool balanceAfterRightAdd(AvlNode*&);
    const AvlNode* find(const AvlNode*, const std::string&) const noexcept;
    bool del(AvlNode*&, const std::string&);
    bool rebaseGreatest(AvlNode*&, AvlNode*&);
    bool balanceAfterLeftDel(AvlNode*&);
    bool balanceAfterRightDel(AvlNode*&);
    bool lRotation(AvlNode*&);
    bool rRotation(AvlNode*&);
    bool lrRotation(AvlNode*&);
    bool rlRotation(AvlNode*&);
    AvlNode* copy(const AvlNode*);
    void del(AvlNode*&);
};


#endif
