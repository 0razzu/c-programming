#ifndef node_hpp
#define node_hpp

#include <string>


struct Node {
    std::string word_;
    unsigned frequency_;
    
    Node* left_;
    Node* right_;
    
    
    Node(const std::string& word, unsigned frequency) {
        word_ = word;
        frequency_ = frequency;
        left_ = nullptr;
        right_ = nullptr;
    }
    
    
    Node(const std::string& word): Node(word, 1) {
    }
};


#endif
