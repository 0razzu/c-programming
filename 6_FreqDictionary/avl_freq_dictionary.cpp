#include "avl_freq_dictionary.hpp"


AvlFreqDictionary::AvlFreqDictionary() {
    root_ = nullptr;
    size_ = 0;
}


AvlFreqDictionary::AvlFreqDictionary(const AvlFreqDictionary& o) {
    root_ = o.root_? AvlFreqDictionary::copy(o.root_) : nullptr;
    size_ = o.size_;
}


AvlFreqDictionary::AvlFreqDictionary(AvlFreqDictionary&& o) {
    root_ = o.root_;
    size_ = o.size_;
    
    o.root_ = nullptr;
    o.size_ = 0;
}


AvlFreqDictionary::~AvlFreqDictionary() {
    clear();
}


void AvlFreqDictionary::add(const std::string& word) {
    add(root_, word);
    size_++;
}


unsigned AvlFreqDictionary::frequency(const std::string& word) const noexcept {
    const AvlNode* node = find(root_, word);
    
    if (node)
        return node->frequency_;
    
    return 0;
}


bool AvlFreqDictionary::del(const std::string& word) {
    unsigned long long currSize = size_;
    
    del(root_, word);
    
    return size_ < currSize;
}


void AvlFreqDictionary::clear() {
    if (root_)
        del(root_);
    
    size_ = 0;
}


unsigned long long AvlFreqDictionary::size() const noexcept {
    return size_;
}


bool AvlFreqDictionary::empty() const noexcept {
    return !size_;
}


AvlFreqDictionary& AvlFreqDictionary::operator =(const AvlFreqDictionary& o) {
    if (root_)
        del(root_);
    
    if (o.root_)
        root_ = AvlFreqDictionary::copy(o.root_);
    
    size_ = o.size_;
    
    return *this;
}


AvlFreqDictionary& AvlFreqDictionary::operator =(AvlFreqDictionary&& o) {
    std::swap(root_, o.root_);
    std::swap(size_, o.size_);
    
    return *this;
}


bool AvlFreqDictionary::add(AvlNode*& node, const std::string& word) {
    if (!node) {
        node = new AvlNode(word);
        
        return 1;
    }
    
    if (word < node->word_) {
        if (add(node->left_, word))
            return balanceAfterLeftAdd(node);
        
        return 0;
    }
    
    if (word > node->word_) {
        if (add(node->right_, word))
            return balanceAfterRightAdd(node);
        
        return 0;
    }
    
    node->frequency_++;
    
    return 0;
}


bool AvlFreqDictionary::balanceAfterLeftAdd(AvlNode*& node) {
    if (node->balance_ == 1) {
        node->balance_ = 0;
        
        return 0;
    }
    
    if (node->balance_ == 0) {
        node->balance_ = -1;
        
        return 1;
    }
    
    if (node->left_->balance_ < 0)
        return rRotation(node);
    
    return lrRotation(node);
}


bool AvlFreqDictionary::balanceAfterRightAdd(AvlNode*& node) {
    if (node->balance_ == -1) {
        node->balance_ = 0;
        
        return 0;
    }
    
    if (node->balance_ == 0) {
        node->balance_ = 1;
        
        return 1;
    }
    
    if (node->right_->balance_ < 0)
        return rlRotation(node);
    
    return lRotation(node);
}


const AvlNode* AvlFreqDictionary::find(const AvlNode* node, const std::string& word) const noexcept {
    if (!node)
        return node;
    
    else if (word < node->word_)
        return find(node->left_, word);
        
    else if (word > node->word_)
        return find(node->right_, word);
    
    return node;
}


bool AvlFreqDictionary::del(AvlNode*& node, const std::string& word) {
    if (!node)
        return 0;
    
    if (word < node->word_) {
        if (del(node->left_, word))
            return balanceAfterLeftDel(node);
        
        return 0;
    }
    
    if (word > node->word_) {
        if (del(node->right_, word))
            return balanceAfterRightDel(node);
        
        return 0;
    }
        
    if (node->frequency_ == 1) {
        AvlNode* toDelete = node;
        
        if (!node->left_) {
            node = node->right_;
            delete toDelete;
            size_--;
            
            return 1;
        }
        
        if (!node->right_) {
            node = node->left_;
            delete toDelete;
            size_--;
            
            return 1;
        }
        
        bool balance = rebaseGreatest(toDelete, node->left_);
        delete toDelete;
        size_--;
                
        return balance? balanceAfterLeftDel(node) : 0;
    }
    
    node->frequency_--;
    size_--;
    
    return 0;
}


bool AvlFreqDictionary::rebaseGreatest(AvlNode*& node, AvlNode*& subtreeNode) {
    if (subtreeNode->right_) {
        if (rebaseGreatest(node, subtreeNode->right_))
            return balanceAfterRightDel(subtreeNode);
        
        return 0;
    }
    
    node->word_ = subtreeNode->word_;
    node->frequency_ = subtreeNode->frequency_;
    node = subtreeNode;
    subtreeNode = subtreeNode->left_;
    
    return 1;
}


bool AvlFreqDictionary::balanceAfterLeftDel(AvlNode*& node) {
    if (node->balance_ == -1) {
        node->balance_ = 0;
        
        return 1;
    }
    
    if (node->balance_ == 0) {
        node->balance_ = 1;
        
        return 0;
    }
    
    if (node->right_->balance_ >= 0) {
        lRotation(node);
        
        return !node->left_->right_;
    }
    
    return rlRotation(node);
}


bool AvlFreqDictionary::balanceAfterRightDel(AvlNode*& node) {
    if (node->balance_ == 1) {
        node->balance_ = 0;
        
        return 1;
    }
    
    if (node->balance_ == 0) {
        node->balance_ = -1;
        
        return 0;
    }
    
    if (node->left_->balance_ <= 0) {
        rRotation(node);
        
        return !node->right_->left_;
    }
    
    return lrRotation(node);
}


AvlNode* AvlFreqDictionary::copy(const AvlNode* node) {
    AvlNode* copy = new AvlNode(node->word_, node->frequency_);
    
    if (node->left_)
        copy->left_ = AvlFreqDictionary::copy(node->left_);
    
    if (node->right_)
        copy->right_ = AvlFreqDictionary::copy(node->right_);
    
    return copy;
}


void AvlFreqDictionary::del(AvlNode*& node) {
    if (node->left_)
        del(node->left_);
    
    if (node->right_)
        del(node->right_);
    
    delete node;
    node = nullptr;
}


bool AvlFreqDictionary::lRotation(AvlNode*& node) {
    AvlNode* right = node->right_;
    
    if (right->balance_ == 0) {
        node->balance_ = 1;
        right->balance_ = -1;
    }
        
    else // right->balance_ == 1
        node->balance_ = right->balance_ = 0;
    
    node->right_ = right->left_;
    right->left_ = node;
    node = right;
    
    return 0;
}


bool AvlFreqDictionary::rRotation(AvlNode*& node) {
    AvlNode* left = node->left_;
    
    if (left->balance_ == 0) {
        node->balance_ = -1;
        left->balance_ = 1;
    }
    
    else // left->balance == -1
        node->balance_ = left->balance_ = 0;
    
    node->left_ = left->right_;
    left->right_ = node;
    node = left;
    
    return 0;
}


bool AvlFreqDictionary::lrRotation(AvlNode*& node) {
    AvlNode* left = node->left_;
    AvlNode* leftRight = node->left_->right_;
    
    if (leftRight->balance_ < 0) {
        leftRight->balance_ = left->balance_ = 0;
        node->balance_ = 1;
    }
    
    else if (leftRight->balance_ > 0) {
        leftRight->balance_ = node->balance_ = 0;
        left->balance_ = -1;
    }
    
    else
        node->balance_ = left->balance_ = 0;
    
    node->left_ = leftRight->right_;
    left->right_ = leftRight->left_;
    leftRight->left_ = left;
    leftRight->right_ = node;
    node = leftRight;
    
    return 0;
}


bool AvlFreqDictionary::rlRotation(AvlNode*& node) {
    AvlNode* right = node->right_;
    AvlNode* rightLeft = node->right_->left_;
    
    if (rightLeft->balance_ > 0) {
        rightLeft->balance_ = right->balance_ = 0;
        node->balance_ = -1;
    }
    
    else if (rightLeft->balance_ < 0) {
        rightLeft->balance_ = node->balance_ = 0;
        right->balance_ = 1;
    }
    
    else
        node->balance_ = right->balance_ = 0;
    
    node->right_ = rightLeft->left_;
    right->left_ = rightLeft->right_;
    rightLeft->left_ = node;
    rightLeft->right_ = right;
    node = rightLeft;
    
    return 0;
}


void out(std::ostream& os, const AvlNode* node) {
    if (node->left_)
        out(os, node->left_);
    
    os << node->word_ << " (" << node->frequency_ << ")" << std::endl;
    
    if (node->right_)
        out(os, node->right_);
}


std::ostream& operator <<(std::ostream& os, const AvlFreqDictionary& dict) {
    if (!dict.root_)
        os << std::endl;
    
    else
        out(os, dict.root_);
    
    return os;
}
