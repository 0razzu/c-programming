#ifndef test_int_dynamic_array_h
#define test_int_dynamic_array_h

#include "test_constants.hpp"
#include "constants.hpp"
#include "int_dynamic_array.hpp"
#include "int_dynamic_array_exception.hpp"
#include <cxxtest/TestSuite.h>
#include <string>
#include <sstream>


class TestIntDynamicArray: public CxxTest::TestSuite {
public:
    void testConstr1() {
        IntDynamicArray a1(0, 0, 5);
        IntDynamicArray a2(3, 1, 3);
        
        TS_ASSERT_EQUALS(0, a1.size());
        TS_ASSERT_EQUALS(5, a1.capacity());
        TS_ASSERT_EQUALS(3, a2.size());
        TS_ASSERT_EQUALS(3, a2.capacity());
        TS_ASSERT_EQUALS(1, a2[0]);
        TS_ASSERT_EQUALS(1, a2[1]);
        TS_ASSERT_EQUALS(1, a2[2]);
    }
    
    
    void testConstr2() {
        IntDynamicArray a1(0, 0);
        IntDynamicArray a2(4, -2);
        
        TS_ASSERT_EQUALS(0, a1.size());
        TS_ASSERT_EQUALS(16, a1.capacity());
        TS_ASSERT_EQUALS(4, a2.size());
        TS_ASSERT_EQUALS(20, a2.capacity());
        TS_ASSERT_EQUALS(-2, a2[0]);
        TS_ASSERT_EQUALS(-2, a2[1]);
        TS_ASSERT_EQUALS(-2, a2[2]);
        TS_ASSERT_EQUALS(-2, a2[3]);
    }
    
    
    void testConstr3() {
        IntDynamicArray a1(0);
        IntDynamicArray a2(2);
        
        TS_ASSERT_EQUALS(0, a1.size());
        TS_ASSERT_EQUALS(16, a1.capacity());
        TS_ASSERT_EQUALS(2, a2.size());
        TS_ASSERT_EQUALS(18, a2.capacity());
        TS_ASSERT_EQUALS(0, a2[0]);
        TS_ASSERT_EQUALS(0, a2[1]);
    }
    
    
    void testConstr4() {
        IntDynamicArray a;
        
        TS_ASSERT_EQUALS(16, a.size());
        TS_ASSERT_EQUALS(32, a.capacity());
        
        for (short i = 0; i < 16; i++)
            TS_ASSERT_EQUALS(0, a[i]);
    }
    
    
    void testConstrExceptions() {
        try {
            IntDynamicArray a(6, 0, 5);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (IntDynamicArrayException e) {
            TS_ASSERT_SAME_DATA(INCORRECT_CAPACITY, e.what(), strlen(INCORRECT_CAPACITY));
        }
    }
    
    
    void testArrayParameterizedConstr1() {
        constexpr int a1[] = {};
        constexpr int a2[] = {1};
        constexpr int a3[] = {0, 1, 4, 9, 16, 25, 36, 49, 64, 81};
        
        IntDynamicArray a(a1, 0, 0);
        const IntDynamicArray b(a2, 1, 1);
        IntDynamicArray c(a3, 10, 12);
        IntDynamicArray d(a3, 3, 10);
        
        TS_ASSERT_EQUALS(0, a.size());
        TS_ASSERT_EQUALS(0, a.capacity());
        
        TS_ASSERT_EQUALS(1, b.size());
        TS_ASSERT_EQUALS(1, b.capacity());
        TS_ASSERT_EQUALS(1, b[0]);
        
        TS_ASSERT_EQUALS(10, c.size());
        TS_ASSERT_EQUALS(12, c.capacity());
        for (short i = 0; i < 10; i++)
            TS_ASSERT_EQUALS(i * i, c[i]);
        
        TS_ASSERT_EQUALS(3, d.size());
        TS_ASSERT_EQUALS(10, d.capacity());
        TS_ASSERT_EQUALS(0, d[0]);
        TS_ASSERT_EQUALS(1, d[1]);
        TS_ASSERT_EQUALS(4, d[2]);
    }
    
    
    void testArrayParameterizedConstr2() {
        constexpr int a1[] = {};
        constexpr int a2[] = {1};
        constexpr int a3[] = {0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20};
        
        IntDynamicArray a(a1, 0);
        const IntDynamicArray b(a2, 1);
        IntDynamicArray c(a3, 11);
        IntDynamicArray d(a3, 5);
        
        TS_ASSERT_EQUALS(0, a.size());
        TS_ASSERT_EQUALS(DEFAULT_SIZE, a.capacity());
        
        TS_ASSERT_EQUALS(1, b.size());
        TS_ASSERT_EQUALS(1 + DEFAULT_SIZE, b.capacity());
        TS_ASSERT_EQUALS(1, b[0]);
        
        TS_ASSERT_EQUALS(11, c.size());
        TS_ASSERT_EQUALS(11 + DEFAULT_SIZE, c.capacity());
        for (short i = 0; i < 11; i++)
            TS_ASSERT_EQUALS(2 * i, c[i]);
        
        TS_ASSERT_EQUALS(5, d.size());
        TS_ASSERT_EQUALS(5 + DEFAULT_SIZE, d.capacity());
        for (short i = 0; i < 5; i++)
            TS_ASSERT_EQUALS(2 * i, d[i]);
    }
    
    
    void testArrayParameterizedConstrExceptions() {
        constexpr int a1[] = {1, 0, 9, 6};
        
        try {
            IntDynamicArray a(a1, 3, 2);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (IntDynamicArrayException e) {
            TS_ASSERT_SAME_DATA(INCORRECT_CAPACITY, e.what(), strlen(INCORRECT_CAPACITY));
        }
    }
    
    
    void testCopyConstr() {
        IntDynamicArray a1(10, 2, 10);
        
        a1[1] = 8;
        a1[2] = 13;
        
        IntDynamicArray a2(a1);
        
        a1[0] = 0;
        
        TS_ASSERT_EQUALS(10, a2.size());
        TS_ASSERT_EQUALS(10, a2.capacity());
        
        TS_ASSERT_EQUALS(2, a2[0]);
        TS_ASSERT_EQUALS(8, a2[1]);
        TS_ASSERT_EQUALS(13, a2[2]);
        for (short i = 3; i < 10; i++)
            TS_ASSERT_EQUALS(2, a2[i]);
    }
    
    
    void testCopyConstr2() {
        IntDynamicArray a1(10, 2, 10);
        
        a1[1] = 8;
        a1[2] = 13;
        
        IntDynamicArray a2(a1, 2, 6);
        
        a1[0] = 0;
        
        TS_ASSERT_EQUALS(3, a2.size());
        TS_ASSERT_EQUALS(19, a2.capacity());
        
        TS_ASSERT_EQUALS(13, a2[0]);
        TS_ASSERT_EQUALS(2, a2[1]);
        TS_ASSERT_EQUALS(2, a2[2]);
    }
    
    
    void testMoveConstr() {
        IntDynamicArray a1(3, 2, 3);
        
        a1[0] = 1;
        a1[2] = 3;
        
        IntDynamicArray a2(std::move(a1));
        
        TS_ASSERT_EQUALS(3, a2.size());
        
        TS_ASSERT_EQUALS(1, a2[0]);
        TS_ASSERT_EQUALS(2, a2[1]);
        TS_ASSERT_EQUALS(3, a2[2]);
    }
    
    
    void testResizeNotExceedingCapacity() {
        IntDynamicArray a(1, 9);
        
        a.resize(10);
        
        TS_ASSERT_EQUALS(10, a.size());
        TS_ASSERT_EQUALS(1 + DEFAULT_SIZE, a.capacity());
        
        TS_ASSERT_EQUALS(9, a[0]);
        for (short i = 1; i < 10; i++)
            TS_ASSERT_EQUALS(0, a[i]);
    }
    
    
    void testResizeExceedingCapacity() {
        IntDynamicArray a(0);
        constexpr short newSize = 3 + DEFAULT_SIZE;
        
        a.resize(newSize);
        
        TS_ASSERT_EQUALS(newSize, a.size());
        TS_ASSERT_EQUALS(newSize, a.capacity());
        
        for (short i = 0; i < newSize; i++)
            TS_ASSERT_EQUALS(0, a[i]);
    }
    
    
    void testReserve() {
        IntDynamicArray a;
        
        a[1] = 9;
        
        a.reserve(4);
        
        TS_ASSERT_EQUALS(DEFAULT_SIZE, a.size());
        TS_ASSERT_EQUALS(4 + 2 * DEFAULT_SIZE, a.capacity());
    }
    
    
    void testShrinkNotChangingSize() {
        IntDynamicArray a(3, 1, 10);
        
        a.shrink(6);
        
        TS_ASSERT_EQUALS(3, a.size());
        TS_ASSERT_EQUALS(4, a.capacity());
    }
    
    
    void testShrinkChangingSize() {
        IntDynamicArray a(3, 1, 10);
        
        a.shrink(8);
        
        TS_ASSERT_EQUALS(2, a.size());
        TS_ASSERT_EQUALS(2, a.capacity());
    }
    
    
    void testShrinkExceptions() {
        IntDynamicArray a(3, 1, 10);
        
        try {
            a.shrink(11);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (IntDynamicArrayException e) {
            TS_ASSERT_SAME_DATA(INCORRECT_SHRINK, e.what(), strlen(INCORRECT_SHRINK));
        }
    }
    
    
    void testShrinkToSize() {
        IntDynamicArray a(3, 1, 10);
        
        a.shrinkToSize();
        
        TS_ASSERT_EQUALS(3, a.size());
        TS_ASSERT_EQUALS(3, a.capacity());
    }
    
    
    void testAt() {
        IntDynamicArray a(4, 100);
        
        TS_ASSERT_EQUALS(100, a.at(1));
        
        a.at(1) = 6;
        
        TS_ASSERT_EQUALS(6, a.at(1));
        
        try {
            int x = a.at(4);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (IntDynamicArrayException e) {
            TS_ASSERT_SAME_DATA(INCORRECT_INDEX, e.what(), strlen(INCORRECT_INDEX));
        }
    }
    
    
    void testAtConst() {
        const IntDynamicArray a(5, 12);
        
        TS_ASSERT_EQUALS(12, a.at(1));
        
        try {
            int x = a.at(10);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (IntDynamicArrayException e) {
            TS_ASSERT_SAME_DATA(INCORRECT_INDEX, e.what(), strlen(INCORRECT_INDEX));
        }
    }
    
    
    void testPushBackNotChangingReserve() {
        IntDynamicArray a(1, 1, 5);
        
        a.pushBack(2);
        a.pushBack(3);
        
        TS_ASSERT_EQUALS(3, a.size());
        TS_ASSERT_EQUALS(5, a.capacity());
        
        TS_ASSERT_EQUALS(1, a[0]);
        TS_ASSERT_EQUALS(2, a[1]);
        TS_ASSERT_EQUALS(3, a[2]);
    }
    
    
    void testPushBackChangingReserve() {
        IntDynamicArray a(1, 3, 1);
        
        a.pushBack(2);
        a.pushBack(1);
        
        TS_ASSERT_EQUALS(3, a.size());
        TS_ASSERT_LESS_THAN_EQUALS(3, a.capacity());
        
        TS_ASSERT_EQUALS(3, a[0]);
        TS_ASSERT_EQUALS(2, a[1]);
        TS_ASSERT_EQUALS(1, a[2]);
    }
    
    
    void testPopBackNotChangingReserve() {
        IntDynamicArray a(DEFAULT_SIZE, 8, DEFAULT_SIZE);
        
        TS_ASSERT_EQUALS(8, a.popBack());
        
        TS_ASSERT_EQUALS(DEFAULT_SIZE - 1, a.size());
        TS_ASSERT_EQUALS(DEFAULT_SIZE, a.capacity());
    }
    
    
    void testPopBackChangingReserve() {
        IntDynamicArray a(DEFAULT_SIZE, 5, 100 * DEFAULT_SIZE);
        
        TS_ASSERT_EQUALS(5, a.popBack());
        
        TS_ASSERT_EQUALS(DEFAULT_SIZE - 1, a.size());
        TS_ASSERT_LESS_THAN(a.capacity(), 100 * DEFAULT_SIZE);
    }
    
    
    void testPopBackWithArgNotChangingReserve() {
        IntDynamicArray a(DEFAULT_SIZE, 5, 100 * DEFAULT_SIZE);
        
        TS_ASSERT_EQUALS(5, a.popBack(0));
        
        TS_ASSERT_EQUALS(DEFAULT_SIZE - 1, a.size());
        TS_ASSERT_EQUALS(a.capacity(), 100 * DEFAULT_SIZE);
    }
    
    
    void testPopBackWithArgChangingReserve() {
        IntDynamicArray a(DEFAULT_SIZE, 5, 100 * DEFAULT_SIZE);
        
        TS_ASSERT_EQUALS(5, a.popBack(1));
        
        TS_ASSERT_EQUALS(DEFAULT_SIZE - 1, a.size());
        TS_ASSERT_LESS_THAN(a.capacity(), 100 * DEFAULT_SIZE);
    }
    
    
    void setElem(int& e, int value) {
        e = value;
    }
    
    
    void testOperatorBrackets() {
        IntDynamicArray a(10, 8, 12);
        
        for (short i = 0; i < a.size(); i++)
            a[i] = i + 1;
        
        for (short i = 0; i < a.size(); i++)
            TS_ASSERT_EQUALS(i + 1, a[i])
        
        TS_ASSERT_THROWS_NOTHING(a[10]);
        TS_ASSERT_THROWS_NOTHING(setElem(a[11], 10));
        TS_ASSERT_EQUALS(10, a[11]);
        TS_ASSERT_THROWS_NOTHING(setElem(a[11], -5));
        TS_ASSERT_EQUALS(-5, a[11]);
    }
    
    
    void testOperatorBracketsConst() {
        const IntDynamicArray a(10, 5, 20);
        
        for (short i = 0; i < 10; i++)
            TS_ASSERT_EQUALS(5, a[i]);
        
        TS_ASSERT_THROWS_NOTHING(a[15]);
    }
    
    
    void testOperatorAssign() {
        IntDynamicArray a(2, 3);
        IntDynamicArray b(10, 4, 11);
        
        a[0] = 0;
        b[0] = 15;
        
        a = b;
        
        TS_ASSERT_EQUALS(10, b.size());
        TS_ASSERT_EQUALS(11, b.capacity());
        
        TS_ASSERT_EQUALS(15, b[0]);
        for (short i = 1; i < 10; i++)
            TS_ASSERT_EQUALS(4, b[i]);
        
        TS_ASSERT_EQUALS(b.size(), a.size());
        TS_ASSERT_EQUALS(b.capacity(), a.capacity());
        
        TS_ASSERT_EQUALS(b[0], a[0]);
        for (short i = 1; i < 10; i++)
            TS_ASSERT_EQUALS(b[i], a[i]);
    }
    
    
    void testMoveOperatorAssign() {
        IntDynamicArray a(8, 3);
        IntDynamicArray b(10, 1, 11);
        
        a[2] = 0;
        b[9] = 9;
        
        a = std::move(b);
        
        TS_ASSERT_EQUALS(10, a.size());
        TS_ASSERT_EQUALS(11, a.capacity());
        
        for (short i = 0; i < 9; i++)
            TS_ASSERT_EQUALS(1, a[i]);
        TS_ASSERT_EQUALS(9, a[9]);
    }
    
    
    void testOperatorEquals() {
        const IntDynamicArray a(2, 3, 2);
        IntDynamicArray b(2, 3, 8);
        IntDynamicArray c(3, 3, 8);
        IntDynamicArray d(3, 4);
        IntDynamicArray e(d);
        IntDynamicArray f(0, 0, 0);
        
        e[1] = 0;
        
        f.pushBack(4);
        f.pushBack(4);
        f.pushBack(4);
        
        TS_ASSERT_EQUALS(a, a);
        TS_ASSERT_EQUALS(a, b);
        TS_ASSERT_EQUALS(b, a);
        
        try {
            bool bEqualsC = b == c;
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (IntDynamicArrayException e) {
            TS_ASSERT_SAME_DATA(EQ_CHECK_FAIL, e.what(), strlen(EQ_CHECK_FAIL));
        }
        
        TS_ASSERT(!(c == d));
        TS_ASSERT(!(d == e));
        TS_ASSERT_EQUALS(d, f);
    }
    
    
    void testOperatorDiffers() {
        const IntDynamicArray a(2, 3, 2);
        IntDynamicArray b(2, 3, 8);
        IntDynamicArray c(3, 3, 8);
        IntDynamicArray d(3, 4);
        IntDynamicArray e(d);
        IntDynamicArray f(0, 0, 0);
        
        e[1] = 0;
        
        f.pushBack(4);
        f.pushBack(4);
        f.pushBack(4);
        
        TS_ASSERT(!(a != a));
        TS_ASSERT(!(a != b));
        TS_ASSERT(!(b != a));
        
        try {
            bool bEqualsC = b != c;
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (IntDynamicArrayException e) {
            TS_ASSERT_SAME_DATA(EQ_CHECK_FAIL, e.what(), strlen(EQ_CHECK_FAIL));
        }
        
        TS_ASSERT(c != d);
        TS_ASSERT(d != e);
        TS_ASSERT(!(d != f));
    }
    
    
    void testOperatorLess() {
        IntDynamicArray a(10);
        IntDynamicArray b(0);
        const IntDynamicArray c(0);
        
        for (short i = 0; i < 10; i++)
            a[i] = i;
        
        for (short i = 0; i < 9; i++)
            b.pushBack(i);
        
        const IntDynamicArray d = a;
        IntDynamicArray e = b;
        IntDynamicArray f = e;
        
        e.pushBack(1);
        f.pushBack(100);
        
        IntDynamicArray g = f;
        
        g[5] = 22;
        
        TS_ASSERT(!(a < a));
        TS_ASSERT(!(a < b) && (b < a));
        TS_ASSERT(!(a < d) && !(d < a));
        TS_ASSERT_LESS_THAN(a, f);
        TS_ASSERT_LESS_THAN(a, g);
        
        TS_ASSERT_LESS_THAN(b, e);
        TS_ASSERT_LESS_THAN(b, g);
        
        TS_ASSERT_LESS_THAN(c, a);
        TS_ASSERT_LESS_THAN(c, b);
        TS_ASSERT_LESS_THAN(c, d);
        TS_ASSERT_LESS_THAN(c, e);
        TS_ASSERT_LESS_THAN(c, g);
        
        TS_ASSERT_LESS_THAN(e, f);
        TS_ASSERT_LESS_THAN(e, g);
        
        TS_ASSERT_LESS_THAN(f, g);
    }
    
    
    void testOperatorGreater() {
        IntDynamicArray a(10);
        IntDynamicArray b(0);
        const IntDynamicArray c(0);
        
        for (short i = 0; i < 10; i++)
            a[i] = i;
        
        for (short i = 0; i < 9; i++)
            b.pushBack(i);
        
        const IntDynamicArray d = a;
        IntDynamicArray e = b;
        IntDynamicArray f = e;
        
        e.pushBack(1);
        f.pushBack(100);
        
        IntDynamicArray g = f;
        
        g[5] = 22;
        
        TS_ASSERT(!(a > a));
        TS_ASSERT((a > b) && !(b > a));
        TS_ASSERT(!(a > d) && !(d > a));
        
        TS_ASSERT(!(c > a));
        TS_ASSERT(!(c > b));
        TS_ASSERT(!(c > d));
        TS_ASSERT(!(c > e));
        TS_ASSERT(!(c > g));
        
        TS_ASSERT(e > b);
        
        TS_ASSERT(f > a);
        TS_ASSERT(f > e);
        
        TS_ASSERT(g > a);
        TS_ASSERT(g > b);
        TS_ASSERT(g > e);
        TS_ASSERT(g > f);
    }
    
    
    void testOperatorLessOrEquals() {
        IntDynamicArray a(10);
        IntDynamicArray b(0);
        const IntDynamicArray c(0);
        
        for (short i = 0; i < 10; i++)
            a[i] = i;
        
        for (short i = 0; i < 9; i++)
            b.pushBack(i);
        
        const IntDynamicArray d = a;
        IntDynamicArray e = b;
        IntDynamicArray f = e;
        
        e.pushBack(1);
        f.pushBack(100);
        
        IntDynamicArray g = f;
        
        g[5] = 22;
        
        TS_ASSERT(a <= a);
        TS_ASSERT(!(a <= b) && (b <= a));
        TS_ASSERT((a <= d) && (d <= a));
        TS_ASSERT_LESS_THAN_EQUALS(a, f);
        TS_ASSERT_LESS_THAN_EQUALS(a, g);
        
        TS_ASSERT_LESS_THAN_EQUALS(b, e);
        TS_ASSERT_LESS_THAN_EQUALS(b, g);
        
        TS_ASSERT_LESS_THAN_EQUALS(c, a);
        TS_ASSERT_LESS_THAN_EQUALS(c, b);
        TS_ASSERT_LESS_THAN_EQUALS(c, d);
        TS_ASSERT_LESS_THAN_EQUALS(c, e);
        TS_ASSERT_LESS_THAN_EQUALS(c, g);
        
        TS_ASSERT_LESS_THAN_EQUALS(e, f);
        TS_ASSERT_LESS_THAN_EQUALS(e, g);
        
        TS_ASSERT_LESS_THAN(f, g);
    }
    
    
    void testOperatorGreaterOrEquals() {
        IntDynamicArray a(10);
        IntDynamicArray b(0);
        const IntDynamicArray c(0);
        
        for (short i = 0; i < 10; i++)
            a[i] = i;
        
        for (short i = 0; i < 9; i++)
            b.pushBack(i);
        
        const IntDynamicArray d = a;
        IntDynamicArray e = b;
        IntDynamicArray f = e;
        
        e.pushBack(1);
        f.pushBack(100);
        
        IntDynamicArray g = f;
        
        g[5] = 22;
        
        TS_ASSERT(a >= a);
        TS_ASSERT((a >= b) && !(b >= a));
        TS_ASSERT((a >= d) && (d >= a));
        
        TS_ASSERT(!(c >= a));
        TS_ASSERT(!(c >= b));
        TS_ASSERT(!(c >= d));
        TS_ASSERT(!(c >= e));
        TS_ASSERT(!(c >= g));
        
        TS_ASSERT(e >= b);
        
        TS_ASSERT(f >= a);
        TS_ASSERT(f >= e);
        
        TS_ASSERT(g >= a);
        TS_ASSERT(g >= b);
        TS_ASSERT(g >= e);
        TS_ASSERT(g >= f);
    }
    
    
    void testOperatorConcat() {
        IntDynamicArray a(0, 0, 3);
        const IntDynamicArray b(2, 1, 2);
        IntDynamicArray c(2, 2);
        
        c.pushBack(3);
        
        constexpr int bcArray[] = {1, 1, 2, 2, 3};
        constexpr int ccArray[] = {2, 2, 3, 2, 2, 3};
        constexpr int cabcArray[] = {2, 2, 3, 1, 1, 2, 2, 3};
        
        IntDynamicArray bc(bcArray, 5, 5);
        IntDynamicArray cc(ccArray, 6, 6);
        IntDynamicArray cabc(cabcArray, 8, 8);
        
        IntDynamicArray d = a + b;
        TS_ASSERT_EQUALS(b, d);
        TS_ASSERT_EQUALS(b.size() + DEFAULT_SIZE, d.capacity());
        
        IntDynamicArray e = b + c;
        TS_ASSERT_EQUALS(bc, e);
        TS_ASSERT_EQUALS(bc.size() + DEFAULT_SIZE, e.capacity());
        
        IntDynamicArray f = c + c;
        TS_ASSERT_EQUALS(cc, f);
        TS_ASSERT_EQUALS(cc.size() + DEFAULT_SIZE, f.capacity());
        
        IntDynamicArray g = c + a + e;
        TS_ASSERT_EQUALS(cabc, g);
        TS_ASSERT_EQUALS(cabc.size() + DEFAULT_SIZE, g.capacity());
    }
    
    
    void testOperatorIn1() {
        std::stringstream s;
        IntDynamicArray a;
        
        s << "0 abc";
        s >> a;
        
        TS_ASSERT_EQUALS(0, a.size());
        TS_ASSERT_EQUALS(DEFAULT_SIZE, a.capacity());
    }
    
    
    void testOperatorIn2() {
        std::stringstream s;
        IntDynamicArray a(2, 3, 4);
        
        s << "6 -1 3 10 20 15 8923";
        s >> a;
        
        TS_ASSERT_EQUALS(6, a.size());
        TS_ASSERT_EQUALS(6 + DEFAULT_SIZE, a.capacity());
        TS_ASSERT_EQUALS(-1, a[0]);
        TS_ASSERT_EQUALS(3, a[1]);
        TS_ASSERT_EQUALS(10, a[2]);
        TS_ASSERT_EQUALS(20, a[3]);
        TS_ASSERT_EQUALS(15, a[4]);
        TS_ASSERT_EQUALS(8923, a[5]);
    }
    
    
    void testOperatorOut1() {
        std::ostringstream os;
        IntDynamicArray a(0);
        const std::string str = "IntDynamicArray {size: 0, data: []}";

        os << a;

        TS_ASSERT_EQUALS(str, os.str())
    }
    
    
    void testOperatorOut2() {
        std::ostringstream os;
        constexpr int a1[] = {0, 3, 78, -64, 2};
        IntDynamicArray a(a1, 5);
        const std::string str = "IntDynamicArray {size: 5, data: [0, 3, 78, -64, 2]}";
        
        os << a;
        
        TS_ASSERT_EQUALS(str, os.str())
    }
};


#endif
