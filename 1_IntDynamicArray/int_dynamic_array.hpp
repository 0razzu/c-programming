#ifndef IntDynamicArray_hpp
#define IntDynamicArray_hpp

#include "constants.hpp"
#include "int_dynamic_array_exception.hpp"
#include <cmath>
#include <iostream>


class IntDynamicArray {
public:
    IntDynamicArray(unsigned long long size, int number, unsigned long long capacity);
    IntDynamicArray(unsigned long long size, int number);
    IntDynamicArray(unsigned long long size);
    IntDynamicArray();
    IntDynamicArray(const int[], unsigned long long quantity, unsigned long long capacity);
    IntDynamicArray(const int[], unsigned long long quantity);
    IntDynamicArray(const IntDynamicArray&);
    IntDynamicArray(const IntDynamicArray&, unsigned long long from, unsigned long long to);
    IntDynamicArray(IntDynamicArray&&);
    
    ~IntDynamicArray();
    
    unsigned long long size() const noexcept;
    unsigned long long capacity() const noexcept;
    
    void resize(unsigned long long);
    void reserve(unsigned long long);
    void shrink(unsigned long long);
    void shrinkToSize();
    
    int& at(unsigned long long);
    int at(unsigned long long) const;
    void pushBack(int);
    void pushBack(const int[], unsigned long long quantity);
    int popBack(bool shrink);
    int popBack();
    void delBack(unsigned long long quantity, bool shrink);
    void delBack(unsigned long long quantity);
    
    int& operator [](unsigned long long);
    int operator [](unsigned long long) const;
    
    IntDynamicArray& operator =(const IntDynamicArray&);
    IntDynamicArray& operator =(IntDynamicArray&&);
    
    bool operator ==(const IntDynamicArray&) const;
    bool operator !=(const IntDynamicArray&) const;
    
    bool operator <(const IntDynamicArray&) const noexcept;
    bool operator >(const IntDynamicArray&) const noexcept;
    bool operator <=(const IntDynamicArray&) const noexcept;
    bool operator >=(const IntDynamicArray&) const noexcept;
    
    IntDynamicArray operator +(const IntDynamicArray&) const;
    
    friend std::istream& operator >>(std::istream&, IntDynamicArray&);
    friend std::ostream& operator <<(std::ostream&, const IntDynamicArray&);
    
private:
    int* data_;
    unsigned long long size_;
    unsigned long long capacity_;
    int resizes_;
    
    void checkIndex(unsigned long long) const;
    
    void shrinkIfNeeded();
};


#endif
