#ifndef box_exception_hpp
#define box_exception_hpp

#include <exception>


class IntDynamicArrayException: public std::exception {
public:
    IntDynamicArrayException(const char*);
    virtual const char* what() const noexcept override;
    
private:
    const char* message;
};


#endif
