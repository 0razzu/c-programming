#include "int_dynamic_array_exception.hpp"


IntDynamicArrayException::IntDynamicArrayException(const char* message) {
    this->message = message;
}


const char* IntDynamicArrayException::what() const noexcept {
    return message;
}
