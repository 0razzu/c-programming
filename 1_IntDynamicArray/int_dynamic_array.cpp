#include "int_dynamic_array.hpp"


IntDynamicArray::IntDynamicArray(unsigned long long size, int number, unsigned long long capacity) {
    if (capacity < size)
        throw IntDynamicArrayException(INCORRECT_CAPACITY);
    
    data_ = new int[capacity];
    
    for (unsigned long long i = 0; i < size; i++)
        data_[i] = number;
    
    size_ = size;
    capacity_ = capacity;
    resizes_ = 0;
}


IntDynamicArray::IntDynamicArray(unsigned long long size, int number): IntDynamicArray(size, number, size + DEFAULT_SIZE) {
}


IntDynamicArray::IntDynamicArray(unsigned long long size): IntDynamicArray(size, 0) {
}


IntDynamicArray::IntDynamicArray(): IntDynamicArray(DEFAULT_SIZE) {
}


IntDynamicArray::IntDynamicArray(const int array[], unsigned long long quantity, unsigned long long capacity) {
    if (capacity < quantity)
        throw IntDynamicArrayException(INCORRECT_CAPACITY);
    
    data_ = new int[capacity];
    
    for (unsigned long long i = 0; i < quantity; i++)
        data_[i] = array[i];
    
    size_ = quantity;
    capacity_ = capacity;
    resizes_ = 0;
}


IntDynamicArray::IntDynamicArray(const int array[], unsigned long long quantity): IntDynamicArray(array, quantity, quantity + DEFAULT_SIZE) {
}


IntDynamicArray::IntDynamicArray(const IntDynamicArray& o) {
    data_ = new int[o.capacity_];
    
    for (unsigned long long i = 0; i < o.size_; i++)
        data_[i] = o.data_[i];
    
    size_ = o.size_;
    capacity_ = o.capacity_;
    resizes_ = 0;
}


IntDynamicArray::IntDynamicArray(const IntDynamicArray& o, unsigned long long from, unsigned long long to) {
    if (from >= to || to > o.size_)
        throw IntDynamicArrayException(INCORRECT_RANGE);
    
    data_ = new int[to - from - 1 + DEFAULT_SIZE];
    
    for (unsigned long long i = from; i < to; i++)
        data_[i - from] = o.data_[i];
    
    size_ = to - from - 1;
    capacity_ = size_ + DEFAULT_SIZE;
    resizes_ = 0;
}


IntDynamicArray::IntDynamicArray(IntDynamicArray&& o) {
    data_ = o.data_;
    o.data_ = nullptr;
    size_ = o.size_;
    capacity_ = o.capacity_;
    resizes_ = 0;
}


IntDynamicArray::~IntDynamicArray() {
    if (data_ != nullptr)
        delete[] data_;
}


void IntDynamicArray::checkIndex(unsigned long long index) const {
    if (index >= size_)
        throw IntDynamicArrayException(INCORRECT_INDEX);
}


void IntDynamicArray::shrinkIfNeeded() {
    if (size_ < 0.01 * capacity_)
        shrink(capacity_ - size_ - DEFAULT_SIZE);
    
    else if (capacity_ > DEFAULT_SIZE && size_ < 0.25 * capacity_)
        shrink(floor(0.5 * capacity_));
    
    else if (capacity_ > DEFAULT_SIZE * DEFAULT_SIZE && size_ < 0.5 * capacity_)
        shrink(floor(0.4 * capacity_));
}


unsigned long long IntDynamicArray::size() const noexcept {
    return size_;
}


unsigned long long IntDynamicArray::capacity() const noexcept {
    return capacity_;
}


void IntDynamicArray::resize(unsigned long long newSize) {
    if (newSize > size_) {
        if (newSize <= capacity_)
            for (unsigned long long i = size_; i < newSize; i++)
                data_[i] = 0;
        
        else {
            int* newData = new int[newSize];
            
            for (unsigned long long i = 0; i < size_; i++)
                newData[i] = data_[i];
            
            for (unsigned long long i = size_; i < newSize; i++)
                newData[i] = 0;
            
            delete[] data_;
            data_ = newData;
            capacity_ = newSize;
            resizes_++;
        }
    }
    
    size_ = newSize;
}


void IntDynamicArray::reserve(unsigned long long n) {
    if (n != 0) {
        int* newData = new int[capacity_ + n];
        
        for (unsigned long long i = 0; i < size_; i++)
            newData[i] = data_[i];
        
        delete[] data_;
        data_ = newData;
        capacity_ += n;
    }
}


void IntDynamicArray::shrink(unsigned long long n) {
    if (n > capacity_)
        throw IntDynamicArrayException(INCORRECT_SHRINK);
    
    if (n != 0) {
        const unsigned long long newCapacity = capacity_ - n;
        int* newData = new int[newCapacity];
        
        if (size_ > newCapacity)
            size_ = newCapacity;
        
        for (unsigned long long i = 0; i < size_; i++)
            newData[i] = data_[i];
        
        delete[] data_;
        data_ = newData;
        capacity_ = newCapacity;
        resizes_ = 0;
    }
}


void IntDynamicArray::shrinkToSize() {
    shrink(capacity_ - size_);
}


int& IntDynamicArray::at(unsigned long long index) {
    checkIndex(index);
    
    return data_[index];
}


int IntDynamicArray::at(unsigned long long index) const {
    checkIndex(index);
    
    return data_[index];
}


void IntDynamicArray::pushBack(int number) {
    if (size_ == capacity_)
        reserve(floor(0.125 * ++resizes_ * size_ + DEFAULT_SIZE));
    
    data_[size_] = number;
    size_++;
}


void IntDynamicArray::pushBack(const int array[], unsigned long long quantity) {
    if (size_ + quantity > capacity_)
        reserve(floor(0.125 * ++resizes_ * size_ + quantity));
    
    for (unsigned long long i = 0; i < quantity; i++)
        data_[size_ + i] = array[i];
    
    size_ += quantity;
}


int IntDynamicArray::popBack(bool shrink) {
    if (size_ == 0)
        throw IntDynamicArrayException(EMPTY_ARRAY);
    
    int last = data_[--size_];
    
    if (shrink)
        shrinkIfNeeded();
    
    return last;
}


int IntDynamicArray::popBack() {
    return popBack(1);
}


void IntDynamicArray::delBack(unsigned long long quantity, bool shrink) {
    if (size_ < quantity)
        throw IntDynamicArrayException(NOT_ENOUGH_ELEMS);
    
    size_ -= quantity;
    
    if (shrink)
        shrinkIfNeeded();
}


void IntDynamicArray::delBack(unsigned long long quantity) {
    delBack(quantity, 1);
}


int& IntDynamicArray::operator [](unsigned long long index) {
    return data_[index];
}


int IntDynamicArray::operator [](unsigned long long index) const {
    return data_[index];
}


IntDynamicArray& IntDynamicArray::operator =(const IntDynamicArray& o) {
    int* temp = new int[o.capacity_];
    
    for (unsigned long long i = 0; i < o.size_; i++)
        temp[i] = o.data_[i];
    
    delete[] data_;
    data_ = temp;
    size_ = o.size_;
    capacity_ = o.capacity_;
    resizes_ = 0;
    
    return *this;
}


IntDynamicArray& IntDynamicArray::operator =(IntDynamicArray&& o) {
    std::swap(data_, o.data_);
    size_ = o.size_;
    capacity_ = o.capacity_;
    resizes_ = 0;
    
    return *this;
}


bool IntDynamicArray::operator ==(const IntDynamicArray& o) const {
    if (size_ != o.size_)
        throw IntDynamicArrayException(EQ_CHECK_FAIL);
    
    for (unsigned long long i = 0; i < size_; i++)
        if (data_[i] != o.data_[i])
            return false;
    
    return true;
}


bool IntDynamicArray::operator !=(const IntDynamicArray& o) const {
    return !operator ==(o);
}


bool IntDynamicArray::operator <(const IntDynamicArray& o) const noexcept {
    for (unsigned long long i = 0; i < std::min(size_, o.size_); i++)
        if (data_[i] < o.data_[i])
            return true;
        else if (data_[i] > o.data_[i])
            return false;
    
    return size_ < o.size_;
}


bool IntDynamicArray::operator >(const IntDynamicArray& o) const noexcept {
    for (unsigned long long i = 0; i < std::min(size_, o.size_); i++)
        if (data_[i] > o.data_[i])
            return true;
        else if (data_[i] < o.data_[i])
            return false;
    
    return size_ > o.size_;
}


bool IntDynamicArray::operator <=(const IntDynamicArray& o) const noexcept {
    return !operator >(o);
}


bool IntDynamicArray::operator >=(const IntDynamicArray& o) const noexcept {
    return !operator <(o);
}


IntDynamicArray IntDynamicArray::operator +(const IntDynamicArray& o) const {
    IntDynamicArray sum(0, 0, size_ + o.size_ + DEFAULT_SIZE);
    
    for (unsigned long long i = 0; i < size_; i++)
        sum.data_[i] = data_[i];
    
    for (unsigned long long i = 0; i < o.size_; i++)
        sum.data_[i + size_] = o.data_[i];
    
    sum.size_ = size_ + o.size_;
    
    return sum;
}


std::istream& operator >>(std::istream& is, IntDynamicArray& array) {
    unsigned long long size;
    
    is >> size;
    
    int* temp = new int[size + DEFAULT_SIZE];
    
    for (unsigned long long i = 0; i < size; i++)
        is >> temp[i];
    
    std::swap(array.data_, temp);
    delete[] temp;
    array.size_ = size;
    array.capacity_ = size + DEFAULT_SIZE;
    array.resizes_ = 0;
    
    return is;
}


std::ostream& operator <<(std::ostream& os, const IntDynamicArray& array) {
    os << "IntDynamicArray {size: " << array.size_ << ", data: [";
    
    if (array.size_ > 1) {
        for (unsigned long long i = 0; i < array.size_ - 1; i++)
            os << array.data_[i] << ", ";
        
        os << array.data_[array.size_ - 1];
    }
    
    os << "]}";
    
    return os;
}
