#ifndef constants_hpp
#define constants_hpp


// IntDynamicArray
constexpr int DEFAULT_SIZE = 16;

// IntDynamicArrayException
constexpr char INCORRECT_CAPACITY[] = "A capacity value cannot be less than a size";
constexpr char INCORRECT_RANGE[] = "A ‘from’ index must be greater than the ‘to’ one, neither of them can exceed the size";
constexpr char INCORRECT_INDEX[] = "There is no element with this index in the array";
constexpr char INCORRECT_SHRINK[] = "Quantity of data to be freed cannot be greater than allocated data";
constexpr char EMPTY_ARRAY[] = "The array is empty";
constexpr char NOT_ENOUGH_ELEMS[] = "The array does not contain enough elements";
constexpr char EQ_CHECK_FAIL[] = "Cannot check equality of arrays of different size";


#endif
