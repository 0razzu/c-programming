#ifndef hash_map_hpp
#define hash_map_hpp

#include "exceptions/hash_map_exception.hpp"
#include "exceptions/hash_map_iterator_exception.hpp"
#include <utility>


constexpr unsigned long long DEFAULT_CAPACITY = 16;


template <typename T, typename U>
struct Entry {
    T key_;
    U value_;
    
    Entry* next_;
    
    
    Entry(T key, U value) {
        key_ = key;
        value_ = value;
    }
};


template <typename T, typename U>
struct Root {
    unsigned long long size_;
    
    Entry<T, U>* first_;
    
    
    Root() {
        size_ = 0;
        first_ = nullptr;
    }
    
    
    void add(Entry<T, U>* entry) {
        entry->next_ = first_;
        first_ = entry;
        
        size_++;
    }
};


template <typename T, typename U>
class HashMap {
public:
    HashMap(unsigned long long initCapacity);
    HashMap();
    HashMap(const HashMap&);
    HashMap(HashMap&&);
    virtual ~HashMap();
    
    virtual void add(T key, U value);
    virtual bool addIfAbsent(T key, U value);
    U get(T key) const;
    virtual bool del(T key);
    
    bool contains(T key) const noexcept;
    
    unsigned long long size() const noexcept;
    bool empty() const noexcept;
    
    virtual void clear();
    
    HashMap& operator =(const HashMap&);
    HashMap& operator =(HashMap&&);
    
    class Iterator {
    public:
        Iterator(HashMap&);
        
        void start();
        void next();
        
        bool hasNext() const;
        
        T getKey() const;
        U getValue() const;
        
        U changeValue(U value);
    
    private:
        HashMap* map_;
        unsigned long long index_;
        Entry<T, U>* curr_;
    };
    
protected:
    Root<T, U>* arr_;
    unsigned long long capacity_;
    unsigned long long size_;
    
    HashMap(bool);
    unsigned long long hash(T key, unsigned long long capacity) const noexcept;
    void increase();
};


template <typename T, typename U>
HashMap<T, U>::HashMap(unsigned long long initCapacity) {
    if (initCapacity == 0)
        throw HashMapException(HASH_MAP_ZERO_CAPACITY);
    
    arr_ = new Root<T, U>[initCapacity];
    
    capacity_ = initCapacity;
    size_ = 0;
}


template <typename T, typename U>
HashMap<T, U>::HashMap(): HashMap(DEFAULT_CAPACITY) {
}


template <typename T, typename U>
HashMap<T, U>::HashMap(const HashMap<T, U>& o) {
    Entry<T, U>* o_curr;
    
    arr_ = new Root<T, U>[o.capacity_];
    
    for (unsigned long long i = 0; i < o.capacity_; i++) {
        o_curr = o.arr_[i].first_;
        
        while (o_curr) {
            arr_[i].add(new Entry<T, U>(o_curr->key_, o_curr->value_));
            
            o_curr = o_curr->next_;
        }
    }
    
    capacity_ = o.capacity_;
    size_ = o.size_;
}


template <typename T, typename U>
HashMap<T, U>::HashMap(HashMap<T, U>&& o) {
    arr_ = o.arr_;
    capacity_ = o.capacity_;
    size_ = o.size_;
    
    o.arr_ = nullptr;
    o.capacity_ = 0;
    o.size_ = 0;
}


template <typename T, typename U>
HashMap<T, U>::HashMap(bool) {
}


template <typename T, typename U>
HashMap<T, U>::~HashMap() {
    clear();
    
    delete[] arr_;
    arr_ = nullptr;
    
    capacity_ = 0;
}


template <typename T, typename U>
void HashMap<T, U>::add(T key, U value) {
    unsigned long long index = hash(key, capacity_);
    
    Entry<T, U>* curr = arr_[index].first_;
    while (curr) {
        if (curr->key_ == key) {
            curr->value_ = value;
            
            return;
        }
        
        curr = curr->next_;
    }
    
    arr_[index].add(new Entry<T, U>(key, value));
    size_++;
    
    if (arr_[index].size_ > capacity_)
        increase();
}


template <typename T, typename U>
bool HashMap<T, U>::addIfAbsent(T key, U value) {
    unsigned long long index = hash(key, capacity_);
    
    Entry<T, U>* curr = arr_[index].first_;
    while (curr) {
        if (curr->key_ == key)
            return false;
        
        curr = curr->next_;
    }
    
    arr_[index].add(new Entry<T, U>(key, value));
    size_++;
    
    if (arr_[index].size_ > capacity_)
        increase();
    
    return true;
}


template <typename T, typename U>
U HashMap<T, U>::get(T key) const {
    unsigned long long index = hash(key, capacity_);
    
    Entry<T, U>* curr = arr_[index].first_;
    while (curr) {
        if (curr->key_ == key)
            return curr->value_;
        
        curr = curr->next_;
    }
    
    throw HashMapException(HASH_MAP_NO_ENTRY);
}


template <typename T, typename U>
bool HashMap<T, U>::del(T key) {
    unsigned long long index = hash(key, capacity_);
    
    bool found = 0;
    Entry<T, U>* prev;
    Entry<T, U>* curr = arr_[index].first_;
    
    if (curr->key_ == key) {
        arr_[index].first_ = curr->next_;
        
        found = 1;
    }
    
    else
        while (curr->next_ && !found) {
            prev = curr;
            curr = curr->next_;
            
            if (curr->key_ == key) {
                prev->next_ = curr->next_;
                
                found = 1;
            }
        }
    
    if (found) {
        delete curr;
        arr_[index].size_--;
        size_--;
    }
    
    return found;
}


template <typename T, typename U>
bool HashMap<T, U>::contains(T key) const noexcept {
    unsigned long long index = hash(key, capacity_);
    
    Entry<T, U>* curr = arr_[index].first_;
    while (curr) {
        if (curr->key_ == key)
            return true;
        
        curr = curr->next_;
    }
    
    return false;
}


template <typename T, typename U>
unsigned long long HashMap<T, U>::size() const noexcept {
    return size_;
}


template <typename T, typename U>
bool HashMap<T, U>::empty() const noexcept {
    return size_ == 0;
}


template <typename T, typename U>
void HashMap<T, U>::clear() {
    Entry<T, U>* prev;
    Entry<T, U>* curr;
    
    for (unsigned long long i = 0; i < capacity_; i++) {
        curr = arr_[i].first_;
        
        while (curr) {
            prev = curr;
            curr = curr->next_;
            
            delete prev;
        }
        
        arr_[i].size_ = 0;
        arr_[i].first_ = nullptr;
    }
    
    size_ = 0;
}


template <typename T, typename U>
HashMap<T, U>& HashMap<T, U>::operator =(const HashMap<T, U>& o) {
    Entry<T, U>* o_curr;
    
    clear();
    
    if (capacity_ != o.capacity_) {
        delete[] arr_;
        
        arr_ = new Root<T, U>[o.capacity_];
        capacity_ = o.capacity_;
    }
    
    for (unsigned long long i = 0; i < o.capacity_; i++) {
        o_curr = o.arr_[i].first_;
        
        while (o_curr) {
            arr_[i].add(new Entry<T, U>(o_curr->key_, o_curr->value_));
            
            o_curr = o_curr->next_;
        }
    }
    
    size_ = o.size_;
    
    return *this;
}


template <typename T, typename U>
HashMap<T, U>& HashMap<T, U>::operator =(HashMap<T, U>&& o) {
    std::swap(arr_, o.arr_);
    std::swap(capacity_, o.capacity_);
    size_ = o.size_;
    o.size_ = 0;
    
    return *this;
}


template <typename T, typename U>
unsigned long long HashMap<T, U>::hash(T key, unsigned long long capacity) const noexcept {
    return abs(key) % capacity;
}


template <typename T, typename U>
void HashMap<T, U>::increase() {
    unsigned long long capacity = 2 * capacity_;
    Root<T, U>* arr = new Root<T, U>[capacity];
    
    Entry<T, U>* curr;
    Entry<T, U>* next;
    unsigned long long index;
    
    for (unsigned long long i = 0; i < capacity_; i++) {
        curr = arr_[i].first_;
        
        while (curr) {
            next = curr->next_;
            
            index = hash(curr->key_, capacity);
            arr[index].add(curr);
            
            curr = next;
        }
    }
    
    delete[] arr_;
    
    arr_ = arr;
    capacity_ = capacity;
}


template <typename T, typename U>
HashMap<T, U>::Iterator::Iterator(HashMap& map) {
    map_ = &map;
    
    start();
}


template <typename T, typename U>
void HashMap<T, U>::Iterator::start() {
    index_ = 0;
    curr_ = nullptr;
    
    while (!curr_ && index_ < map_->capacity_)
        curr_ = map_->arr_[index_++].first_;
    
    index_--;
}


template <typename T, typename U>
void HashMap<T, U>::Iterator::next() {
    if (!hasNext())
        throw HashMapIteratorException(HASH_MAP_IT_NO_ELEMENT);
    
    if (curr_)
        curr_ = curr_->next_;
    
    while (!curr_ && index_ < map_->capacity_ - 1)
        curr_ = map_->arr_[++index_].first_;
}


template <typename T, typename U>
bool HashMap<T, U>::Iterator::hasNext() const {
    return curr_;
}


template <typename T, typename U>
T HashMap<T, U>::Iterator::getKey() const {
    if (!hasNext())
        throw HashMapIteratorException(HASH_MAP_IT_NO_ELEMENT);
    
    return curr_->key_;
}


template <typename T, typename U>
U HashMap<T, U>::Iterator::getValue() const {
    if (!hasNext())
        throw HashMapIteratorException(HASH_MAP_IT_NO_ELEMENT);
    
    return curr_->value_;
}


template <typename T, typename U>
U HashMap<T, U>::Iterator::changeValue(U newValue) {
    if (!hasNext())
        throw HashMapIteratorException(HASH_MAP_IT_NO_ELEMENT);
    
    U currValue = curr_->value_;
    
    curr_->value_ = newValue;
    
    return currValue;
}


#endif
