#ifndef test_hash_map_hpp
#define test_hash_map_hpp

#include "../hash_map.hpp"
#include "../exceptions/hash_map_exception.hpp"
#include <cxxtest/TestSuite.h>
#include <string>
#include <utility>


const std::string ERR_NO_EXCEPTION = "Expected an exception but there was not any";


class TestHashMap: public CxxTest::TestSuite {
public:
    void testConstr() {
        HashMap<int, long> map1;
        HashMap<char, unsigned> map2(8);
        
        TS_ASSERT_EQUALS(0, map1.size());
        TS_ASSERT(map1.empty());
        
        TS_ASSERT_EQUALS(0, map2.size());
        TS_ASSERT(map2.empty());
        
        try {
            HashMap<char, unsigned> map3(0);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (HashMapException& e) {
            TS_ASSERT_SAME_DATA(HASH_MAP_ZERO_CAPACITY, e.what(), strlen(HASH_MAP_ZERO_CAPACITY));
        }
    }
    
    
    void testCopyConstr() {
        HashMap<int, int> map1;
        
        map1.add(1, 1);
        map1.add(15, 5);
        map1.add(16, 8);
        map1.add(31, 31);
        map1.add(32, 16);
        map1.add(64, 32);
        
        HashMap<int, int> map2(map1);
        
        TS_ASSERT_EQUALS(map1.size(), map2.size());
        
        TS_ASSERT_EQUALS(map1.get(1), map2.get(1));
        TS_ASSERT_EQUALS(map1.get(15), map2.get(15));
        TS_ASSERT_EQUALS(map1.get(16), map2.get(16));
        TS_ASSERT_EQUALS(map1.get(31), map2.get(31));
        TS_ASSERT_EQUALS(map1.get(32), map2.get(32));
        TS_ASSERT_EQUALS(map1.get(64), map2.get(64));
    }
    
    
    void testMoveConstr() {
        HashMap<int, int> map1;
        
        map1.add(1, 1);
        map1.add(15, 5);
        map1.add(16, 8);
        map1.add(31, 1);
        map1.add(32, 16);
        map1.add(64, 32);
        
        HashMap<int, int> map2(std::move(map1));
        
        TS_ASSERT_EQUALS(0, map1.size());
        TS_ASSERT_EQUALS(6, map2.size());
        
        TS_ASSERT_EQUALS(1, map2.get(1));
        TS_ASSERT_EQUALS(5, map2.get(15));
        TS_ASSERT_EQUALS(8, map2.get(16));
        TS_ASSERT_EQUALS(1, map2.get(31));
        TS_ASSERT_EQUALS(16, map2.get(32));
        TS_ASSERT_EQUALS(32, map2.get(64));
    }
    
    
    void testAdd1() {
        HashMap<int, char> map;
        
        map.add(0, 'a');
        map.add(1, 'b');
        map.add(-1, 'z');
        
        TS_ASSERT(!map.empty());
        TS_ASSERT_EQUALS(3, map.size());
        
        TS_ASSERT_EQUALS('a', map.get(0));
        TS_ASSERT_EQUALS('b', map.get(1));
        TS_ASSERT_EQUALS('z', map.get(-1));
    }
    
    
    void testAdd2() {
        HashMap<int, unsigned> map(2);
        
        map.add(-5, 5);
        map.add(-3, 2);
        map.add(-1, 1);
        map.add(0, 0);
        map.add(10, 3);
        map.add(1000, 4);
        
        TS_ASSERT(!map.empty());
        TS_ASSERT_EQUALS(6, map.size());
        
        TS_ASSERT_EQUALS(5, map.get(-5));
        TS_ASSERT_EQUALS(2, map.get(-3));
        TS_ASSERT_EQUALS(1, map.get(-1));
        TS_ASSERT_EQUALS(0, map.get(0));
        TS_ASSERT_EQUALS(3, map.get(10));
        TS_ASSERT_EQUALS(4, map.get(1000));
    }
    
    
    void testAdd3() {
        HashMap<char, short> map(1);
        
        map.add('a', 2);
        map.add('b', -3);
        map.add('c', 2);
        map.add('d', 10);
        map.add('0', -2);
        map.add('b', 3);
        map.add('0', -93);
        
        TS_ASSERT(!map.empty());
        TS_ASSERT_EQUALS(5, map.size());
        
        TS_ASSERT_EQUALS(2, map.get('a'));
        TS_ASSERT_EQUALS(3, map.get('b'));
        TS_ASSERT_EQUALS(2, map.get('c'));
        TS_ASSERT_EQUALS(10, map.get('d'));
        TS_ASSERT_EQUALS(-93, map.get('0'));
    }
    
    
    void testAddIfAbsent1() {
        HashMap<short, char> map;
        
        map.add(1, 'a');
        map.add(-1, 'b');
        TS_ASSERT(map.addIfAbsent(0, '+'));
        TS_ASSERT(map.addIfAbsent(10, ')'));
        TS_ASSERT(!map.addIfAbsent(1, 'c'));
        TS_ASSERT(!map.addIfAbsent(10, '('));
        
        TS_ASSERT(!map.empty());
        TS_ASSERT_EQUALS(4, map.size());
        
        TS_ASSERT_EQUALS('a', map.get(1));
        TS_ASSERT_EQUALS('b', map.get(-1));
        TS_ASSERT_EQUALS('+', map.get(0));
        TS_ASSERT_EQUALS(')', map.get(10));
    }
    
    
    void testAddIfAbsent2() {
        HashMap<char, short> map(1);
        
        map.add('a', 2);
        map.add('b', -3);
        TS_ASSERT(map.addIfAbsent('c', 2));
        TS_ASSERT(map.addIfAbsent('0', -2));
        TS_ASSERT(!map.addIfAbsent('b', 3));
        TS_ASSERT(!map.addIfAbsent('0', -93));
        
        TS_ASSERT(!map.empty());
        TS_ASSERT_EQUALS(4, map.size());
        
        TS_ASSERT_EQUALS(2, map.get('a'));
        TS_ASSERT_EQUALS(-3, map.get('b'));
        TS_ASSERT_EQUALS(2, map.get('c'));
        TS_ASSERT_EQUALS(-2, map.get('0'));
    }
    
    
    void testGet() {
        HashMap<int, double> map;
        
        map.add(1, 0.93);
        
        TS_ASSERT_THROWS_NOTHING(map.get(1));
        
        try {
            map.get(2);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (HashMapException& e) {
            TS_ASSERT_SAME_DATA(HASH_MAP_NO_ENTRY, e.what(), strlen(HASH_MAP_NO_ENTRY));
        }
    }
    
    
    void testDel() {
        HashMap<int, int> map;
        
        map.add(1, 2);
        map.add(2, -2);
        map.add(15, -15);
        map.add(16, 3);
        map.add(18, 9);
        map.add(31, 8);
        map.add(32, 4);
        map.add(64, 2);
        
        map.del(1);
        map.del(15);
        map.del(18);
        map.del(32);
        
        TS_ASSERT_EQUALS(4, map.size());
        
        TS_ASSERT(!map.contains(1));
        TS_ASSERT_EQUALS(-2, map.get(2));
        TS_ASSERT(!map.contains(15));
        TS_ASSERT_EQUALS(3, map.get(16));
        TS_ASSERT(!map.contains(18));
        TS_ASSERT_EQUALS(8, map.get(31));
        TS_ASSERT(!map.contains(32));
        TS_ASSERT_EQUALS(2, map.get(64));
    }
    
    
    void testContains() {
        HashMap<int, double> map;
        
        map.add(1, -0.93);
        
        TS_ASSERT(map.contains(1));
        TS_ASSERT(!map.contains(0));
    }
    
    
    void testClear() {
        HashMap<int, int> map;
        
        map.add(1, 1);
        map.add(15, 5);
        map.add(16, 8);
        map.add(31, 31);
        map.add(32, 16);
        map.add(64, 32);
        
        map.clear();
        
        TS_ASSERT(map.empty());
    }
    
    
    void testOperatorAssign() {
        HashMap<int, int> map1;
        HashMap<int, int> map2;
        
        map1.add(1, 1);
        map1.add(15, 5);
        map1.add(16, 8);
        map1.add(31, 31);
        map1.add(32, 16);
        map1.add(64, 32);
        
        map2.add(1, 1);
        map2.add(16, 9);
        map2.add(17, 3);
        
        map2 = map1;
        
        TS_ASSERT_EQUALS(map1.size(), map2.size());
        
        TS_ASSERT_EQUALS(map1.get(1), map2.get(1));
        TS_ASSERT_EQUALS(map1.get(15), map2.get(15));
        TS_ASSERT_EQUALS(map1.get(16), map2.get(16));
        TS_ASSERT(!map2.contains(17));
        TS_ASSERT_EQUALS(map1.get(31), map2.get(31));
        TS_ASSERT_EQUALS(map1.get(32), map2.get(32));
        TS_ASSERT_EQUALS(map1.get(64), map2.get(64));
    }
    
    
    void testOperatorMove() {
        HashMap<int, int> map1;
        HashMap<int, int> map2;
        
        map1.add(1, 1);
        map1.add(15, 5);
        map1.add(16, 8);
        map1.add(31, 31);
        map1.add(32, 16);
        map1.add(64, 32);
        
        map2.add(1, 1);
        map2.add(16, 9);
        map2.add(17, 3);
        
        map2 = std::move(map1);
        
        TS_ASSERT_EQUALS(0, map1.size());
        TS_ASSERT_EQUALS(6, map2.size());
        
        TS_ASSERT_EQUALS(1, map2.get(1));
        TS_ASSERT_EQUALS(5, map2.get(15));
        TS_ASSERT_EQUALS(8, map2.get(16));
        TS_ASSERT(!map2.contains(17));
        TS_ASSERT_EQUALS(31, map2.get(31));
        TS_ASSERT_EQUALS(16, map2.get(32));
        TS_ASSERT_EQUALS(32, map2.get(64));
    }
};


#endif
