#ifndef test_linked_hash_map_iterator_hpp
#define test_linked_hash_map_iterator_hpp

#include "../linked_hash_map.hpp"
#include "../exceptions/hash_map_exception.hpp"
#include "../exceptions/hash_map_iterator_exception.hpp"
#include <cxxtest/TestSuite.h>
#include <string>
#include <utility>


const std::string ERR_NO_EXCEPTION = "Expected an exception but there was not any";
constexpr double EPS = 1E-6;


class TestHashMapIterator: public CxxTest::TestSuite {
public:
    void testStart() {
        LinkedHashMap<short, char> map(2);
        
        map.add(0, 'a');
        map.add(1, 'b');
        
        LinkedHashMap<short, char>::Iterator it(map);
        
        TS_ASSERT_EQUALS(0, it.getKey());
        TS_ASSERT_EQUALS('a', it.getValue());
        
        it.start();
        
        TS_ASSERT_EQUALS(0, it.getKey());
        TS_ASSERT_EQUALS('a', it.getValue());
    }
    
    
    void testNext1() {
        LinkedHashMap<int, unsigned> map(3);
        
        map.add(-5, 5);
        map.add(-3, 2);
        map.add(-1, 1);
        map.add(0, 0);
        map.add(10, 3);
        map.add(1000, 4);
        
        LinkedHashMap<int, unsigned>::Iterator it(map);
        
        TS_ASSERT_EQUALS(-5, it.getKey());
        TS_ASSERT_EQUALS(5, it.getValue());
        it.next();
        
        TS_ASSERT_EQUALS(-3, it.getKey());
        TS_ASSERT_EQUALS(2, it.getValue());
        it.next();
        
        TS_ASSERT_EQUALS(-1, it.getKey());
        TS_ASSERT_EQUALS(1, it.getValue());
        it.next();
        
        TS_ASSERT_EQUALS(0, it.getKey());
        TS_ASSERT_EQUALS(0, it.getValue());
        it.next();
        
        TS_ASSERT_EQUALS(10, it.getKey());
        TS_ASSERT_EQUALS(3, it.getValue());
        it.next();
        
        TS_ASSERT_EQUALS(1000, it.getKey());
        TS_ASSERT_EQUALS(4, it.getValue());
        TS_ASSERT_THROWS_NOTHING(it.next());
        
        try {
            it.next();
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (HashMapIteratorException& e) {
            TS_ASSERT_SAME_DATA(HASH_MAP_IT_NO_ELEMENT, e.what(), strlen(HASH_MAP_IT_NO_ELEMENT));
        }
    }
    
    
    void testNext2() {
        LinkedHashMap<int, int> map(3);
        
        map.add(-5, 5);
        map.add(-3, 2);
        map.add(-1, 1);
        map.add(0, 0);
        map.add(1, 3);
        map.add(10, 4);
        
        LinkedHashMap<int, int>::Iterator it(map);
        
        map.del(-3);
        map.del(1);
        map.add(-5, -5);
        map.add(4, 11);
        
        it.start();
        
        TS_ASSERT_EQUALS(-1, it.getKey());
        TS_ASSERT_EQUALS(1, it.getValue());
        it.next();
        
        TS_ASSERT_EQUALS(0, it.getKey());
        TS_ASSERT_EQUALS(0, it.getValue());
        it.next();
        
        TS_ASSERT_EQUALS(10, it.getKey());
        TS_ASSERT_EQUALS(4, it.getValue());
        it.next();
        
        TS_ASSERT_EQUALS(-5, it.getKey());
        TS_ASSERT_EQUALS(-5, it.getValue());
        it.next();
        
        TS_ASSERT_EQUALS(4, it.getKey());
        TS_ASSERT_EQUALS(11, it.getValue());
        TS_ASSERT_THROWS_NOTHING(it.next());
    }
    
    
    void testHasNext() {
        LinkedHashMap<char, char> map(2);
        
        map.add('a', 'b');
        map.add('b', 'a');
        map.add('c', 'a');
        
        LinkedHashMap<char, char>::Iterator it(map);
        
        TS_ASSERT(it.hasNext());
        it.next();
        TS_ASSERT(it.hasNext());
        it.next();
        TS_ASSERT(it.hasNext());
        it.next();
        TS_ASSERT(!it.hasNext());
    }
    
    
    void testGetKeyGetValueExceptions() {
        LinkedHashMap<int, short> map;
        LinkedHashMap<int, short>::Iterator it(map);
        
        map.add(1, 1);
        map.add(2, 2);
        
        it.start();
        it.next();
        it.next();
        
        try {
            it.getKey();
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (HashMapIteratorException& e) {
            TS_ASSERT_SAME_DATA(HASH_MAP_IT_NO_ELEMENT, e.what(), strlen(HASH_MAP_IT_NO_ELEMENT));
        }
        
        try {
            it.getValue();
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (HashMapIteratorException& e) {
            TS_ASSERT_SAME_DATA(HASH_MAP_IT_NO_ELEMENT, e.what(), strlen(HASH_MAP_IT_NO_ELEMENT));
        }
        
        map.clear();
        it.start();
        
        try {
            it.getKey();
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (HashMapIteratorException& e) {
            TS_ASSERT_SAME_DATA(HASH_MAP_IT_NO_ELEMENT, e.what(), strlen(HASH_MAP_IT_NO_ELEMENT));
        }
        
        try {
            it.getValue();
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (HashMapIteratorException& e) {
            TS_ASSERT_SAME_DATA(HASH_MAP_IT_NO_ELEMENT, e.what(), strlen(HASH_MAP_IT_NO_ELEMENT));
        }
    }
    
    
    void testChangeValue1() {
        LinkedHashMap<int, double> map(5);
        LinkedHashMap<int, double>::Iterator it(map);
        
        map.add(-5, 6);
        map.add(-1, 9.5);
        map.add(0, 4);
        map.add(2, -1);
        map.add(3, 0.15);
        
        it.start();
        
        TS_ASSERT_DELTA(6, it.changeValue(5), EPS);
        it.next();
        it.next();
        TS_ASSERT_DELTA(4, it.changeValue(0.5), EPS);
        it.next();
        it.next();
        TS_ASSERT_DELTA(0.15, it.changeValue(0.125), EPS);
        
        it.start();
        
        TS_ASSERT_DELTA(5, it.getValue(), EPS);
        it.next();
        TS_ASSERT_DELTA(9.5, it.getValue(), EPS);
        it.next();
        TS_ASSERT_DELTA(0.5, it.getValue(), EPS);
        it.next();
        TS_ASSERT_DELTA(-1, it.getValue(), EPS);
        it.next();
        TS_ASSERT_DELTA(0.125, it.getValue(), EPS);
        it.next();
        
        try {
            it.changeValue(1);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (HashMapIteratorException& e) {
            TS_ASSERT_SAME_DATA(HASH_MAP_IT_NO_ELEMENT, e.what(), strlen(HASH_MAP_IT_NO_ELEMENT));
        }
    }
    
    
    void testChangeValue2() {
        LinkedHashMap<int, double> map;
        LinkedHashMap<int, double>::Iterator it(map);
        
        map.add(-5, 6);
        map.add(-4, 5);
        map.add(-3, 6);
        map.add(-2, 7);
        map.add(-1, 50);
        map.add(0, 4);
        map.add(1, 11);
        map.add(2, -1);
        map.add(3, 0.15);
        map.add(4, 4);
        map.add(5, 8);
        
        it.start();
        
        while (it.hasNext()) {
            it.changeValue(it.getKey() / 4.);
            it.next();
        }
        
        it.start();
        
        while (it.hasNext()) {
            TS_ASSERT_DELTA(it.getKey() / 4., it.getValue(), EPS);
            it.next();
        }
    }
};


#endif
