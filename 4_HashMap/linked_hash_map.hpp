#ifndef linked_hash_map_hpp
#define linked_hash_map_hpp

#include "hash_map.hpp"
#include "exceptions/hash_map_iterator_exception.hpp"
#include <utility>


template <typename T, typename U>
struct LinkedList {
    Entry<T, U>* first_;
    Entry<T, U>* last_;
    
    
    LinkedList();
    ~LinkedList();
    
    void add(Entry<T, U>* entry);
    void changeValue(T key, U value);
    void del(T key);
    
    void clear();
};


template <typename T, typename U>
class LinkedHashMap: public HashMap<T, U> {
public:
    LinkedHashMap(unsigned long long initCapacity);
    LinkedHashMap();
    LinkedHashMap(const LinkedHashMap&);
    LinkedHashMap(LinkedHashMap&&);
    virtual ~LinkedHashMap();
    
    virtual void add(T key, U value);
    virtual bool addIfAbsent(T key, U value);
    virtual bool del(T key);
    
    virtual void clear();
    
    LinkedHashMap& operator =(const LinkedHashMap&);
    LinkedHashMap& operator =(LinkedHashMap&&);
    
    class Iterator {
    public:
        Iterator(LinkedHashMap&);
        
        void start();
        void next();
        
        bool hasNext() const;
        
        T getKey() const;
        U getValue() const;
        
        U changeValue(U value);
    
    private:
        LinkedHashMap* map_;
        Entry<T, U>* curr_;
    };
    
protected:
    LinkedList<T, U> list_;
};


template <typename T, typename U>
LinkedList<T, U>::LinkedList() {
    first_ = nullptr;
    last_ = nullptr;
}


template <typename T, typename U>
LinkedList<T, U>::~LinkedList() {
    clear();
}


template <typename T, typename U>
void LinkedList<T, U>::add(Entry<T, U>* entry) {
    entry->next_ = nullptr;
    
    if (!first_) {
        first_ = entry;
        last_ = first_;
    }
    
    else {
        last_->next_ = entry;
        last_ = last_->next_;
    }
}


template <typename T, typename U>
void LinkedList<T, U>::changeValue(T key, U value) {
    Entry<T, U>* curr = first_;
        
    while (curr && curr->key_ != key)
        curr = curr->next_;
            
    if (curr)
        curr->value_ = value;
}


template <typename T, typename U>
void LinkedList<T, U>::del(T key) {
    if (first_) {
        Entry<T, U>* next;
        
        if (first_->key_ == key) {
            next = first_->next_;
            
            delete first_;
            
            first_ = next;
            
            if (!first_)
                last_ = nullptr;
        }
        
        else {
            Entry<T, U>* prev = first_;
            Entry<T, U>* curr = first_->next_;
            
            while (curr && curr->key_ != key) {
                prev = curr;
                curr = curr->next_;
            }
            
            if (curr) {
                next = curr->next_;
                
                delete curr;
                
                prev->next_ = next;
            }
        }
    }
}


template <typename T, typename U>
void LinkedList<T, U>::clear() {
    if (first_) {
        Entry<T, U>* prev;
        Entry<T, U>* curr = first_;
        
        while (curr) {
            prev = curr;
            curr = curr->next_;
            
            delete prev;
        }
        
        first_ = nullptr;
        last_ = nullptr;
    }
}


template <typename T, typename U>
LinkedHashMap<T, U>::LinkedHashMap(unsigned long long initCapacity): HashMap<T, U>::HashMap(initCapacity) {
}


template <typename T, typename U>
LinkedHashMap<T, U>::LinkedHashMap() {
}


template <typename T, typename U>
LinkedHashMap<T, U>::~LinkedHashMap() {
}


template <typename T, typename U>
LinkedHashMap<T, U>::LinkedHashMap(const LinkedHashMap<T, U>& o): HashMap<T, U>::HashMap(false) {
    Entry<T, U>* o_curr;
    
    HashMap<T, U>::arr_ = new Root<T, U>[o.capacity_];
    
    for (unsigned long long i = 0; i < o.capacity_; i++) {
        o_curr = o.arr_[i].first_;
        
        while (o_curr) {
            HashMap<T, U>::arr_[i].add(new Entry<T, U>(o_curr->key_, o_curr->value_));
            
            o_curr = o_curr->next_;
        }
    }
    
    HashMap<T, U>::capacity_ = o.capacity_;
    HashMap<T, U>::size_ = o.size_;
    
    o_curr = o.list_.first_;
    Entry<T, U>* curr;
    
    while (o_curr) {
        curr = new Entry<T, U>(o_curr->key_, o_curr->value_);
        
        list_.add(curr);
        
        o_curr = o_curr->next_;
    }
}


template <typename T, typename U>
LinkedHashMap<T, U>::LinkedHashMap(LinkedHashMap<T, U>&& o): HashMap<T, U>::HashMap(false) {
    HashMap<T, U>::arr_ = o.arr_;
    HashMap<T, U>::capacity_ = o.capacity_;
    HashMap<T, U>::size_ = o.size_;
    list_.first_ = o.list_.first_;
    list_.last_ = o.list_.last_;
    
    o.arr_ = nullptr;
    o.capacity_ = 0;
    o.size_ = 0;
    o.list_.first_ = nullptr;
    o.list_.last_ = nullptr;
}


template <typename T, typename U>
void LinkedHashMap<T, U>::add(T key, U value) {
    list_.del(key);
    list_.add(new Entry<T, U>(key, value));
    
    HashMap<T, U>::add(key, value);
}


template <typename T, typename U>
bool LinkedHashMap<T, U>::addIfAbsent(T key, U value) {
    unsigned long long index = HashMap<T, U>::hash(key, HashMap<T, U>::capacity_);
    
    Entry<T, U>* curr = HashMap<T, U>::arr_[index].first_;
    while (curr) {
        if (curr->key_ == key)
            return false;
        
        curr = curr->next_;
    }
    
    HashMap<T, U>::add(key, value);
    list_.add(new Entry<T, U>(key, value));
    
    return true;
}


template <typename T, typename U>
bool LinkedHashMap<T, U>::del(T key) {
    unsigned long long sizeBefore = HashMap<T, U>::size_;
    
    HashMap<T, U>::del(key);
    
    if (sizeBefore > HashMap<T, U>::size_)
        list_.del(key);
    
    return sizeBefore > HashMap<T, U>::size_;
}


template <typename T, typename U>
void LinkedHashMap<T, U>::clear() {
    HashMap<T, U>::clear();
    list_.clear();
}


template <typename T, typename U>
LinkedHashMap<T, U>& LinkedHashMap<T, U>::operator =(const LinkedHashMap<T, U>& o) {
    clear();
    
    if (HashMap<T, U>::capacity_ != o.capacity_) {
        delete[] HashMap<T, U>::arr_;
        
        HashMap<T, U>::arr_ = new Root<T, U>[o.capacity_];
        HashMap<T, U>::capacity_ = o.capacity_;
    }
    
    Entry<T, U>* o_curr;
    
    for (unsigned long long i = 0; i < o.capacity_; i++) {
        o_curr = o.arr_[i].first_;
        
        while (o_curr) {
            HashMap<T, U>::arr_[i].add(new Entry<T, U>(o_curr->key_, o_curr->value_));
            
            o_curr = o_curr->next_;
        }
    }
    
    HashMap<T, U>::size_ = o.size_;
    
    o_curr = o.list_.first_;
    Entry<T, U>* curr;
    
    while (o_curr) {
        curr = new Entry<T, U>(o_curr->key_, o_curr->value_);
        
        list_.add(curr);
        
        o_curr = o_curr->next_;
    }
    
    return *this;
}


template <typename T, typename U>
LinkedHashMap<T, U>& LinkedHashMap<T, U>::operator =(LinkedHashMap<T, U>&& o) {
    std::swap(HashMap<T, U>::arr_, o.arr_);
    std::swap(HashMap<T, U>::capacity_, o.capacity_);
    HashMap<T, U>::size_ = o.size_;
    o.size_ = 0;
    std::swap(list_.first_, o.list_.first_);
    std::swap(list_.last_, o.list_.last_);
    
    return *this;
}


template <typename T, typename U>
LinkedHashMap<T, U>::Iterator::Iterator(LinkedHashMap<T, U>& map) {
    map_ = &map;
    start();
}


template <typename T, typename U>
void LinkedHashMap<T, U>::Iterator::start() {
    curr_ = map_->list_.first_;
}


template <typename T, typename U>
void LinkedHashMap<T, U>::Iterator::next() {
    if (!hasNext())
        throw HashMapIteratorException(HASH_MAP_IT_NO_ELEMENT);
    
    curr_ = curr_->next_;
}


template <typename T, typename U>
bool LinkedHashMap<T, U>::Iterator::hasNext() const {
    return curr_;
}


template <typename T, typename U>
T LinkedHashMap<T, U>::Iterator::getKey() const {
    if (!hasNext())
        throw HashMapIteratorException(HASH_MAP_IT_NO_ELEMENT);
    
    return curr_->key_;
}


template <typename T, typename U>
U LinkedHashMap<T, U>::Iterator::getValue() const {
    if (!hasNext())
        throw HashMapIteratorException(HASH_MAP_IT_NO_ELEMENT);
    
    return curr_->value_;
}


template <typename T, typename U>
U LinkedHashMap<T, U>::Iterator::changeValue(U newValue) {
    if (!hasNext())
        throw HashMapIteratorException(HASH_MAP_IT_NO_ELEMENT);
    
    U currValue = curr_->value_;
    
    curr_->value_ = newValue;
    
    unsigned long long index = map_->hash(curr_->key_, map_->capacity_);
    Entry<T, U>* curr = map_->arr_[index].first_;
    
    while (curr->key_ != curr_->key_ && curr)
        curr = curr->next_;
    
    curr->value_ = newValue;
    
    return currValue;
}


#endif
