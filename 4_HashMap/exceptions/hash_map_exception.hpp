#ifndef ring_list_exception_hpp
#define ring_list_exception_hpp

#include <exception>


constexpr char HASH_MAP_ZERO_CAPACITY[] = "The capacity value cannot be zero";
constexpr char HASH_MAP_NO_ENTRY[] = "There is no entry with this key";


class HashMapException: public std::exception {
public:
    HashMapException(const char*);
    virtual const char* what() const noexcept override;
    
private:
    const char* message;
};


#endif
