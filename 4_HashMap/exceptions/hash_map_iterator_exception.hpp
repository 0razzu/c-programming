#ifndef ring_list_iterator_exception_hpp
#define ring_list_iterator_exception_hpp

#include <exception>


constexpr char HASH_MAP_IT_NO_ELEMENT[] = "The requested element doesn’t exist";


class HashMapIteratorException: public std::exception {
public:
    HashMapIteratorException(const char*);
    virtual const char* what() const noexcept override;
    
private:
    const char* message;
};


#endif
