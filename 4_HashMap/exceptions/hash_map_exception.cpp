#include "hash_map_exception.hpp"


HashMapException::HashMapException(const char* message) {
    this->message = message;
}


const char* HashMapException::what() const noexcept {
    return message;
}
