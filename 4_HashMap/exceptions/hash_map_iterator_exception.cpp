#include "hash_map_iterator_exception.hpp"


HashMapIteratorException::HashMapIteratorException(const char* message) {
    this->message = message;
}


const char* HashMapIteratorException::what() const noexcept {
    return message;
}
