#ifndef test_int_bin_tree_hpp
#define test_int_bin_tree_hpp

#include "../int_bin_tree.hpp"
#include "../exceptions/int_bin_tree_exception.hpp"
#include <cxxtest/TestSuite.h>
#include <string>
#include <vector>
#include <utility>
#include <sstream>


const std::string ERR_NO_EXCEPTION = "Expected an exception but there was not any";
constexpr double EPS = 1E-6;


class TestHashMapIterator: public CxxTest::TestSuite {
public:
    void testConstr() {
        IntBinTree tree;
        
        TS_ASSERT_EQUALS(0, tree.size());
        TS_ASSERT(tree.empty());
    }
    
    
    void testCopyConstr() {
        IntBinTree tree1;
        IntBinTree tree2;

        tree1.insert(1, {});
        tree1.insert(11, {0});
        tree1.insert(12, {1});
        tree1.insert(3, {1, 0});
        tree1.insert(4, {0, 1});
        tree1.insert(5, {1, 1});
        tree1.insert(6, {1, 1, 1});

        tree1.insert(2, {});
        tree1.insert(-1, {1, 1, 1});

        IntBinTree tree3(tree1);
        IntBinTree tree4(tree2);
        
        TS_ASSERT_EQUALS(tree1.size(), tree3.size());
        TS_ASSERT(tree1.getPath(2) == tree3.getPath(2));
        TS_ASSERT(tree1.getPath(11) == tree3.getPath(11));
        TS_ASSERT(tree1.getPath(12) == tree3.getPath(12));
        TS_ASSERT(tree1.getPath(3) == tree3.getPath(3));
        TS_ASSERT(tree1.getPath(4) == tree3.getPath(4));
        TS_ASSERT(tree1.getPath(5) == tree3.getPath(5));
        TS_ASSERT(tree1.getPath(-1) == tree3.getPath(-1));
        
        TS_ASSERT(tree4.empty());
    }
    
    
    void testMoveConstr() {
        IntBinTree tree1;
        IntBinTree tree2;
        
        const std::vector<bool> v1 = {};
        const std::vector<bool> v2 = {0};
        const std::vector<bool> v3 = {1};
        const std::vector<bool> v4 = {1, 0};
        const std::vector<bool> v5 = {0, 1};
        const std::vector<bool> v6 = {1, 1};
        const std::vector<bool> v7 = {1, 1, 1};
        const std::vector<bool> v8 = {0, 1, 0};

        tree1.insert(1, v1);
        tree1.insert(11, v2);
        tree1.insert(12, v3);
        tree1.insert(3, v4);
        tree1.insert(4, v5);
        tree1.insert(5, v6);
        tree1.insert(6, v7);
        tree1.insert(6, v8);

        tree1.insert(2, v1);
        tree1.insert(-1, v7);

        IntBinTree tree3(std::move(tree1));
        IntBinTree tree4(std::move(tree2));
        
        TS_ASSERT(tree1.empty());
        TS_ASSERT_EQUALS(8, tree3.size());
        TS_ASSERT(v1 == tree3.getPath(2));
        TS_ASSERT(v2 == tree3.getPath(11));
        TS_ASSERT(v3 == tree3.getPath(12));
        TS_ASSERT(v4 == tree3.getPath(3));
        TS_ASSERT(v5 == tree3.getPath(4));
        TS_ASSERT(v6 == tree3.getPath(5));
        TS_ASSERT(v7 == tree3.getPath(-1));
        TS_ASSERT(v8 == tree3.getPath(6));
        
        TS_ASSERT(tree2.empty());
        TS_ASSERT(tree4.empty());
    }
    
    
    void testInsertGetPath() {
        IntBinTree tree;
        
        const std::vector<bool> v0 = {};
        const std::vector<bool> v1 = {0};
        const std::vector<bool> v2 = {1};
        const std::vector<bool> v3 = {1, 1};
        const std::vector<bool> v4 = {0, 1};
        const std::vector<bool> v5 = {0, 0};
        const std::vector<bool> v6 = {0, 0, 1};
        const std::vector<bool> v7 = {0, 0, 1, 0};
        const std::vector<bool> v8 = {0, 0, 1, 1};
        
        tree.insert(0, v0);
        tree.insert(1, v1);
        tree.insert(2, v2);
        tree.insert(3, v3);
        tree.insert(4, v4);
        tree.insert(5, v5);
        tree.insert(6, v6);
        tree.insert(7, v7);
        tree.insert(8, v8);
        
        tree.insert(-2, v2);
        tree.insert(16, v8);
        
        TS_ASSERT(v0 == tree.getPath(0));
        TS_ASSERT(v1 == tree.getPath(1));
        TS_ASSERT(v2 == tree.getPath(-2));
        TS_ASSERT(v3 == tree.getPath(3));
        TS_ASSERT(v4 == tree.getPath(4));
        TS_ASSERT(v5 == tree.getPath(5));
        TS_ASSERT(v6 == tree.getPath(6));
        TS_ASSERT(v7 == tree.getPath(7));
        TS_ASSERT(v8 == tree.getPath(16));
        
        try {
            tree.getPath(-34);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (IntBinTreeException& e) {
            TS_ASSERT_SAME_DATA(INT_BIN_TREE_NO_ELEMENT, e.what(), strlen(INT_BIN_TREE_NO_ELEMENT));
        }
        
        try {
            tree.getPath(2);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (IntBinTreeException& e) {
            TS_ASSERT_SAME_DATA(INT_BIN_TREE_NO_ELEMENT, e.what(), strlen(INT_BIN_TREE_NO_ELEMENT));
        }
        
        try {
            tree.getPath(8);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (IntBinTreeException& e) {
            TS_ASSERT_SAME_DATA(INT_BIN_TREE_NO_ELEMENT, e.what(), strlen(INT_BIN_TREE_NO_ELEMENT));
        }
    }
    
    
    void testInsertExceptions() {
        IntBinTree tree;
        
        try {
            tree.insert(-2, {0});
            TS_FAIL(ERR_NO_EXCEPTION + ": -2");
        } catch (IntBinTreeException& e) {
            TS_ASSERT_SAME_DATA(INT_BIN_TREE_INCORRECT_PATH, e.what(), strlen(INT_BIN_TREE_INCORRECT_PATH));
        }
        
        tree.insert(8, {});
        tree.insert(9, {});
        
        try {
            tree.insert(-3, {0, 1});
            TS_FAIL(ERR_NO_EXCEPTION + ": -3");
        } catch (IntBinTreeException& e) {
            TS_ASSERT_SAME_DATA(INT_BIN_TREE_INCORRECT_PATH, e.what(), strlen(INT_BIN_TREE_INCORRECT_PATH));
        }
    }
    
    
    void testClear() {
        IntBinTree tree1;
        IntBinTree tree2;
        
        tree1.insert(1, {});
        tree1.insert(2, {0});
        tree1.insert(3, {0, 1});
        tree1.insert(4, {0, 0});
        tree1.insert(5, {0, 0, 0});
        tree1.insert(6, {0, 1, 0});
        tree1.insert(7, {0, 1, 1});
        tree1.insert(8, {0, 1, 1, 1});
        
        tree1.clear();
        tree2.clear();
        
        TS_ASSERT(tree1.empty());
        TS_ASSERT(tree2.empty());
    }
    
    
    void testCountEven() {
        IntBinTree tree1;
        IntBinTree tree2;
        IntBinTree tree3;
        
        tree1.insert(1, {});
        tree1.insert(-128, {1});
        tree1.insert(2, {0});
        tree1.insert(3, {0, 1});
        tree1.insert(4, {0, 0});
        tree1.insert(5, {0, 0, 0});
        tree1.insert(6, {0, 1, 0});
        tree1.insert(7, {0, 1, 1});
        tree1.insert(0, {0, 1, 1, 1});
        
        tree2.insert(7, {});
        tree2.insert(-1, {0});
        tree2.insert(3, {1});
        tree2.insert(5, {1, 0});
        tree2.insert(-5, {1, 1});
        tree2.insert(9, {1, 0, 1});
        
        TS_ASSERT_EQUALS(5, tree1.countEven());
        TS_ASSERT_EQUALS(0, tree2.countEven());
        TS_ASSERT_EQUALS(0, tree3.countEven());
    }
    
    
    void testOnlyPositive() {
        IntBinTree tree1;
        IntBinTree tree2;
        IntBinTree tree3;
        IntBinTree tree4;
        
        tree1.insert(1, {});
        tree1.insert(128, {1});
        tree1.insert(2, {0});
        tree1.insert(3, {0, 1});
        tree1.insert(4, {0, 0});
        tree1.insert(5, {0, 0, 0});
        tree1.insert(6, {0, 1, 0});
        tree1.insert(7, {0, 1, 1});
        
        tree2.insert(0, {});
        
        tree3.insert(7, {});
        tree3.insert(2, {0});
        tree3.insert(3, {1});
        tree3.insert(5, {1, 0});
        tree3.insert(-5, {1, 1});
        tree3.insert(9, {1, 0, 1});
        
        TS_ASSERT(tree1.onlyPositive());
        TS_ASSERT(!tree2.onlyPositive());
        TS_ASSERT(!tree3.onlyPositive());
        TS_ASSERT(tree4.onlyPositive());
    }
    
    
    void testDelLeaves() {
        IntBinTree tree1;
        IntBinTree tree2;
        IntBinTree tree3;
        
        const std::vector<bool> v1 = {};
        const std::vector<bool> v2 = {0};
        const std::vector<bool> v3 = {0, 1};
        const std::vector<bool> v4 = {0, 0};
        const std::vector<bool> v5 = {0, 1, 1};
        
        tree1.insert(1, v1);      //           ┌—––––– 1 ––––––┐
        tree1.insert(-28, {1});   //      ┌—–– 2 –––┐         -128
        tree1.insert(2, v2);      //   ┌– 4     ┌–– 3 ––┐
        tree1.insert(3, v3);      //   5        6     ┌ 7 ┐
        tree1.insert(4, v4);      //                  8   9
        tree1.insert(5, {0, 0, 0});
        tree1.insert(6, {0, 1, 0});
        tree1.insert(7, {0, 1, 1});
        tree1.insert(8, {0, 1, 1, 0});
        tree1.insert(9, {0, 1, 1, 1});
        
        tree2.insert(8, {});
        
        tree1.delLeaves();
        tree2.delLeaves();
        tree3.delLeaves();
        
        TS_ASSERT_EQUALS(5, tree1.size());
        TS_ASSERT(v1 == tree1.getPath(1));
        TS_ASSERT(v2 == tree1.getPath(2));
        TS_ASSERT(v3 == tree1.getPath(3));
        TS_ASSERT(v4 == tree1.getPath(4));
        TS_ASSERT(v5 == tree1.getPath(7));
        
        TS_ASSERT(tree2.empty());
        TS_ASSERT(tree3.empty());
        
        tree1.delLeaves();
        
        TS_ASSERT_EQUALS(3, tree1.size());
        TS_ASSERT(v1 == tree1.getPath(1));
        TS_ASSERT(v2 == tree1.getPath(2));
        TS_ASSERT(v3 == tree1.getPath(3));
        
        tree1.delLeaves();
        
        TS_ASSERT_EQUALS(2, tree1.size());
        TS_ASSERT(v1 == tree1.getPath(1));
        TS_ASSERT(v2 == tree1.getPath(2));
    }
    
    
    void testArithmeticMean() {
        IntBinTree tree1;
        IntBinTree tree2;
        IntBinTree tree3;
        
        const std::vector<bool> v1 = {};
        const std::vector<bool> v2 = {0};
        const std::vector<bool> v3 = {0, 1};
        const std::vector<bool> v4 = {0, 0};
        const std::vector<bool> v5 = {0, 1, 1};
        
        tree1.insert(1, v1);
        tree1.insert(-28, {1});
        tree1.insert(2, v2);
        tree1.insert(3, v3);
        tree1.insert(4, v4);
        tree1.insert(5, {0, 0, 0});
        tree1.insert(6, {0, 1, 0});
        tree1.insert(7, {0, 1, 1});
        tree1.insert(8, {0, 1, 1, 0});
        tree1.insert(9, {0, 1, 1, 1});
        
        tree2.insert(8, {});
        
        IntBinTree tree4(tree1);
        tree4.insert(0, {1});
        
        TS_ASSERT_DELTA(1.7, tree1.arithmeticMean(), EPS);
        TS_ASSERT_DELTA(8, tree2.arithmeticMean(), EPS);
        TS_ASSERT_DELTA(0, tree3.arithmeticMean(), EPS);
        TS_ASSERT_DELTA(4.5, tree4.arithmeticMean(), EPS);
    }
    
    
    void testIsBinSearchTree() {
        IntBinTree tree1;
        IntBinTree tree2;
        IntBinTree tree3;
        IntBinTree tree4;
        IntBinTree tree5;
        
        tree2.insert(2, {});
        
        tree3.insert(1, {});
        tree3.insert(-2, {0});
        tree3.insert(3, {1});
        tree3.insert(2, {1, 0});
        
        tree4.insert(1, {});
        tree4.insert(0, {0});
        tree4.insert(1, {1});
        
        tree5.insert(3, {});            //           ┌—––––– 3 ––––––┐
        tree5.insert(1, {0});           //      ┌—–– 1 –––┐     ┌—–– 11 –––┐
        tree5.insert(-8, {0, 0});       //   ┌ -8 –┐      2     4 ––┐      13
        tree5.insert(-10, {0, 0, 0});   // -10     0            ┌–– 6 ––┐
        tree5.insert(0, {0, 0, 1});     //                      5       7
        tree5.insert(2, {0, 1});
        tree5.insert(11, {1});
        tree5.insert(4, {1, 0});
        tree5.insert(6, {1, 0, 1});
        tree5.insert(5, {1, 0, 1, 0});
        tree5.insert(7, {1, 0, 1, 1});
        tree5.insert(13, {1, 1});
        tree5.insert(14, {1, 1, 1});
        
        IntBinTree tree6(tree5);
        tree6.insert(3, {1, 0, 1});
        
        TS_ASSERT(tree1.isBinSearchTree());
        TS_ASSERT(tree2.isBinSearchTree());
        TS_ASSERT(tree3.isBinSearchTree());
        TS_ASSERT(!tree4.isBinSearchTree());
        TS_ASSERT(tree5.isBinSearchTree());
        TS_ASSERT(!tree6.isBinSearchTree());
    }
    
    
    void testOperatorAssign() {
        IntBinTree tree1;
        IntBinTree tree2;
        IntBinTree tree3;
        IntBinTree tree4;

        tree1.insert(1, {});
        tree1.insert(-1, {0});
        tree1.insert(4, {0, 1});
        tree1.insert(12, {1});
        tree1.insert(3, {1, 0});
        tree1.insert(5, {1, 1});
        tree1.insert(6, {1, 1, 1});

        tree3.insert(2, {});
        
        tree4.insert(-1, {});
        tree4.insert(0, {0});
        tree4.insert(4, {0, 0});
        tree4.insert(13, {0, 1});
        tree4.insert(1, {1});

        tree3 = tree1;
        tree4 = tree2;
        
        TS_ASSERT_EQUALS(tree1.size(), tree3.size());
        TS_ASSERT(tree1.getPath(1) == tree3.getPath(1));
        TS_ASSERT(tree1.getPath(-1) == tree3.getPath(-1));
        TS_ASSERT(tree1.getPath(4) == tree3.getPath(4));
        TS_ASSERT(tree1.getPath(12) == tree3.getPath(12));
        TS_ASSERT(tree1.getPath(3) == tree3.getPath(3));
        TS_ASSERT(tree1.getPath(5) == tree3.getPath(5));
        TS_ASSERT(tree1.getPath(6) == tree3.getPath(6));
        
        TS_ASSERT(tree4.empty());
    }
    
    
    void testOperatorMove() {
        IntBinTree tree1;
        IntBinTree tree2;
        IntBinTree tree3;
        IntBinTree tree4;
        
        std::vector<bool> v1 = {};
        std::vector<bool> v2 = {0};
        std::vector<bool> v3 = {0, 1};
        std::vector<bool> v4 = {1};
        std::vector<bool> v5 = {1, 0};
        std::vector<bool> v6 = {1, 1};
        std::vector<bool> v7 = {1, 1, 1};

        tree1.insert(1, v1);
        tree1.insert(-1, v2);
        tree1.insert(4, v3);
        tree1.insert(12, v4);
        tree1.insert(3, v5);
        tree1.insert(5, v6);
        tree1.insert(6, v7);

        tree3.insert(2, {});
        
        tree4.insert(-1, {});
        tree4.insert(0, {0});
        tree4.insert(4, {0, 0});
        tree4.insert(13, {0, 1});
        tree4.insert(1, {1});

        tree3 = std::move(tree1);
        tree4 = std::move(tree2);
        
        TS_ASSERT_EQUALS(7, tree3.size());
        TS_ASSERT(v1 == tree3.getPath(1));
        TS_ASSERT(v2 == tree3.getPath(-1));
        TS_ASSERT(v3 == tree3.getPath(4));
        TS_ASSERT(v4 == tree3.getPath(12));
        TS_ASSERT(v5 == tree3.getPath(3));
        TS_ASSERT(v6 == tree3.getPath(5));
        TS_ASSERT(v7 == tree3.getPath(6));
        
        TS_ASSERT(tree4.empty());
    }
    
    
    void testOperatorOut1() {
        std::ostringstream os;
        IntBinTree tree;
        const std::string str = "\n";
        
        os << tree;
        
        TS_ASSERT_EQUALS(str, os.str());
    }
    
    
    void testOperatorOut2() {
        std::ostringstream os;
        IntBinTree tree;
        const std::string str =
            "         213\n"
            "                     -32\n"
            "                                2398\n"
            "                                9140\n"
            "                 8910213\n"
            "                               -4389\n"
            "                                   1\n";
        
        tree.insert(213, {});        //         ┌—–––––– 213 –––––––┐
        tree.insert(-32, {0});       //    ┌— -32 ––┐          ┌—–– 8910213 –––┐
        tree.insert(2398, {0, 0});   // 2398        9140   -4389               1
        tree.insert(9140, {0, 1});
        tree.insert(8910213, {1});
        tree.insert(-4389, {1, 0});
        tree.insert(1, {1, 1});
        
        os << tree;
        
        TS_ASSERT_EQUALS(str, os.str());
    }
    
    
    void testOperatorOut3() {
        std::ostringstream os;
        IntBinTree tree;
        const std::string str =
            "           3\n"
            "                       1\n"
            "                                  -8\n"
            "                                             -10\n"
            "                                               x\n"
            "                                   2\n"
            "                      11\n"
            "                                   4\n"
            "                                               x\n"
            "                                               6\n"
            "                                                           5\n"
            "                                                           7\n"
            "                                  13\n";
        
        tree.insert(3, {});            //           ┌—––––– 3 ––––––┐
        tree.insert(1, {0});           //      ┌—–– 1 –––┐     ┌—–– 11 –––┐
        tree.insert(-8, {0, 0});       //   ┌ -8         2     4 ––┐      13
        tree.insert(-10, {0, 0, 0});   // -10                  ┌–– 6 ––┐
        tree.insert(2, {0, 1});        //                      5       7
        tree.insert(11, {1});
        tree.insert(4, {1, 0});
        tree.insert(6, {1, 0, 1});
        tree.insert(5, {1, 0, 1, 0});
        tree.insert(7, {1, 0, 1, 1});
        tree.insert(13, {1, 1});
        
        os << tree;
        
        TS_ASSERT_EQUALS(str, os.str());
    }
};


#endif
