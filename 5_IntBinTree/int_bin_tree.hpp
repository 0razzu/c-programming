#ifndef int_bin_tree_hpp
#define int_bin_tree_hpp

#include "exceptions/int_bin_tree_exception.hpp"
#include <vector>
#include <utility>
#include <ostream>
#include <iomanip>


struct Node {
    int value_;
    Node* left_;
    Node* right_;
    
    
    Node(int value, Node* left, Node* right) {
        value_ = value;
        left_ = left;
        right_ = right;
    }
    
    
    Node(int data): Node(data, nullptr, nullptr) {
    }
};


class IntBinTree {
public:
    IntBinTree();
    IntBinTree(const IntBinTree&);
    IntBinTree(IntBinTree&&);
    ~IntBinTree();
    
    void insert(int, std::vector<bool>);
    
    unsigned long long size() const noexcept;
    bool empty() const noexcept;
    
    void clear();
    
    unsigned long long countEven() const noexcept;
    bool onlyPositive() const noexcept;
    void delLeaves();
    double arithmeticMean() const noexcept;
    std::vector<bool>& getPath(int) const;
    bool isBinSearchTree() const noexcept;
    
    IntBinTree& operator =(const IntBinTree&);
    IntBinTree& operator =(IntBinTree&&);
    friend std::ostream& operator <<(std::ostream&, const IntBinTree&);

private:
    Node* root_;
    unsigned long long size_;
    
    Node* copy(const Node*);
    void del(Node*&);
    unsigned long long countEven(const Node*) const noexcept;
    bool onlyPositive(const Node*) const noexcept;
    void delLeaves(Node*&);
    int sum(const Node*) const noexcept;
    bool getPath(const Node*, int, std::vector<bool>*) const noexcept;
    bool isBinSearchTree(const Node*, int, int) const noexcept;
};


#endif
