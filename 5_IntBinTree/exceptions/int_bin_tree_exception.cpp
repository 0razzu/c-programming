#include "int_bin_tree_exception.hpp"


IntBinTreeException::IntBinTreeException(const char* message) {
    this->message = message;
}


const char* IntBinTreeException::what() const noexcept {
    return message;
}
