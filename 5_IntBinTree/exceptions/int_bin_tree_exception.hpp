#ifndef int_bin_tree_exception_hpp
#define int_bin_tree_exception_hpp

#include <exception>


constexpr char INT_BIN_TREE_INCORRECT_PATH[] = "The provided path leads to nowhere";
constexpr char INT_BIN_TREE_NO_ELEMENT[] = "There is no element with this value";


class IntBinTreeException: public std::exception {
public:
    IntBinTreeException(const char*);
    virtual const char* what() const noexcept override;
    
private:
    const char* message;
};


#endif
