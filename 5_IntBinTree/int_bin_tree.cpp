#include "int_bin_tree.hpp"


IntBinTree::IntBinTree() {
    root_ = nullptr;
    size_ = 0;
}


IntBinTree::IntBinTree(const IntBinTree& o) {
    root_ = o.root_? IntBinTree::copy(o.root_) : nullptr;
    size_ = o.size_;
}


IntBinTree::IntBinTree(IntBinTree&& o) {
    root_ = o.root_;
    size_ = o.size_;
    
    o.root_ = nullptr;
    o.size_ = 0;
}


IntBinTree::~IntBinTree() {
    clear();
}


void IntBinTree::insert(int value, std::vector<bool> path) {
    if (!root_ && path.empty()) {
        root_ = new Node(value);
        size_ = 1;
    }
    
    else {
        bool right = 0;
        Node* prev = root_;
        Node* curr = root_;
        
        for (unsigned long long i = 0; i < path.size(); i++) {
            if (!curr)
                throw IntBinTreeException(INT_BIN_TREE_INCORRECT_PATH);
            
            right = path[i];
            prev = curr;
            curr = right? curr->right_ : curr->left_;
        }
        
        if (curr)
            curr->value_ = value;
        
        else {
            curr = new Node(value);
            
            if (right)
                prev->right_ = curr;
            
            else
                prev->left_ = curr;
            
            size_++;
        }
    }
}


unsigned long long IntBinTree::size() const noexcept {
    return size_;
}


bool IntBinTree::empty() const noexcept {
    return !size_;
}


void IntBinTree::clear() {
    if (root_)
        del(root_);
    
    size_ = 0;
}


unsigned long long IntBinTree::countEven() const noexcept {
    if (!root_)
        return 0;
    
    return countEven(root_);
}


bool IntBinTree::onlyPositive() const noexcept {
    if (!root_)
        return 1;
    
    return onlyPositive(root_);
}


void IntBinTree::delLeaves() {
    if (root_)
        delLeaves(root_);
}


double IntBinTree::arithmeticMean() const noexcept {
    if (root_)
        return static_cast<double>(sum(root_)) / size_;
    
    return 0;
}


std::vector<bool>& IntBinTree::getPath(int value) const {
    if (!root_)
        throw IntBinTreeException(INT_BIN_TREE_NO_ELEMENT);
    
    std::vector<bool>* path = new std::vector<bool>;
    
    if (root_->value_ != value)
        if (!getPath(root_, value, path))
            throw IntBinTreeException(INT_BIN_TREE_NO_ELEMENT);
    
    return *path;
}


bool IntBinTree::isBinSearchTree() const noexcept {
    if (!root_)
        return 1;
    
    return isBinSearchTree(root_, INT_MIN, INT_MAX);
}


IntBinTree& IntBinTree::operator =(const IntBinTree& o) {
    if (root_)
        del(root_);
    
    if (o.root_)
        root_ = IntBinTree::copy(o.root_);
    
    size_ = o.size_;
    
    return *this;
}


IntBinTree& IntBinTree::operator =(IntBinTree&& o) {
    std::swap(root_, o.root_);
    std::swap(size_, o.size_);
    
    return *this;
}


Node* IntBinTree::copy(const Node* node) {
    Node* copy = new Node(node->value_);
    
    if (node->left_)
        copy->left_ = IntBinTree::copy(node->left_);
    
    if (node->right_)
        copy->right_ = IntBinTree::copy(node->right_);
    
    return copy;
}


void IntBinTree::del(Node*& node) {
    if (node->left_)
        del(node->left_);
    
    if (node->right_)
        del(node->right_);
    
    delete node;
    node = nullptr;
}


unsigned long long IntBinTree::countEven(const Node* node) const noexcept {
    unsigned long long counter = 0;
    
    if (node->value_ % 2 == 0)
        counter = 1;
    
    if (node->left_)
        counter += countEven(node->left_);
    
    if (node->right_)
        counter += countEven(node->right_);
    
    return counter;
}


bool IntBinTree::onlyPositive(const Node* node) const noexcept {
    if (node->value_ <= 0)
        return 0;
    
    bool positive = 1;
    
    if (node->left_)
        positive = onlyPositive(node->left_);
    
    if (positive && node->right_)
        positive = onlyPositive(node->right_);
    
    return positive;
}


void IntBinTree::delLeaves(Node*& node) {
    if (!node->left_ && !node->right_) {
        delete node;
        node = nullptr;
        size_--;
    }
    
    else {
        if (node->left_)
            delLeaves(node->left_);
        
        if (node->right_)
            delLeaves(node->right_);
    }
}


int IntBinTree::sum(const Node* node) const noexcept {
    int s = node->value_;
    
    if (node->left_)
        s += sum(node->left_);
    
    if (node->right_)
        s += sum(node->right_);
    
    return s;
}


bool IntBinTree::getPath(const Node* node, int value, std::vector<bool>* vector) const noexcept {
    if (node->value_ == value)
        return 1;
    
    if (node->left_) {
        vector->push_back(0);
        
        if (getPath(node->left_, value, vector))
            return 1;
        
        vector->pop_back();
    }
    
    if (node->right_) {
        vector->push_back(1);
        
        if (getPath(node->right_, value, vector))
            return 1;
        
        vector->pop_back();
    }
    
    return 0;
}


bool IntBinTree::isBinSearchTree(const Node* node, int min, int max) const noexcept {
    if (node->value_ < min || node->value_ > max)
        return 0;
    
    bool res = 1;
    
    if (node->left_)
        res = isBinSearchTree(node->left_, min, node->value_ - 1);
    
    if (res && node->right_)
        res = isBinSearchTree(node->right_, node->value_ + 1, max);
    
    return res;
}


void out(std::ostream& os, const Node* node, int level) {
    os << std::setw(level * 12) << node->value_ << std::endl;
    
    level++;
    
    if (node->left_)
        out(os, node->left_, level);
    else if (node->right_)
        os << std::setw(level * 12) << 'x' << std::endl;
    
    if (node->right_)
        out(os, node->right_, level);
    else if (node->left_)
        os << std::setw(level * 12) << 'x' << std::endl;
}


std::ostream& operator <<(std::ostream& os, const IntBinTree& tree) {
    if (tree.root_)
        out(os, tree.root_, 1);
    
    else
        os << std::endl;
    
    return os;
}
