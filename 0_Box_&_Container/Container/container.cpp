#include "container.hpp"


boxes::Container::Container(int length, int width, int height, double maxWeight) {
    setLength(length);
    setWidth(width);
    setHeight(height);
    setMaxWeight(maxWeight);
    weight = 0;
}


boxes::Container::Container(const Container& o) {
    boxes = o.boxes;
    setLength(o.length);
    setWidth(o.width);
    setHeight(o.height);
    setMaxWeight(o.maxWeight);
    weight = o.weight;
}


void boxes::Container::setLength(int length) {
    if (length < 0)
        throw ContainerException(ERR_CONT_NEGATIVE_FIELD);
    
    this->length = length;
}


void boxes::Container::setWidth(int width) {
    if (width < 0)
        throw ContainerException(ERR_CONT_NEGATIVE_FIELD);
    
    this->width = width;
}


 void boxes::Container::setHeight(int height) {
     if (height < 0)
         throw ContainerException(ERR_CONT_NEGATIVE_FIELD);
     
     this->height = height;
 }


void boxes::Container::setMaxWeight(double maxWeight) {
    if (double_comparator::compare(maxWeight, 0) <= 0)
        throw ContainerException(ERR_CONT_NEGATIVE_FIELD);
    
    this->maxWeight = maxWeight;
}


int boxes::Container::getLength() const noexcept {
    return length;
}


int boxes::Container::getWidth() const noexcept {
    return width;
}


int boxes::Container::getHeight() const noexcept {
    return height;
}


double boxes::Container::getMaxWeight() const noexcept {
    return maxWeight;
}


unsigned long boxes::Container::getBoxQuantity() const noexcept {
    return boxes.size();
}


double boxes::Container::getWeight() const noexcept {
    return weight;
}


int boxes::Container::getValue() const noexcept {
    int value = 0;
    
    for (const Box& box: boxes)
        value += box.getValue();
    
    return value;
}


unsigned long boxes::Container::addBox(boxes::Box& box) {
    if (double_comparator::compare(weight + box.getWeight(), maxWeight) > 0)
        throw ContainerException(ERR_CONT_TOO_HEAVY);
    
    boxes.push_back(box);
    weight += box.getWeight();
    
    return boxes.size() - 1;
}


boxes::Box& boxes::Container::getBox(unsigned long i) {
    try {
        return boxes.at(i);
    } catch (std::out_of_range e) {
        throw ContainerException(ERR_CONT_INCORRECT_INDEX);
    }
}


const boxes::Box& boxes::Container::getBox(unsigned long i) const {
    try {
        return boxes.at(i);
    } catch (std::out_of_range e) {
        throw ContainerException(ERR_CONT_INCORRECT_INDEX);
    }
}


void boxes::Container::delBox(unsigned long i) {
    if (i < 0 || i >= boxes.size())
        throw ContainerException(ERR_CONT_INCORRECT_INDEX);
    
    std::vector<Box>::iterator it = boxes.begin() + i;
    
    weight -= it[0].getWeight();
    
    boxes.erase(it);
}


boxes::Container& boxes::Container::operator =(const boxes::Container& o) {
    boxes = o.boxes;
    length = o.length;
    width = o.width;
    height = o.height;
    maxWeight = o.maxWeight;
    weight = o.weight;
    
    return *this;
}


boxes::Box& boxes::Container::operator [](unsigned long i) {
    return boxes[i];
}


const boxes::Box& boxes::Container::operator [](unsigned long i) const {
    return boxes[i];
}


std::istream& boxes::operator >>(std::istream& is, boxes::Container& container) {
    int length, width, height;
    double maxWeight;
    
    is >> length >> width >> height >> maxWeight;
    
    container.boxes.clear();
    container.weight = 0;
    
    container.setLength(length);
    container.setWidth(width);
    container.setHeight(height);
    container.setMaxWeight(maxWeight);
    
    return is;
}


std::ostream& boxes::operator <<(std::ostream& os, const boxes::Container& container) {
    unsigned long quantity = container.getBoxQuantity();
    
    os << "Container {length: " << container.length <<
                   ", width: " << container.width <<
                   ", height: " << container.height <<
                   ", maxWeight: " << container.maxWeight <<
                   ", boxQuantity: " << quantity <<
                   ", value: " << container.getValue() <<
                   ", weight: " << container.getWeight() <<
                   ", boxes: [";
    
    if (quantity > 0) {
        for (unsigned long i = 0; i < quantity - 1; i++)
            os << std::endl << container.getBox(i) << ",";
        
        os << std::endl << container.getBox(quantity - 1);
    }
    
    os << "]}";
    
    return os;
}
