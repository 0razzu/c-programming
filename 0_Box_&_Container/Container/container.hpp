#ifndef container_hpp
#define container_hpp

#include "constants.hpp"
#include "exception/container_exception.hpp"
#include "box.hpp"
#include "common/double_comparator.hpp"
#include <vector>
#include <iostream>


namespace boxes {
    class Container {
    public:
        Container(int length, int width, int height, double maxWeight);
        Container(const Container&);
        
        void setLength(int);
        void setWidth(int);
        void setHeight(int);
        void setMaxWeight(double);
        
        int getLength() const noexcept;
        int getWidth() const noexcept;
        int getHeight() const noexcept;
        double getMaxWeight() const noexcept;
        
        unsigned long getBoxQuantity() const noexcept;
        double getWeight() const noexcept;
        int getValue() const noexcept;
        
        unsigned long addBox(Box&);
        Box& getBox(unsigned long);
        const Box& getBox(unsigned long) const;
        void delBox(unsigned long);
        
        Container& operator =(const Container&);
        Box& operator [](unsigned long);
        const Box& operator [](unsigned long) const;
        
        friend std::istream& operator >>(std::istream&, Container&);
        friend std::ostream& operator <<(std::ostream&, const Container&);
        
    private:
        std::vector<Box> boxes;
        int length;
        int width;
        int height;
        double maxWeight;
        double weight;
    };
}


#endif
