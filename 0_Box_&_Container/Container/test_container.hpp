#ifndef test_container_h
#define test_container_h

#include "test_constants.hpp"
#include "constants.hpp"
#include "box.hpp"
#include "container.hpp"
#include "exception/container_exception.hpp"
#include "common/double_comparator.hpp"
#include <cxxtest/TestSuite.h>
#include <string>
#include <sstream>


class TestContainer: public CxxTest::TestSuite {
public:
    void testConstr() {
        boxes::Container container(1, 2, 3, 4.5);
        
        TS_ASSERT_EQUALS(1, container.getLength());
        TS_ASSERT_EQUALS(2, container.getWidth());
        TS_ASSERT_EQUALS(3, container.getHeight());
        TS_ASSERT_DELTA(4.5, container.getMaxWeight(), EPS);
        TS_ASSERT_DELTA(0, container.getWeight(), EPS);
        TS_ASSERT_EQUALS(0, container.getValue());
    }
    
    
    void testConstrExceptions() {
        try {
            boxes::Container container1(-1, 1, 1, 1);
            TS_FAIL("container1: " + ERR_NO_EXCEPTION);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_NEGATIVE_FIELD, e.what(), strlen(ERR_CONT_NEGATIVE_FIELD));
        }
        
        try {
            boxes::Container container2(1, -1, 1, 1);
            TS_FAIL("container2: " + ERR_NO_EXCEPTION);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_NEGATIVE_FIELD, e.what(), strlen(ERR_CONT_NEGATIVE_FIELD));
        }
        
        try {
            boxes::Container container3(1, 1, -1, 1);
            TS_FAIL("container3: " + ERR_NO_EXCEPTION);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_NEGATIVE_FIELD, e.what(), strlen(ERR_CONT_NEGATIVE_FIELD));
        }
        
        try {
            boxes::Container container4(1, 1, 1, -1);
            TS_FAIL("container4: " + ERR_NO_EXCEPTION);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_NEGATIVE_FIELD, e.what(), strlen(ERR_CONT_NEGATIVE_FIELD));
        }
    }
    
    
    void testCopyConstr1() {
        boxes::Container container1(123, 4, 23, 423.129);
        boxes::Container container2(container1);
        
        TS_ASSERT_EQUALS(container1.getLength(), container2.getLength());
        TS_ASSERT_EQUALS(container1.getWidth(), container2.getWidth());
        TS_ASSERT_EQUALS(container1.getHeight(), container2.getHeight());
        TS_ASSERT_DELTA(container1.getMaxWeight(), container2.getMaxWeight(), EPS);
        TS_ASSERT_DELTA(container1.getWeight(), container2.getWeight(), EPS);
        TS_ASSERT_EQUALS(container1.getValue(), container2.getValue());
    }
    
    
    void testCopyConstr2() {
        boxes::Container container1(34, 74, 78, 2345.46);
        
        boxes::Box box1(1, 1, 1, 10.1, 10000);
        boxes::Box box2(23, 32, 31, 3, 4230);
        
        container1.addBox(box1);
        container1.addBox(box2);
        
        boxes::Container container2(container1);
        
        TS_ASSERT_EQUALS(container1.getLength(), container2.getLength());
        TS_ASSERT_EQUALS(container1.getWidth(), container2.getWidth());
        TS_ASSERT_EQUALS(container1.getHeight(), container2.getHeight());
        TS_ASSERT_DELTA(container1.getMaxWeight(), container2.getMaxWeight(), EPS);
        TS_ASSERT_DELTA(container1.getWeight(), container2.getWeight(), EPS);
        TS_ASSERT_EQUALS(container1.getValue(), container2.getValue());
        
        container1.addBox(box1);
        
        TS_ASSERT_LESS_THAN(0, double_comparator::compare(container1.getWeight(), container2.getWeight()));
        TS_ASSERT_LESS_THAN(container2.getValue(), container1.getValue());
    }
    
    
    void testSetters() {
        boxes::Container container(1, 2, 3, 4);
        
        container.setLength(10);
        container.setWidth(11);
        container.setHeight(312);
        container.setMaxWeight(1500);
        
        TS_ASSERT_EQUALS(10, container.getLength());
        TS_ASSERT_EQUALS(11, container.getWidth());
        TS_ASSERT_EQUALS(312, container.getHeight());
        TS_ASSERT_DELTA(1500, container.getMaxWeight(), EPS);
        TS_ASSERT_DELTA(0, container.getWeight(), EPS);
        TS_ASSERT_EQUALS(0, container.getValue());
    }
    
    
    void testPrimitiveValuedGetters() {
        boxes::Container container(10, 10, 10, 32);
        
        TS_ASSERT_EQUALS(0, container.getBoxQuantity());
        TS_ASSERT_DELTA(0, container.getWeight(), EPS);
        TS_ASSERT_EQUALS(0, container.getValue());
        
        boxes::Box box1(2, 1, 3, 10.1, 1000);
        boxes::Box box2(3, 2, 1, 3, 4230);
        
        container.addBox(box1);
        container.addBox(box2);
        
        TS_ASSERT_EQUALS(2, container.getBoxQuantity());
        TS_ASSERT_DELTA(13.1, container.getWeight(), EPS);
        TS_ASSERT_EQUALS(5230, container.getValue());
        
        container.addBox(box2);
        
        TS_ASSERT_EQUALS(3, container.getBoxQuantity());
        TS_ASSERT_DELTA(16.1, container.getWeight(), EPS);
        TS_ASSERT_EQUALS(9460, container.getValue());
    }
    
    
    void testAddBox() {
        boxes::Container container(100, 200, 500, 10000);
        
        boxes::Box box1(10, 20, 30, 1, 100);
        boxes::Box box2(11, 29, 35, 1.5, 500);
        
        unsigned long box1Index = container.addBox(box1);
        unsigned long box2Index = container.addBox(box2);
        
        TS_ASSERT_EQUALS(0, box1Index);
        TS_ASSERT_EQUALS(1, box2Index);
    }
    
    
    void testAddBoxExceptions() {
        boxes::Container container1(10, 20, 30, 50);
        boxes::Container container2(10, 20, 30, 55);
        
        boxes::Box box1(1, 2, 3, 51, 1000);
        boxes::Box box2(1, 2, 3, 5, 1000);
        boxes::Box box3(1, 2, 3, 4, 1000);
        
        try {
            container1.addBox(box1);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_TOO_HEAVY, e.what(), strlen(ERR_CONT_TOO_HEAVY));
        }
        
        TS_ASSERT_THROWS_NOTHING(container2.addBox(box1));
        
        try {
            container2.addBox(box2);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_TOO_HEAVY, e.what(), strlen(ERR_CONT_TOO_HEAVY));
        }
        
        TS_ASSERT_THROWS_NOTHING(container2.addBox(box3));
    }
    
    
    void testGetBox() {
        boxes::Container container(100, 200, 500, 10000);
        
        boxes::Box box1(11, 29, 35, 1.5, 500);
        boxes::Box box2(11, 29, 35, 1.5, 100);
        
        unsigned long box1Index = container.addBox(box1);
        unsigned long box2Index = container.addBox(box2);
        
        TS_ASSERT_EQUALS(box1, container.getBox(box1Index));
        TS_ASSERT_EQUALS(box2, container.getBox(box2Index));
        
        container.getBox(box1Index).setValue(100);
        
        TS_ASSERT_EQUALS(box2, container.getBox(box1Index));
    }
    
    
    void testGetBoxExceptions() {
        boxes::Container container(100, 200, 500, 10000);
        
        boxes::Box box1(10, 20, 30, 1, 100);
        boxes::Box box2(23, 13, 78, 1.5, 500);
        
        container.addBox(box1);
        container.addBox(box2);
        
        TS_ASSERT_THROWS_NOTHING(container.getBox(0));
        TS_ASSERT_THROWS_NOTHING(container.getBox(1));
        
        try {
            boxes::Box box3 = container.getBox(-1);
            TS_FAIL("box3: " + ERR_NO_EXCEPTION);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_INCORRECT_INDEX, e.what(), strlen(ERR_CONT_INCORRECT_INDEX));
        }
        
        try {
            boxes::Box box4 = container.getBox(2);
            TS_FAIL("box4: " + ERR_NO_EXCEPTION);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_INCORRECT_INDEX, e.what(), strlen(ERR_CONT_INCORRECT_INDEX));
        }
    }
    
    
    void testDelBox() {
        boxes::Container container(100, 200, 500, 10000);
        
        boxes::Box box1(10, 20, 30, 1, 100);
        boxes::Box box2(12, 9, 5, 1.5, 500);
        boxes::Box box3(1, 1, 1, 0.3, 35);
        
        container.addBox(box1);
        container.addBox(box2);
        
        container.delBox(0);
        
        TS_ASSERT_EQUALS(box2, container.getBox(0));
        
        try {
            container.getBox(1);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_INCORRECT_INDEX, e.what(), strlen(ERR_CONT_INCORRECT_INDEX));
        }
        
        container.addBox(box3);
        
        TS_ASSERT_EQUALS(box3, container.getBox(1));
    }
    
    
    void testDelBoxExceptions() {
        boxes::Container container(10, 20, 50, 1000);
        
        boxes::Box box(10, 20, 30, 1, 100);
        
        container.addBox(box);
        
        try {
            container.delBox(-1);
            TS_FAIL("1: " + ERR_NO_EXCEPTION);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_INCORRECT_INDEX, e.what(), strlen(ERR_CONT_INCORRECT_INDEX));
        }
        
        try {
            container.getBox(1);
            TS_FAIL("2: " + ERR_NO_EXCEPTION);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_INCORRECT_INDEX, e.what(), strlen(ERR_CONT_INCORRECT_INDEX));
        }
        
        TS_ASSERT_THROWS_NOTHING(container.delBox(0));
        
        try {
            container.getBox(0);
            TS_FAIL("3: " + ERR_NO_EXCEPTION);
        } catch (ContainerException e) {
            TS_ASSERT_SAME_DATA(ERR_CONT_INCORRECT_INDEX, e.what(), strlen(ERR_CONT_INCORRECT_INDEX));
        }
    }
    
    
    void testOperatorAssign1() {
        boxes::Container container1(13, 40, 23, 34.4);
        boxes::Container container2(1, 5, 10, 100.25);
        
        container2 = container1;
        
        TS_ASSERT_EQUALS(container1.getLength(), container2.getLength());
        TS_ASSERT_EQUALS(container1.getWidth(), container2.getWidth());
        TS_ASSERT_EQUALS(container1.getHeight(), container2.getHeight());
        TS_ASSERT_DELTA(container1.getMaxWeight(), container2.getMaxWeight(), EPS);
        TS_ASSERT_DELTA(container1.getWeight(), container2.getWeight(), EPS);
        TS_ASSERT_EQUALS(container1.getValue(), container2.getValue());
    }
    
    
    void testOperatorAssign2() {
        boxes::Container container1(300, 450, 650, 3240);
        boxes::Container container2(2, 3, 6, 10);
        
        boxes::Box box1(43, 65, 18, 10.1, 10000);
        boxes::Box box2(26, 72, 31, 5, 423);
        
        container1.addBox(box1);
        container1.addBox(box2);
        
        container2 = container1;
        
        TS_ASSERT_EQUALS(container1.getLength(), container2.getLength());
        TS_ASSERT_EQUALS(container1.getWidth(), container2.getWidth());
        TS_ASSERT_EQUALS(container1.getHeight(), container2.getHeight());
        TS_ASSERT_DELTA(container1.getMaxWeight(), container2.getMaxWeight(), EPS);
        TS_ASSERT_DELTA(container1.getWeight(), container2.getWeight(), EPS);
        TS_ASSERT_EQUALS(container1.getValue(), container2.getValue());
        
        container1.delBox(0);
        
        TS_ASSERT_LESS_THAN(double_comparator::compare(container1.getWeight(), container2.getWeight()), 0);
        TS_ASSERT_LESS_THAN(container1.getValue(), container2.getValue());
    }
    
    
    void testOperatorSquareBrackets() {
        boxes::Container container(100, 200, 500, 10000);
        
        boxes::Box box1(11, 29, 35, 1.5, 500);
        boxes::Box box2(11, 29, 35, 1.5, 100);
        
        unsigned long box1Index = container.addBox(box1);
        unsigned long box2Index = container.addBox(box2);
        
        TS_ASSERT_EQUALS(box1, container[box1Index]);
        TS_ASSERT_EQUALS(box2, container[box2Index]);
        
        container[box1Index].setValue(100);
        
        TS_ASSERT_EQUALS(box2, container[box1Index]);
    }
    
    
    void testOperatorIn1() {
        std::stringstream s;
        boxes::Container container(1, 2, 3, 4);
        
        s << "10 20 15 8.125";
        s >> container;
        
        TS_ASSERT_EQUALS(10, container.getLength());
        TS_ASSERT_EQUALS(20, container.getWidth());
        TS_ASSERT_EQUALS(15, container.getHeight());
        TS_ASSERT_DELTA(8.125, container.getMaxWeight(), EPS);
        TS_ASSERT_DELTA(0, container.getWeight(), EPS);
        TS_ASSERT_EQUALS(0, container.getValue());
    }
    
    
    void testOperatorIn2() {
        std::stringstream s;
        boxes::Container container(1, 2, 3, 4);
        boxes::Box box(1, 1, 1, 1, 10);
        
        container.addBox(box);
        
        s << "10 20 15 8.125";
        s >> container;
        
        TS_ASSERT_EQUALS(10, container.getLength());
        TS_ASSERT_EQUALS(20, container.getWidth());
        TS_ASSERT_EQUALS(15, container.getHeight());
        TS_ASSERT_DELTA(8.125, container.getMaxWeight(), EPS);
        TS_ASSERT_DELTA(0, container.getWeight(), EPS);
        TS_ASSERT_EQUALS(0, container.getValue());
    }
    
    
    void testOperatorOut1() {
        std::ostringstream os;
        boxes::Container container(10, 21, 35, 400.5);
        const std::string str =
            "Container {length: 10, width: 21, height: 35,"
            " maxWeight: 400.5, boxQuantity: 0, value: 0, weight: 0,"
            " boxes: []}";
        
        os << container;
        
        TS_ASSERT_EQUALS(str, os.str());
    }
    
    
    void testOperatorOut2() {
        std::ostringstream os;
        boxes::Container container(10, 21, 35, 400.5);
        const std::string str =
            "Container {length: 10, width: 21, height: 35,"
            " maxWeight: 400.5, boxQuantity: 2, value: 1005, weight: 16.75,"
            " boxes: [\n"
            "Box {length: 5, width: 8, height: 14, weight: 3.25, value: 100},\n"
            "Box {length: 4, width: 20, height: 5, weight: 13.5, value: 905}]}";
        
        boxes::Box box1(5, 8, 14, 3.25, 100);
        boxes::Box box2(4, 20, 5, 13.5, 905);
        
        container.addBox(box1);
        container.addBox(box2);
        
        os << container;
        
        TS_ASSERT_EQUALS(str, os.str());
    }
};


#endif
