#include "box_exception.hpp"


BoxException::BoxException(const char* message) {
    this->message = message;
}


const char* BoxException::what() const noexcept {
    return message;
}
