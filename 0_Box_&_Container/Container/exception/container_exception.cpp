#include "container_exception.hpp"


ContainerException::ContainerException(const char* message) {
    this->message = message;
}


const char* ContainerException::what() const noexcept {
    return message;
}
