#ifndef container_exception_hpp
#define container_exception_hpp

#include <exception>


class ContainerException: public std::exception {
public:
    ContainerException(const char*);
    virtual const char* what() const noexcept override;
    
private:
    const char* message;
};


#endif
