#include "box.hpp"


boxes::Box::Box(unsigned length, unsigned width, unsigned height, double weight, unsigned value) {
    setLength(length);
    setWidth(width);
    setHeight(height);
    setWeight(weight);
    setValue(value);
}


boxes::Box::Box(const Box& o) {
    length = o.length;
    width = o.width;
    height = o.height;
    weight = o.weight;
    value = o.value;
}


void boxes::Box::setLength(unsigned length) {
    checkSign(length);
    
    this->length = length;
}


void boxes::Box::setWidth(unsigned width) {
    checkSign(width);
    
    this->width = width;
}


void boxes::Box::setHeight(unsigned height) {
    checkSign(height);
    
    this->height = height;
}


void boxes::Box::setWeight(double weight) {
    if (double_comparator::compare(weight, 0) < 0)
        throw BoxException(ERR_BOX_NEGATIVE_FIELD);
    
    this->weight = weight;
}


void boxes::Box::setValue(unsigned value) {
    checkSign(value);
    
    this->value = value;
}


unsigned boxes::Box::getLength() const noexcept {
    return length;
}


unsigned boxes::Box::getWidth() const noexcept {
    return width;
}


unsigned boxes::Box::getHeight() const noexcept {
    return height;
}


double boxes::Box::getWeight() const noexcept {
    return weight;
}


unsigned boxes::Box::getValue() const noexcept {
    return value;
}


long long boxes::Box::getVolume() const noexcept {
    return length * width * height;
}


void boxes::Box::rotate() {
    int t;
    
    if (length < height) {
        t = length;
        length = height;
        height = t;
    }
    
    if (length < width) {
        t = length;
        length = width;
        width = t;
    }
    
    else if (width < height) {
        t = width;
        width = height;
        height = t;
    }
}


bool boxes::Box::operator ==(const boxes::Box& o) const noexcept {
    return length == o.length &&
           width == o.width &&
           height == o.height &&
           double_comparator::equal(weight, o.weight) &&
           value == o.value;
}


bool boxes::Box::operator <(const boxes::Box& o) const noexcept {
    return length < o.length &&
        width < o.width &&
        height < o.height;
}


bool boxes::Box::operator >(const boxes::Box& o) const noexcept {
    return length > o.length &&
        width > o.width &&
        height > o.height;
}


std::istream& boxes::operator >>(std::istream& is, boxes::Box& box) {
    int length, width, height, value;
    double weight;
    
    is >> length >> width >> height >> weight >> value;
    
    box.setLength(length);
    box.setWidth(width);
    box.setHeight(height);
    box.setWeight(weight);
    box.setValue(value);
    
    return is;
}


std::ostream& boxes::operator <<(std::ostream& os, const boxes::Box& box) {
    os << "Box {length: " << box.length <<
             ", width: " << box.width <<
             ", height: " << box.height <<
             ", weight: " << box.weight <<
             ", value: " << box.value << "}";
    
    return os;
}


void boxes::Box::checkSign(int value) {
    if (value < 0)
        throw BoxException(ERR_BOX_NEGATIVE_FIELD);
}
