#ifndef constants_hpp
#define constants_hpp


constexpr double EPS = 1e-9;

// BoxException
constexpr char ERR_BOX_NEGATIVE_FIELD[] = "Neither of box fields can be negative";

// ContainerException
constexpr char ERR_CONT_NEGATIVE_FIELD[] = "Neither of container fields can be negative";
constexpr char ERR_CONT_INCORRECT_INDEX[] = "The container has no elements with this index";
constexpr char ERR_CONT_TOO_HEAVY[] = "The box is too heavy to be stored in this container";


#endif
