#include "box.hpp"


Box::Box(int length, int width, int height, double weight, int value) {
    this->length = length;
    this->width = width;
    this->height = height;
    this->weight = weight;
    this->value = value;
}


Box::Box(const Box& o) {
    length = o.length;
    width = o.width;
    height = o.height;
    weight = o.weight;
    value = o.value;
}


long long Box::getVolume() {
    return length * width * height;
}


void Box::rotate() {
    int t;
    
    if (length < height) {
        t = length;
        length = height;
        height = t;
    }
    
    if (length < width) {
        t = length;
        length = width;
        width = t;
    }
    
    else if (width < height) {
        t = width;
        width = height;
        height = t;
    }
}


bool Box::operator ==(const Box& o) const {
    return length == o.length &&
           width == o.width &&
           height == o.height &&
           double_comparator::equal(weight, o.weight) &&
           value == o.value;
}


bool Box::operator <(const Box& o) const {
    return length < o.length &&
        width < o.width &&
        height < o.height;
}


bool Box::operator >(const Box& o) const {
    return length > o.length &&
        width > o.width &&
        height > o.height;
}


std::istream& operator >>(std::istream& is, Box& box) {
    is >> box.length >> box.width >> box.height >> box.weight >> box.value;
    
    return is;
}


std::ostream& operator <<(std::ostream& os, const Box& box) {
    os << "Box {length: " << box.length <<
             ", width: " << box.width <<
             ", height: " << box.height <<
             ", weight: " << box.weight <<
             ", value: " << box.value << "}";
    
    return os;
}
