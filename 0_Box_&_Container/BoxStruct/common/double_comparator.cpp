#include "double_comparator.hpp"


int double_comparator::compare(double a, double b, double eps) {
    const double diff = a - b;
    
    if (diff < -eps)
        return -1;
    
    if (diff > eps)
        return 1;
    
    return 0;
}


int double_comparator::compare(double a, double b) {
    return compare(a, b, EPS);
}


bool double_comparator::equal(double a, double b, double eps) {
    return abs(a - b) <= eps;
}


bool double_comparator::equal(double a, double b) {
    return equal(a, b, EPS);
}
