#ifndef double_comparator_hpp
#define double_comparator_hpp

#include <cmath>


namespace double_comparator {
    const double EPS = 1e-9;
    
    
    int compare(double, double, double eps);
    int compare(double, double);
    bool equal(double, double, double eps);
    bool equal(double, double);
}


#endif
