#ifndef box_functions_hpp
#define box_functions_hpp

#include "box.hpp"


namespace box_functions {
    int sumValue(Box[], unsigned size);
    bool sizeSumIsNotGreaterThan(Box[], unsigned size, int value);
    double maxWeightNotGreaterThan(Box[], unsigned size, double maxV);
    bool canBePutIntoEachOther(Box[], unsigned size);
    bool canBePutIntoEachOtherByRotation(Box[], unsigned size);
}


#endif
