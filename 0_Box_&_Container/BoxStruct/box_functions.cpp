#include "box_functions.hpp"


int box_functions::sumValue(Box boxes[], unsigned size) {
    int sum = 0;
    
    for (int i = 0; i < size; i++)
        sum += boxes[i].value;
    
    return sum;
}


bool box_functions::sizeSumIsNotGreaterThan(Box boxes[], unsigned size, int value) {
    for (int i = 0; i < size; i++)
        if (boxes[i].length + boxes[i].width + boxes[i].height > value)
            return false;
    
    return true;
}


double box_functions::maxWeightNotGreaterThan(Box boxes[], unsigned size, double maxV) {
    double max = 0;
    bool found = 0;
    int i;
    
    for (i = 0; i < size; i++)
        if (double_comparator::compare(boxes[i].getVolume(), maxV) < 0) {
            max = boxes[i].weight;
            found = 1;
            break;
        }
    
    if (!found)
        throw ERR_BOX_VOLUMES_GT_MAXV;
    
    for (i++; i < size; i++)
        if (double_comparator::compare(boxes[i].getVolume(), maxV) < 0 &&
            double_comparator::compare(boxes[i].weight, max) > 0)
            max = boxes[i].weight;
    
    return max;
}


void merge(Box* boxes[], Box* buff[], unsigned begin, unsigned middle, unsigned end) {
    unsigned i1 = begin;
    unsigned i2 = middle;
    
    while (i1 < middle && i2 < end) {
        if (*boxes[i1] < *boxes[i2] && *boxes[i2] > *boxes[i1]) {
            buff[i1 + i2 - begin - middle] = boxes[i1];
            i1++;
        }
        
        else if (*boxes[i1] > *boxes[i2] && *boxes[i2] < *boxes[i1]) {
            buff[i1 + i2 - begin - middle] = boxes[i2];
            i2++;
        }
        
        else
            throw false;
    }
    
    while (i1 < middle) {
        buff[i1 + i2 - begin - middle] = boxes[i1];
        i1++;
    }
    
    while (i2 < end) {
        buff[i1 + i2 - begin - middle] = boxes[i2];
        i2++;
    }
    
    for (int i = 0; i < end - begin; i++)
        boxes[begin + i] = buff[i];
}


void mergeSort(Box* boxes[], Box* buff[], unsigned begin, unsigned end) {
    if (begin == end - 1)
        return;
    
    int middle = (begin + end) / 2;
    
    mergeSort(boxes, buff, begin, middle);
    mergeSort(boxes, buff, middle, end);
    merge(boxes, buff, begin, middle, end);
}


bool box_functions::canBePutIntoEachOther(Box boxes[], unsigned size) {
    Box* boxesCopy[size];
    for (int i = 0; i < size; i++)
        boxesCopy[i] = new Box(boxes[i]);
    
    Box* buff[size];
    
    try {
        mergeSort(boxesCopy, buff, 0, size);
    } catch (bool x) {
        return false;
    }
    
    for (int i = 0; i < size; i++)
        delete boxesCopy[i];
    
    return true;
}


bool box_functions::canBePutIntoEachOtherByRotation(Box boxes[], unsigned size) {
    Box* boxesCopy[size];
    Box* buff[size];
    
    for (int i = 0; i < size; i++) {
        boxesCopy[i] = new Box(boxes[i]);
        boxesCopy[i]->rotate();
    }
    
    try {
        mergeSort(boxesCopy, buff, 0, size);
    } catch (bool x) {
        return false;
    }
    
    for (int i = 0; i < size; i++)
        delete[] boxesCopy[i];
    
    return true;
}
