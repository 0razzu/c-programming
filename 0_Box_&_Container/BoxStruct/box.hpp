#ifndef box_hpp
#define box_hpp

#include "constants.hpp"
#include "common/double_comparator.hpp"
#include <iostream>


struct Box {
    int length;
    int width;
    int height;
    double weight;
    int value;
    
    
    Box(int length, int width, int height, double weight, int value);
    Box(const Box&);
    
    long long getVolume();
    
    void rotate();
    
    bool operator ==(const Box&) const;
    bool operator <(const Box&) const;
    bool operator >(const Box&) const;
    
    friend std::istream& operator >>(std::istream&, Box&);
    friend std::ostream& operator <<(std::ostream&, const Box&);
};


#endif
