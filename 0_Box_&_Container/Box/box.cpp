#include "box.hpp"


Box::Box(unsigned length, unsigned width, unsigned height, double weight, unsigned value) {
    setLength(length);
    setWidth(width);
    setHeight(height);
    setWeight(weight);
    setValue(value);
}


Box::Box(const Box& o) {
    length = o.length;
    width = o.width;
    height = o.height;
    weight = o.weight;
    value = o.value;
}


void Box::setLength(unsigned length) {
    checkSign(length);
    
    this->length = length;
}


void Box::setWidth(unsigned width) {
    checkSign(width);
    
    this->width = width;
}


void Box::setHeight(unsigned height) {
    checkSign(height);
    
    this->height = height;
}


void Box::setWeight(double weight) {
    if (double_comparator::compare(weight, 0) < 0)
        throw BoxException(ERR_NEGATIVE_FIELD);
    
    this->weight = weight;
}


void Box::setValue(unsigned value) {
    checkSign(value);
    
    this->value = value;
}


unsigned Box::getLength() const noexcept {
    return length;
}


unsigned Box::getWidth() const noexcept {
    return width;
}


unsigned Box::getHeight() const noexcept {
    return height;
}


double Box::getWeight() const noexcept {
    return weight;
}


unsigned Box::getValue() const noexcept {
    return value;
}


long long Box::getVolume() const noexcept {
    return length * width * height;
}


void Box::rotate() {
    int t;
    
    if (length < height) {
        t = length;
        length = height;
        height = t;
    }
    
    if (length < width) {
        t = length;
        length = width;
        width = t;
    }
    
    else if (width < height) {
        t = width;
        width = height;
        height = t;
    }
}


bool Box::operator ==(const Box& o) const noexcept {
    return length == o.length &&
           width == o.width &&
           height == o.height &&
           double_comparator::equal(weight, o.weight) &&
           value == o.value;
}


bool Box::operator <(const Box& o) const noexcept {
    return length < o.length &&
        width < o.width &&
        height < o.height;
}


bool Box::operator >(const Box& o) const noexcept {
    return length > o.length &&
        width > o.width &&
        height > o.height;
}


std::istream& operator >>(std::istream& is, Box& box) {
    int length, width, height, value;
    double weight;
    
    is >> length >> width >> height >> weight >> value;
    
    box.setLength(length);
    box.setWidth(width);
    box.setHeight(height);
    box.setWeight(weight);
    box.setValue(value);
    
    return is;
}


std::ostream& operator <<(std::ostream& os, const Box& box) {
    os << "Box {length: " << box.length <<
             ", width: " << box.width <<
             ", height: " << box.height <<
             ", weight: " << box.weight <<
             ", value: " << box.value << "}";
    
    return os;
}


void Box::checkSign(int value) {
    if (value < 0)
        throw BoxException(ERR_NEGATIVE_FIELD);
}
