#ifndef box_functions_hpp
#define box_functions_hpp

#include "box.hpp"
#include "box_exception.hpp"


namespace box_functions {
    int sumValue(Box[], unsigned size) noexcept;
    bool sizeSumIsNotGreaterThan(Box[], unsigned size, int value) noexcept;
    double maxWeightNotGreaterThan(Box[], unsigned size, double maxV);
    bool canBePutIntoEachOther(Box[], unsigned size);
    bool canBePutIntoEachOtherByRotation(Box[], unsigned size);
}


#endif
