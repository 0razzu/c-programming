#include "box.hpp"
#include "box_functions.hpp"
#include "box_exception.hpp"
#include "common/getch.hpp"
#include <iostream>
#include <iomanip>


template <typename T>
void read(T& value) {
    bool inputFailed = true;
    
    while (inputFailed) {
        std::cin.clear();
        rewind(stdin);
        
        std::cin >> value;
        inputFailed = std::cin.fail();
    }
}


void delBoxes(Box*& boxes, int& boxesSize) {
    operator delete[](boxes);
    boxes = nullptr;
    boxesSize = 0;
}


bool yesNo() {
    int c = 27;
    rewind(stdin);
    
    std::cout << "1 — Yes; 2 — No" << std::endl << std::endl;
    while (c != '1' && c != '2')
        c = getch();
    
    return c == '1';
}


void anyKey() {
    rewind(stdin);
    
    std::cout << "Press any key to continue." << std::endl;
    getch();
}


bool askDelBoxes(Box*& boxes, int& boxesSize) {
    bool yes = true;
    
    if (boxes != nullptr) {
        std::cout << "The existing boxes will be deleted. Continue?" << std::endl;
        yes = yesNo();
        
        if (yes)
            delBoxes(boxes, boxesSize);
    }
    
    return yes;
}


int main() {
    Box* boxes = nullptr;
    int boxesSize = 0;
    int boxLength, boxWidth, boxHeight, boxValue;
    double boxWeight;
    int c = 27, fieldWidth = 1, value1, value2;
    bool yes = false;
    
    srand((unsigned)time(nullptr));
    
    while (c != 'Q' && c != 'q' && c != 153 && c != 185) {
        if (c == 27) {
            system("clear");
            
            if (boxesSize == 0) {
                std::cout << "No boxes" << std::endl;
            }
            
            else {
                std::cout << "Boxes:" << std::endl;
                for (int i = 0; i < boxesSize - 1; i++) {
                    std::cout << std::setw(fieldWidth) << i + 1;
                    std::cout << ") " << boxes[i] << "," << std::endl;
                }
                std::cout << boxesSize << ") " << boxes[boxesSize - 1] << "." << std::endl;
            }
            
            std::cout << std::endl;
            
            std::cout << "1 — Create boxes;" << std::endl;
            std::cout << "2 — Change box data;" << std::endl;
            std::cout << "3 — Delete boxes;" << std::endl;
            std::cout << "4 — Check if boxes are equal;" << std::endl;
            std::cout << "5 — Calculate the sum of the box values;" << std::endl;
            std::cout << "6 — Check if the size sum of each box is not greater than a given value;" << std::endl;
            std::cout << "7 — Find the greatest box weight among the boxes with volume not greater than a given value;" << std::endl;
            std::cout << "8 — Check if the boxes can be put into each other;" << std::endl;
            std::cout << "9 — Check if the boxes can be put into each other by rotation;" << std::endl;
            std::cout << "Q — Quit." << std::endl;

            std::cout << std::endl;
        }
        
        rewind(stdin);
        c = getch();
        
        if (c == '1') {
            if (askDelBoxes(boxes, boxesSize)) {
                std::cout << "How many boxes do you need?" << std::endl;
                while (boxesSize <= 0)
                    read(boxesSize);
                
                if (boxesSize > 5) {
                    std::cout << std::endl << "Entering data for " << boxesSize << " boxes seems to be pretty exhausting. Initialize them with random values instead?" << std::endl;
                    yes = yesNo();
                        
                    if (yes) {
                        boxes = static_cast<Box*>(operator new[](boxesSize * sizeof(Box)));
                        for (int i = 0; i < boxesSize; i++)
                            new(boxes + i) Box(rand() % 100,
                                               rand() % 100,
                                               rand() % 100,
                                               rand() % 10000 / 100.,
                                               rand() % 100);
                    }
                }
                
                if (boxesSize <= 5 || !yes) {
                    boxes = static_cast<Box*>(operator new[](boxesSize * sizeof(Box)));
                    
                    for (int i = 0; i < boxesSize; i++) {
                        std::cout << std::endl << "Enter data for the " << i + 1 << " box." << std::endl;
                        std::cout << "Length: ";
                        read(boxLength);
                        std::cout << "Width: ";
                        read(boxWidth);
                        std::cout << "Height: ";
                        read(boxHeight);
                        std::cout << "Weight: ";
                        read(boxWeight);
                        std::cout << "Value: ";
                        read(boxValue);
                        
                        try {
                            new(boxes + i) Box(boxLength, boxWidth, boxHeight, boxWeight, boxValue);
                        } catch (BoxException& e) {
                            std::cout << std::endl << e.what() << "." << std::endl << std::endl;
                            i--;
                        }
                    }
                }
                
                fieldWidth = floor(log10(boxesSize)) + 1;
            }
            
            c = 27;
        }
        
        else if (c == '2' && boxes != nullptr) {
            value1 = 0;
            std::cout << "Enter the number of the box whose data you need to change." << std::endl;
            while (value1 < 1 || value1 > boxesSize)
                read(value1);
            
            std::cout << std::endl << "The current state of the " << value1 << " box is " << boxes[value1 - 1] << "." << std::endl;
            std::cout << "Enter new data separating each value by a space." << std::endl;
            
            try {
                read(boxes[value1 - 1]);
            } catch (BoxException& e) {
                std::cout << std::endl << e.what() << "." << std::endl << std::endl;
                anyKey();
            }
            
            c = 27;
        }
        
        else if (c == '3' && boxes != nullptr) {
            askDelBoxes(boxes, boxesSize);
            
            c = 27;
        }
        
        else if (c == '4' && boxes != nullptr) {
            value1 = 0;
            std::cout << "Enter the index of the 1 box to compare." << std::endl;
            while (value1 < 1 || value1 > boxesSize)
                read(value1);
            
            value2 = 0;
            std::cout << std::endl << "Enter the index of the 2 box to compare." << std::endl;
            while (value2 < 1 || value2 > boxesSize)
                read(value2);
            
            std::cout << std::endl << "The boxes " << value1 << " and " << value2;
            
            if (boxes[value1 - 1] == boxes[value2 - 1])
                std::cout << " are equal.";
            
            else
                std::cout << " are not equal.";
            
            std::cout << std::endl << std::endl;
            anyKey();
            c = 27;
        }
        
        else if (c == '5' && boxes != nullptr) {
            std::cout << "The sum of the box values is " << box_functions::sumValue(boxes, boxesSize) << std::endl << std::endl;
            
            anyKey();
            c = 27;
        }
        
        else if (c == '6' && boxes != nullptr) {
            std::cout << "Enter a value to compare with the size sums." << std::endl;
            read(value1);
            
            std::cout << std::endl << "There is ";
            
            if (box_functions::sizeSumIsNotGreaterThan(boxes, boxesSize, value1))
                std::cout << "no";
            
            else
                std::cout << "a";
            
            std::cout << " box whose size sum is greater than " << value1 << "." << std::endl << std::endl;
            
            anyKey();
            c = 27;
        }
        
        else if (c == '7' && boxes != nullptr) {
            std::cout << "Enter a value to compare with the volumes." << std::endl;
            read(value1);
            
            std::cout << std::endl;
            
            try {
                boxWeight = box_functions::maxWeightNotGreaterThan(boxes, boxesSize, value1);
                
                std::cout << "The greatest box weight among the boxes with volume not greater than " << value1 << " is " << boxWeight;
            } catch (BoxException& e) {
                std::cout << e.what();
            }
            
            std::cout << "." << std::endl << std::endl;
            
            anyKey();
            c = 27;
        }
        
        else if (c == '8' && boxes != nullptr) {
            std::cout << "These boxes can";
            
            if (!box_functions::canBePutIntoEachOther(boxes, boxesSize))
                std::cout << "not";
            
            std::cout << " be put into each other." << std::endl << std::endl;
            
            anyKey();
            c = 27;
        }
        
        
        else if (c == '9' && boxes != nullptr) {
            std::cout << "These boxes can";
            
            if (!box_functions::canBePutIntoEachOtherByRotation(boxes, boxesSize))
                std::cout << "not";
            
            std::cout << " be put into each other by rotation." << std::endl << std::endl;
            
            anyKey();
            c = 27;
        }
        
        else if ((c == 'Q' || c == 'q' || c == 153 || c == 185) && !askDelBoxes(boxes, boxesSize))
            c = 27;
    }
    
    system("clear");
    std::cout << "Program ended." << std::endl << std::endl;
    
    return 0;
}
