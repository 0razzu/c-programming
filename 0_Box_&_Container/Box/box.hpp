#ifndef box_hpp
#define box_hpp

#include "constants.hpp"
#include "common/double_comparator.hpp"
#include "box_exception.hpp"
#include <iostream>


class Box {
public:
    Box(unsigned length, unsigned width, unsigned height, double weight, unsigned value);
    Box(const Box&);
    
    void setLength(unsigned length);
    void setWidth(unsigned width);
    void setHeight(unsigned height);
    void setWeight(double weight);
    void setValue(unsigned value);
    
    unsigned getLength() const noexcept;
    unsigned getWidth() const noexcept;
    unsigned getHeight() const noexcept;
    double getWeight() const noexcept;
    unsigned getValue() const noexcept;
    long long getVolume() const noexcept;
    
    void rotate();
    
    bool operator ==(const Box&) const noexcept;
    bool operator <(const Box&) const noexcept;
    bool operator >(const Box&) const noexcept;
    
    friend std::istream& operator >>(std::istream&, Box&);
    friend std::ostream& operator <<(std::ostream&, const Box&);
    
private:
    int length;
    int width;
    int height;
    double weight;
    int value;
    
    void checkSign(int);
};


#endif
