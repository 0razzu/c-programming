#ifndef constants_hpp
#define constants_hpp


const double EPS = 1e-9;

// Box
const char ERR_NEGATIVE_FIELD[] = "Neither of box fields can be negative";

// BoxFunctions
const char ERR_BOX_VOLUMES_GT_MAXV[] = "The array does not contain boxes with volume not greater than maxV";


#endif
