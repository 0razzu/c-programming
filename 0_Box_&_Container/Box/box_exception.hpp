#ifndef box_exception_hpp
#define box_exception_hpp

#include <exception>


class BoxException: public std::exception {
public:
    BoxException(const char*);
    virtual const char* what() const noexcept override;
    
private:
    const char* message;
};


#endif
