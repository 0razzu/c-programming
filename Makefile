all: BoxStruct Box Container IntDynamicArray RingBuffer RingList RingListIterator HashMap HashMapIterator LinkedHashMap LinkedHashMapIterator IntBinTree FreqDictionary AvlFreqDictionary

clear:
	rm *.out

BoxStruct:
	cd "0_Box_&_Container"/BoxStruct/ && \
	g++ main.cpp box.cpp box_functions.cpp common/*.cpp -o ../../00_BoxStruct_main.out && \
	cd ../../

Box:
	cd "0_Box_&_Container"/Box/ && \
	g++ --std=c++11 main.cpp box.cpp box_functions.cpp box_exception.cpp common/*.cpp -o ../../01_Box_main.out && \
	cd ../../

Container:
	cd "0_Box_&_Container"/Container/ && \
	cxxtestgen test_container.hpp -o test_container.cpp --error-printer && \
	g++ --std=c++11 test_container.cpp container.cpp box.cpp common/*.cpp exception/*.cpp -o ../../02_Container_test.out && \
	rm test_container.cpp && \
	cd ../../

IntDynamicArray:
	cd 1_IntDynamicArray/ && \
	cxxtestgen test_int_dynamic_array.hpp -o test_int_dynamic_array.cpp --error-printer && \
	g++ --std=c++11 test_int_dynamic_array.cpp int_dynamic_array.cpp int_dynamic_array_exception.cpp -o ../1_IntDynamicArray_test.out && \
	rm test_int_dynamic_array.cpp && \
	cd ../

RingBuffer:
	cd 2_RingBuffer/ && \
	cxxtestgen tests/test_ring_buffer.hpp -o test_ring_buffer.cpp --error-printer && \
	g++ --std=c++11 test_ring_buffer.cpp exceptions/ring_buffer_exception.cpp -o ../2_RingBuffer_test.out && \
	rm test_ring_buffer.cpp && \
	cd ../

RingBufferIterator:
	cd 2_RingBuffer/ && \
	cxxtestgen tests/test_ring_buffer_iterator.hpp -o test_ring_buffer_iterator.cpp --error-printer && \
	g++ --std=c++11 test_ring_buffer_iterator.cpp exceptions/*.cpp -o ../2_RingBufferIterator_test.out && \
	rm test_ring_buffer_iterator.cpp && \
	cd ../

RingList:
	cd 3_List/ && \
	cxxtestgen tests/test_ring_list.hpp -o test_ring_list.cpp --error-printer && \
	g++ --std=c++11 test_ring_list.cpp exceptions/*.cpp -o ../3_RingList_test.out && \
	rm test_ring_list.cpp && \
	cd ../

RingListIterator:
	cd 3_List/ && \
	cxxtestgen tests/test_ring_list_iterator.hpp -o test_ring_list_iterator.cpp --error-printer && \
	g++ --std=c++11 test_ring_list_iterator.cpp exceptions/*.cpp -o ../3_RingListIterator_test.out && \
	rm test_ring_list_iterator.cpp && \
	cd ../

HashMap:
	cd 4_HashMap/ && \
	cxxtestgen tests/test_hash_map.hpp -o test_hash_map.cpp --error-printer && \
	g++ --std=c++11 test_hash_map.cpp exceptions/hash_map_exception.cpp -o ../4_HashMap_test.out && \
	rm test_hash_map.cpp && \
	cd ../

HashMapIterator:
	cd 4_HashMap/ && \
	cxxtestgen tests/test_hash_map_iterator.hpp -o test_hash_map_iterator.cpp --error-printer && \
	g++ --std=c++11 test_hash_map_iterator.cpp exceptions/*.cpp -o ../4_HashMapIterator_test.out && \
	rm test_hash_map_iterator.cpp && \
	cd ../

LinkedHashMap:
	cd 4_HashMap/ && \
	cxxtestgen tests/test_linked_hash_map.hpp -o test_linked_hash_map.cpp --error-printer && \
	g++ --std=c++11 test_linked_hash_map.cpp exceptions/hash_map_exception.cpp -o ../4_LinkedHashMap_test.out && \
	rm test_linked_hash_map.cpp && \
	cd ../

LinkedHashMapIterator:
	cd 4_HashMap/ && \
	cxxtestgen tests/test_linked_hash_map_iterator.hpp -o test_linked_hash_map_iterator.cpp --error-printer && \
	g++ --std=c++11 test_linked_hash_map_iterator.cpp exceptions/*.cpp -o ../4_LinkedHashMapIterator_test.out && \
	rm test_linked_hash_map_iterator.cpp && \
	cd ../

IntBinTree:
	cd 5_IntBinTree/ && \
	cxxtestgen tests/test_int_bin_tree.hpp -o test_int_bin_tree.cpp --error-printer && \
	g++ --std=c++11 test_int_bin_tree.cpp int_bin_tree.cpp exceptions/int_bin_tree_exception.cpp -o ../5_IntBinTree_test.out && \
	rm test_int_bin_tree.cpp && \
	cd ../

FreqDictionary:
	cd 6_FreqDictionary/ && \
	cxxtestgen tests/test_freq_dictionary.hpp -o test_freq_dictionary.cpp --error-printer && \
	g++ --std=c++11 test_freq_dictionary.cpp freq_dictionary.cpp -o ../6_FreqDictionary_test.out && \
	rm test_freq_dictionary.cpp && \
	cd ../

AvlFreqDictionary:
	cd 6_FreqDictionary/ && \
	cxxtestgen tests/test_avl_freq_dictionary.hpp -o test_avl_freq_dictionary.cpp --error-printer && \
	g++ --std=c++11 test_avl_freq_dictionary.cpp avl_freq_dictionary.cpp -o ../6_AvlFreqDictionary_test.out && \
	rm test_avl_freq_dictionary.cpp && \
	cd ../
