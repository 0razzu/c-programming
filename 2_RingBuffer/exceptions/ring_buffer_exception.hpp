#ifndef ring_buffer_exception_hpp
#define ring_buffer_exception_hpp

#include <exception>


constexpr char RING_BUFFER_OVERFLOW[] = "Cannot push more elements: the buffer is already full";
constexpr char RING_BUFFER_EMPTY[] = "Cannot get the element: the buffer is empty";


class RingBufferException: public std::exception {
public:
    RingBufferException(const char*);
    virtual const char* what() const noexcept override;
    
private:
    const char* message;
};


#endif
