#ifndef ring_buffer_iterator_exception_hpp
#define ring_buffer_iterator_exception_hpp

#include <exception>


constexpr char RING_BUFFER_IT_NO_ELEMENT[] = "The buffer has no element at this position";


class RingBufferIteratorException: public std::exception {
public:
    RingBufferIteratorException(const char*);
    virtual const char* what() const noexcept override;
    
private:
    const char* message;
};


#endif
