#include "ring_buffer_iterator_exception.hpp"


RingBufferIteratorException::RingBufferIteratorException(const char* message) {
    this->message = message;
}


const char* RingBufferIteratorException::what() const noexcept {
    return message;
}
