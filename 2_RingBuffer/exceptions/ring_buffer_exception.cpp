#include "ring_buffer_exception.hpp"


RingBufferException::RingBufferException(const char* message) {
    this->message = message;
}


const char* RingBufferException::what() const noexcept {
    return message;
}
