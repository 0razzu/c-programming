#ifndef test_ring_buffer_h
#define test_ring_buffer_h

#include "../ring_buffer.hpp"
#include "../ring_buffer_iterator.hpp"
#include "../exceptions/ring_buffer_iterator_exception.hpp"
#include <cxxtest/TestSuite.h>
#include <string>
#include <sstream>


const std::string ERR_NO_EXCEPTION = "Expected an exception but there was not any";


class TestRingBufferIterator: public CxxTest::TestSuite {
public:
    void testConstr() {
        RingBuffer<int> a(5);
        
        a.push(-5);
        
        RingBufferIterator<int> it(a);
        
        TS_ASSERT_EQUALS(-5, it.getValue());
    }
    
    
    void testNext() {
        RingBuffer<int> a(5);
        
        a.push(10);
        a.push(3);
        a.push(-32);
        a.pop();
        a.push(1);
        a.pop();
        a.push(6);
        a.push(18);
        
        RingBufferIterator<int> it(a);
        
        TS_ASSERT_EQUALS(-32, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(1, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(6, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(18, it.getValue());
    }
    
    
    void testNextException() {
        RingBuffer<int> a(3);
        
        a.push(1);
        a.push(2);
        
        RingBufferIterator<int> it1(a);
        
        it1.next();
        
        try {
            it1.next();
            TS_FAIL(ERR_NO_EXCEPTION + ": it1");
        } catch (RingBufferIteratorException& e) {
            TS_ASSERT_SAME_DATA(RING_BUFFER_IT_NO_ELEMENT, e.what(), strlen(RING_BUFFER_IT_NO_ELEMENT));
        }
        
        RingBuffer<int> b(0);
        RingBufferIterator<int> it2(b);
        
        try {
            it2.next();
            TS_FAIL(ERR_NO_EXCEPTION + ": it2");
        } catch (RingBufferIteratorException& e) {
            TS_ASSERT_SAME_DATA(RING_BUFFER_IT_NO_ELEMENT, e.what(), strlen(RING_BUFFER_IT_NO_ELEMENT));
        }
        
        RingBuffer<int> c(4);
        
        c.push(1);
        c.push(2);
        c.push(3);
        c.push(4);
        
        RingBufferIterator<int> it3(c);
        
        c.pop();
        c.pop();
        
        try {
            it3.next();
            TS_FAIL(ERR_NO_EXCEPTION + ": it3");
        } catch (RingBufferIteratorException& e) {
            TS_ASSERT_SAME_DATA(RING_BUFFER_IT_NO_ELEMENT, e.what(), strlen(RING_BUFFER_IT_NO_ELEMENT));
        }
    }
    
    
    void testStart1() {
        RingBuffer<unsigned> a(4);
        
        a.push(1);
        a.push(2);
        a.push(3);
        a.push(4);
        
        RingBufferIterator<unsigned> it(a);
        
        TS_ASSERT_EQUALS(1, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(2, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(3, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(4, it.getValue());
        
        it.start();
        
        TS_ASSERT_EQUALS(1, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(2, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(3, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(4, it.getValue());
    }
    
    
    void testStart2() {
        RingBuffer<unsigned> a(4);
        
        a.push(1);
        a.push(2);
        a.push(3);
        a.push(4);
        
        RingBufferIterator<unsigned> it(a);
        
        TS_ASSERT_EQUALS(1, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(2, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(3, it.getValue());
        
        a.pop();
        a.pop();
        
        it.start();
        
        TS_ASSERT_EQUALS(3, it.getValue());
        it.next();
        TS_ASSERT_EQUALS(4, it.getValue());
    }
    
    
    void testFinished1() {
        RingBuffer<unsigned> a(4);
        
        a.push(1);
        a.push(2);
        a.push(3);
        
        RingBufferIterator<unsigned> it(a);
        
        TS_ASSERT(!it.finished());
        
        it.next();
        it.next();
        
        TS_ASSERT(it.finished());
    }
    
    
    void testFinished2() {
        RingBuffer<double> a(4);
        
        a.push(1.9);
        a.push(83.2);
        a.push(-7);
        
        RingBufferIterator<double> it(a);
        
        TS_ASSERT(!it.finished());
        
        a.pop();
        a.pop();
        a.pop();
        
        TS_ASSERT(it.finished());
    }
    
    
    void testFinished3() {
        RingBuffer<int> a(4);
        
        a.push(1);
        a.push(2);
        a.push(3);
        a.push(4);
        
        RingBufferIterator<int> it(a);
        
        TS_ASSERT(!it.finished());
        
        a.pop();
        a.pop();
        
        TS_ASSERT(!it.finished());
    }
    
    
    void testGetValueException() {
        RingBuffer<int> a(0);
        RingBuffer<int> b(3);
        
        b.push(1);
        b.push(2);
        
        RingBufferIterator<int> it1(a);
        RingBufferIterator<int> it2(b);
        
        b.pop();
        b.pop();
        
        try {
            it1.getValue();
            TS_FAIL(ERR_NO_EXCEPTION + ": it1");
        } catch (RingBufferIteratorException& e) {
            TS_ASSERT_SAME_DATA(RING_BUFFER_IT_NO_ELEMENT, e.what(), strlen(RING_BUFFER_IT_NO_ELEMENT));
        }
        
        try {
            it2.getValue();
            TS_FAIL(ERR_NO_EXCEPTION + ": it2");
        } catch (RingBufferIteratorException& e) {
            TS_ASSERT_SAME_DATA(RING_BUFFER_IT_NO_ELEMENT, e.what(), strlen(RING_BUFFER_IT_NO_ELEMENT));
        }
    }
};


#endif
