#ifndef test_ring_buffer_h
#define test_ring_buffer_h

#include "../ring_buffer.hpp"
#include "../exceptions/ring_buffer_exception.hpp"
#include <cxxtest/TestSuite.h>
#include <string>
#include <sstream>


const std::string ERR_NO_EXCEPTION = "Expected an exception but there was not any";


class TestRingBuffer: public CxxTest::TestSuite {
public:
    void testConstr() {
        RingBuffer<int> a(10);
        RingBuffer<double> b;
        
        TS_ASSERT_EQUALS(10, a.capacity());
        TS_ASSERT_EQUALS(0, a.size());
        TS_ASSERT_EQUALS(DEFAULT_CAPACITY, b.capacity());
        TS_ASSERT_EQUALS(0, b.size());
    }
    
    
    void testCopyConstr() {
        RingBuffer<int> a(5);
        
        a.push(1);
        a.push(2);
        a.push(3);
        a.pop();
        
        RingBuffer<int> b(a);
        
        TS_ASSERT_EQUALS(5, a.capacity());
        TS_ASSERT_EQUALS(2, a.size());
        TS_ASSERT_EQUALS(2, a.pop());
        TS_ASSERT_EQUALS(3, a.pop());
        
        TS_ASSERT(a.empty() && !b.empty());
        
        TS_ASSERT_EQUALS(5, b.capacity());
        TS_ASSERT_EQUALS(2, b.size());
        TS_ASSERT_EQUALS(2, b.pop());
        TS_ASSERT_EQUALS(3, b.pop());
    }
    
    
    void testMoveConstr() {
        RingBuffer<short> a(5);
        
        a.push(-1);
        a.push(-2);
        a.push(-3);
        
        RingBuffer<short> b(std::move(a));
        
        TS_ASSERT_EQUALS(0, a.capacity());
        TS_ASSERT_EQUALS(0, a.size());
        
        TS_ASSERT_EQUALS(5, b.capacity());
        TS_ASSERT_EQUALS(3, b.size());
        TS_ASSERT_EQUALS(-1, b.pop());
        TS_ASSERT_EQUALS(-2, b.pop());
        TS_ASSERT_EQUALS(-3, b.pop());
    }
    
    
    void testEmptyFull() {
        RingBuffer<float> a(3);
        
        TS_ASSERT(a.empty());
        TS_ASSERT(!a.full());
        
        a.push(-1.1);
        
        TS_ASSERT(!a.empty());
        TS_ASSERT(!a.full());
        
        a.push(0.1);
        a.push(-3243.2);
        
        TS_ASSERT(!a.empty());
        TS_ASSERT(a.full());
    }
    
    
    void testPushPeekPop() {
        RingBuffer<int> a(3);
        
        a.push(-1);
        
        TS_ASSERT_EQUALS(-1, a.peek());
        TS_ASSERT(!a.empty());
        TS_ASSERT_EQUALS(-1, a.pop());
        TS_ASSERT(a.empty());
        
        a.push(1);
        a.push(2);
        
        TS_ASSERT_EQUALS(1, a.peek());
        TS_ASSERT_EQUALS(2, a.size());
        TS_ASSERT_EQUALS(1, a.pop());
        TS_ASSERT_EQUALS(1, a.size());
        
        a.push(3);
        a.push(4);
        
        TS_ASSERT(a.full());
        
        TS_ASSERT_EQUALS(2, a.pop());
        TS_ASSERT_EQUALS(3, a.pop());
        TS_ASSERT_EQUALS(4, a.peek());
        TS_ASSERT_EQUALS(4, a.pop());
        TS_ASSERT(a.empty());
    }
    
    
    void testPushPeekPopExceptions() {
        RingBuffer<unsigned> a(3);
        
        a.push(1);
        a.push(2);
        a.push(3);
        
        try {
            a.push(4);
            TS_FAIL(ERR_NO_EXCEPTION + ": a.push(4)");
        } catch (RingBufferException& e) {
            TS_ASSERT_SAME_DATA(RING_BUFFER_OVERFLOW, e.what(), strlen(RING_BUFFER_OVERFLOW));
        }
        
        a.pop();
        a.push(4);
        a.pop();
        a.pop();
        a.pop();
        
        try {
            a.peek();
            TS_FAIL(ERR_NO_EXCEPTION + ": a.peek()");
        } catch (RingBufferException& e) {
            TS_ASSERT_SAME_DATA(RING_BUFFER_EMPTY, e.what(), strlen(RING_BUFFER_EMPTY));
        }
        
        try {
            a.pop();
            TS_FAIL(ERR_NO_EXCEPTION + ": a.pop()");
        } catch (RingBufferException& e) {
            TS_ASSERT_SAME_DATA(RING_BUFFER_EMPTY, e.what(), strlen(RING_BUFFER_EMPTY));
        }
    }
    
    
    void testClear() {
        RingBuffer<int*> a(4);
        
        int* x = new int[10];
        
        a.push(nullptr);
        a.push(x);
        a.push(x);
        
        a.clear();
        
        TS_ASSERT(a.empty());
        
        delete[] x;
    }
    
    
    void testOperatorAssign() {
        RingBuffer<int> a(5);
        
        a.push(1);
        a.push(2);
        a.push(3);
        a.pop();
        
        RingBuffer<int> b;
        
        b.push(10);
        b.push(7);
        
        b = a;
        
        TS_ASSERT_EQUALS(5, a.capacity());
        TS_ASSERT_EQUALS(2, a.size());
        TS_ASSERT_EQUALS(2, a.pop());
        TS_ASSERT_EQUALS(3, a.pop());
        
        TS_ASSERT(a.empty() && !b.empty());
        
        TS_ASSERT_EQUALS(5, b.capacity());
        TS_ASSERT_EQUALS(2, b.size());
        TS_ASSERT_EQUALS(2, b.pop());
        TS_ASSERT_EQUALS(3, b.pop());
    }
    
    
    void testOperatorMove() {
        RingBuffer<int> a(4);
        
        a.push(-3);
        a.push(-2);
        a.push(-1);
        
        RingBuffer<int> b;
        
        b.push(1);
        b.push(71);
        
        b = std::move(a);
        
        TS_ASSERT_EQUALS(4, b.capacity());
        TS_ASSERT_EQUALS(3, b.size());
        TS_ASSERT_EQUALS(-3, b.pop());
        TS_ASSERT_EQUALS(-2, b.pop());
        TS_ASSERT_EQUALS(-1, b.pop());
    }
    
    
    void testOperatorIn1() {
        std::stringstream s;
        RingBuffer<int> a(2);
        
        s << "10 0";
        s >> a;
        
        TS_ASSERT_EQUALS(10, a.capacity());
        TS_ASSERT_EQUALS(0, a.size());
    }
    
    
    void testOperatorIn2() {
        std::stringstream s;
        RingBuffer<int> a;
        
        s << "7 3 10 15 30";
        s >> a;
        
        TS_ASSERT_EQUALS(7, a.capacity());
        TS_ASSERT_EQUALS(3, a.size());
        TS_ASSERT_EQUALS(10, a.pop());
        TS_ASSERT_EQUALS(15, a.pop());
        TS_ASSERT_EQUALS(30, a.pop());
    }
    
    
    void testOperatorOut1() {
        std::ostringstream os;
        RingBuffer<unsigned short> a(0);
        const std::string str = "RingBuffer {capacity: 0, size: 0, elements: []}";

        os << a;

        TS_ASSERT_EQUALS(str, os.str())
    }
    
    
    void testOperatorOut2() {
        std::ostringstream os;
        RingBuffer<int> a(5);
        
        a.push(-1);
        a.push(1);
        a.pop();
        a.push(2);
        a.push(3);
        
        const std::string str = "RingBuffer {capacity: 5, size: 3, elements: [1, 2, 3]}";

        os << a;

        TS_ASSERT_EQUALS(str, os.str())
    }
};


#endif
