#ifndef ring_buffer_iterator_hpp
#define ring_buffer_iterator_hpp

#include "exceptions/ring_buffer_iterator_exception.hpp"
#include "ring_buffer.hpp"


template <typename T> class RingBuffer;


template <typename T>
class RingBufferIterator {
public:
    RingBufferIterator(RingBuffer<T>&);
    
    void start();
    void next();
    
    bool finished() const;
    
    T getValue() const;

private:
    RingBuffer<T> *buffer_;
    unsigned long long index_;
    
    bool outside() const;
};


template <typename T>
RingBufferIterator<T>::RingBufferIterator(RingBuffer<T>& buffer) {
    buffer_ = &buffer;
    start();
}


template <typename T>
void RingBufferIterator<T>::start() {
    index_ = buffer_->start_;
}


template <typename T>
void RingBufferIterator<T>::next() {
    if (finished() || outside())
        throw RingBufferIteratorException(RING_BUFFER_IT_NO_ELEMENT);
    
    index_ = (index_ + 1) % buffer_->capacity_;
}


template <typename T>
bool RingBufferIterator<T>::finished() const {
    return buffer_->empty() ||
        index_ == (buffer_->start_ + buffer_->size_ - 1) % buffer_->capacity_;
}


template <typename T>
T RingBufferIterator<T>::getValue() const {
    if (buffer_->empty() || outside())
        throw RingBufferIteratorException(RING_BUFFER_IT_NO_ELEMENT);
    
    return buffer_->data_[index_];
}


template <typename T>
bool RingBufferIterator<T>::outside() const {
    return index_ < buffer_->start_ &&
        index_ >= (buffer_->start_ + buffer_->size_) % buffer_->capacity_;
}


#endif
