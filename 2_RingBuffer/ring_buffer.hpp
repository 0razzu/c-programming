#ifndef ring_buffer_hpp
#define ring_buffer_hpp

#include "exceptions/ring_buffer_exception.hpp"
#include "ring_buffer_iterator.hpp"
#include <iostream>


constexpr unsigned long long DEFAULT_CAPACITY = 64;

template <typename T> class RingBufferIterator;


template <typename T>
class RingBuffer {
public:
    RingBuffer(unsigned long long capacity);
    RingBuffer();
    RingBuffer(const RingBuffer&);
    RingBuffer(RingBuffer&&);
    
    ~RingBuffer();
    
    unsigned long long size() const noexcept;
    unsigned long long capacity() const noexcept;
    bool empty() const noexcept;
    bool full() const noexcept;
    
    void push(T);
    T peek() const;
    T pop();
    
    void clear();
    
    RingBuffer& operator =(const RingBuffer&);
    RingBuffer& operator =(RingBuffer&&);
    
    template <typename U>
    friend std::istream& operator >>(std::istream&, RingBuffer<U>&);
    template <typename U>
    friend std::ostream& operator <<(std::ostream&, const RingBuffer<U>&);
    
    friend class RingBufferIterator<T>;

private:
    T* data_;
    unsigned long long capacity_;
    unsigned long long start_;
    unsigned long long size_;
};


template <typename T>
RingBuffer<T>::RingBuffer(unsigned long long capacity) {
    data_ = new T[capacity];
    capacity_ = capacity;
    start_ = 0;
    size_ = 0;
}


template <typename T>
RingBuffer<T>::RingBuffer(): RingBuffer::RingBuffer(DEFAULT_CAPACITY) {
}


template <typename T>
RingBuffer<T>::RingBuffer(const RingBuffer& o) {
    data_ = new T[o.capacity_];
    capacity_ = o.capacity_;
    
    for (unsigned long long i = o.start_; i < (o.start_ + o.size_) % capacity_; i = (i + 1) % capacity_)
        data_[i] = o.data_[i];
    
    start_ = o.start_;
    size_ = o.size_;
}


template <typename T>
RingBuffer<T>::RingBuffer(RingBuffer&& o) {
    data_ = o.data_;
    capacity_ = o.capacity_;
    start_ = o.start_;
    size_ = o.size_;
    
    o.data_ = nullptr;
    o.capacity_ = 0;
    o.start_ = 0;
    o.size_ = 0;
}


template <typename T>
RingBuffer<T>::~RingBuffer() {
    clear();
    
    if (data_ != nullptr) {
        delete[] data_;
        data_ = nullptr;
    }
    
    capacity_ = 0;
}


template <typename T>
unsigned long long RingBuffer<T>::size() const noexcept {
    return size_;
}


template <typename T>
unsigned long long RingBuffer<T>::capacity() const noexcept {
    return capacity_;
}


template <typename T>
bool RingBuffer<T>::empty() const noexcept {
    return size_ == 0;
}


template <typename T>
bool RingBuffer<T>::full() const noexcept {
    return size_ == capacity_;
}


template <typename T>
void RingBuffer<T>::push(T e) {
    if (full())
        throw RingBufferException(RING_BUFFER_OVERFLOW);
    
    data_[(start_ + size_) % capacity_] = e;
    
    size_++;
}


template <typename T>
T RingBuffer<T>::peek() const {
    if (empty())
        throw RingBufferException(RING_BUFFER_EMPTY);
    
    return data_[start_];
}


template <typename T>
T RingBuffer<T>::pop() {
    if (empty())
        throw RingBufferException(RING_BUFFER_EMPTY);
    
    T e = data_[start_];
    
    start_ = (start_ + 1) % capacity_;
    size_--;
    
    return e;
}


template <typename T>
void RingBuffer<T>::clear() {
    start_ = 0;
    size_ = 0;
}


template <typename T>
RingBuffer<T>& RingBuffer<T>::operator =(const RingBuffer& o) {
    if (capacity_ != o.capacity_) {
        delete[] data_;
    
        data_ = new T[o.capacity_];
    }
    
    capacity_ = o.capacity_;
    
    for (unsigned long long i = o.start_; i < (o.start_ + o.size_) % capacity_; i = (i + 1) % capacity_)
        data_[i] = o.data_[i];
    
    start_ = o.start_;
    size_ = o.size_;
    
    return *this;
}


template <typename T>
RingBuffer<T>& RingBuffer<T>::operator =(RingBuffer&& o) {
    std::swap(data_, o.data_);
    start_ = o.start_;
    size_ = o.size_;
    capacity_ = o.capacity_;
    
    return *this;
}


template <typename T>
std::istream& operator >>(std::istream& is, RingBuffer<T>& buffer) {
    unsigned long long capacity, size;
    
    is >> capacity >> size;
    
    buffer.clear();
    
    buffer.data_ = new T[capacity];
    buffer.capacity_ = capacity;
    buffer.start_ = 0;
    buffer.size_ = size;
    
    for (unsigned long long i = 0; i < size; i++)
        is >> buffer.data_[i];
    
    return is;
}


template <typename T>
std::ostream& operator <<(std::ostream& os, const RingBuffer<T>& buffer) {
    os << "RingBuffer {capacity: " << buffer.capacity_ << ", size: " << buffer.size_ << ", elements: [";
    
    if (buffer.size_ > 0) {
        for (unsigned long long i = 0; i < buffer.size_ - 1; i++)
            os << buffer.data_[(buffer.start_ + i) % buffer.capacity_] << ", ";
    
        os << buffer.data_[(buffer.start_ + buffer.size_ - 1) % buffer.capacity_];
    };
    
    os << "]}";
    
    return os;
}


#endif
