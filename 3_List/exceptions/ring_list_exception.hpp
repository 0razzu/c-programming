#ifndef ring_list_exception_hpp
#define ring_list_exception_hpp

#include <exception>


constexpr char LIST_WRONG_ITERATOR[] = "The provided iterator does not iterate this list or the current element has been deleted";
constexpr char LIST_NO_ELEMENT[] = "Cannot find the requested element in the list";
constexpr char LIST_DEL_BUFFER[] = "Cannot delete the buffer element";


class RingListException: public std::exception {
public:
    RingListException(const char*);
    virtual const char* what() const noexcept override;
    
private:
    const char* message;
};


#endif
