#include "ring_list_exception.hpp"


RingListException::RingListException(const char* message) {
    this->message = message;
}


const char* RingListException::what() const noexcept {
    return message;
}
