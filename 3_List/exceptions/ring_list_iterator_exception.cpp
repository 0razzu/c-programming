#include "ring_list_iterator_exception.hpp"


RingListIteratorException::RingListIteratorException(const char* message) {
    this->message = message;
}


const char* RingListIteratorException::what() const noexcept {
    return message;
}
