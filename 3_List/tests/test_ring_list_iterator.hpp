#ifndef test_ring_list_iterator_hpp
#define test_ring_list_iterator_hpp

#include "../list.hpp"
#include "../list_iterator.hpp"
#include "../ring_list.hpp"
#include "../exceptions/ring_list_exception.hpp"
#include "../exceptions/ring_list_iterator_exception.hpp"
#include <cxxtest/TestSuite.h>
#include <string>


const std::string ERR_NO_EXCEPTION = "Expected an exception but there was not any";


class TestRingListIterator: public CxxTest::TestSuite {
public:
    void testHierarchy() {
        RingList<char> list;
        ListIterator<char>* it = new RingList<char>::Iterator(list);
        
        delete it;
    }
    
    
    void testStart() {
        RingList<int> a;
        RingList<int>::Iterator it(a);
        
        a.add(1, &it);
        a.add(7, &it);
        a.add(-5, &it);
        
        it.next();
        it.next();
        it.start();
        
        TS_ASSERT_EQUALS(-5, it.get());
        
        it.next();
        it.prev();
        it.start();
        
        TS_ASSERT_EQUALS(-5, it.get());
    }
    
    
    void testGet1() {
        RingList<short> a;
        RingList<short>::Iterator it1(a);
        
        a.add(1, &it1);
        
        RingList<short>::Iterator it2(a);
        
        TS_ASSERT_EQUALS(1, it2.get());
    }
    
    
    void testGet2() {
        RingList<int> a;
        RingList<int>::Iterator it(a);
        
        try {
            it.get();
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch (RingListIteratorException& e) {
            TS_ASSERT_SAME_DATA(IT_NO_ELEMENT, e.what(), strlen(IT_NO_ELEMENT));
        }
        
        a.add(9, &it);
        
        TS_ASSERT_EQUALS(9, it.get());
    }
    
    
    void testPrevNext() {
        RingList<int> a;
        RingList<int>::Iterator it1(a);
        
        a.add(5, &it1);
        a.add(4, &it1);
        a.add(3, &it1);
        a.add(2, &it1);
        a.add(1, &it1);
        
        RingList<int>::Iterator it2(a);
        
        for (short i = 1; i <= 5; i++) {
            TS_ASSERT_EQUALS(i, it2.get());
            it2.next();
        }
        
        for (short i = 5; i >= 1; i--) {
            it2.prev();
            TS_ASSERT_EQUALS(i, it2.get());
        }
    }
    
    
    void testPrevNextExceptions() {
        RingList<int> a;
        RingList<int>::Iterator it(a);
        
        try {
            it.prev();
            TS_FAIL(ERR_NO_EXCEPTION + ": prev(), empty");
        } catch (RingListIteratorException& e) {
            TS_ASSERT_SAME_DATA(IT_NO_ELEMENT, e.what(), strlen(IT_NO_ELEMENT));
        }
        
        try {
            it.next();
            TS_FAIL(ERR_NO_EXCEPTION + ": next(), empty");
        } catch (RingListIteratorException& e) {
            TS_ASSERT_SAME_DATA(IT_NO_ELEMENT, e.what(), strlen(IT_NO_ELEMENT));
        }
        
        a.add(8, &it);
        a.add(9, &it);
        
        try {
            it.prev();
            TS_FAIL(ERR_NO_EXCEPTION + ": prev()");
        } catch (RingListIteratorException& e) {
            TS_ASSERT_SAME_DATA(IT_NO_ELEMENT, e.what(), strlen(IT_NO_ELEMENT));
        }
        
        it.next();
        it.next();
        
        try {
            it.next();
            TS_FAIL(ERR_NO_EXCEPTION + ": next()");
        } catch (RingListIteratorException& e) {
            TS_ASSERT_SAME_DATA(IT_NO_ELEMENT, e.what(), strlen(IT_NO_ELEMENT));
        }
    }
    
    
    void testHasPrevHasNext() {
        RingList<double> a;
        RingList<double>::Iterator it(a);
        
        TS_ASSERT(!it.hasPrev());
        TS_ASSERT(!it.hasNext());
        
        a.add(1.32, &it);
        
        TS_ASSERT(!it.hasPrev());
        TS_ASSERT(it.hasNext());
        
        it.next();
        a.add(-0.76, &it);
        
        TS_ASSERT(it.hasPrev());
        TS_ASSERT(it.hasNext());
        
        it.next();
        
        TS_ASSERT(it.hasPrev());
        TS_ASSERT(!it.hasNext());
    }
};


#endif
