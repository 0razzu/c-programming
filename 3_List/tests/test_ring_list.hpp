#ifndef test_ring_list_hpp
#define test_ring_list_hpp

#include "../list.hpp"
#include "../list_iterator.hpp"
#include "../ring_list.hpp"
#include "../exceptions/ring_list_exception.hpp"
#include "../exceptions/ring_list_iterator_exception.hpp"
#include <cxxtest/TestSuite.h>
#include <string>


const std::string ERR_NO_EXCEPTION = "Expected an exception but there was not any";


class TestRingList: public CxxTest::TestSuite {
public:
    void testHierarchy() {
        List<bool>* list = new RingList<bool>;
        
        delete list;
    }
    
    
    void testConstr() {
        RingList<char> list;
        
        TS_ASSERT(list.empty());
        TS_ASSERT_EQUALS(0, list.size());
    }
    
    
    void testCopyConstr() {
        RingList<char> list1;
        ListIterator<char>* it1 = new RingList<char>::Iterator(list1);
        
        list1.add('a', it1);
        it1->next();
        list1.add('b', it1);
        list1.add('-', it1);
        
        RingList<char> list2(list1);
        RingList<char>::Iterator it2(list2);
        
        TS_ASSERT_EQUALS(3, list2.size());
        TS_ASSERT_EQUALS('a', it2.get());
        it2.next();
        TS_ASSERT_EQUALS('-', it2.get());
        it2.next();
        TS_ASSERT_EQUALS('b', it2.get());
        
        it1->start();
        TS_ASSERT_EQUALS(3, list1.size());
        TS_ASSERT_EQUALS('a', it1->get());
        it1->next();
        TS_ASSERT_EQUALS('-', it1->get());
        it1->next();
        TS_ASSERT_EQUALS('b', it1->get());
        
        delete it1;
    }
    
    
    void testMoveConstr() {
        RingList<int> list1;
        RingList<int>::Iterator* it1 = new RingList<int>::Iterator(list1);
        
        list1.add(8, it1);
        it1->next();
        list1.add(32, it1);
        list1.add(-19, it1);
        
        RingList<int> list2(std::move(list1));
        RingList<int>::Iterator it2(list2);
        
        TS_ASSERT_EQUALS(3, list2.size());
        TS_ASSERT_EQUALS(8, it2.get());
        it2.next();
        TS_ASSERT_EQUALS(-19, it2.get());
        it2.next();
        TS_ASSERT_EQUALS(32, it2.get());
        
        TS_ASSERT_EQUALS(0, list1.size());
        
        delete it1;
    }
    
    
    void testAdd() {
        RingList<int> a;
        RingList<int>::Iterator it(a);
        
        a.add(1, &it);
        
        TS_ASSERT_EQUALS(1, it.get());
        
        a.add(2, &it);
        
        TS_ASSERT_EQUALS(2, it.get());
        
        it.next();
        
        TS_ASSERT_EQUALS(1, it.get());
    }
    
    
    void testAddExceptions() {
        RingList<int> a;
        RingList<int> b;
        RingList<int>::Iterator it1(a);
        RingList<int>::Iterator it2(b);
        
        try {
            a.add(1, &it2);
            TS_FAIL(ERR_NO_EXCEPTION + ": foreign iterator");
        } catch (RingListException& e) {
            TS_ASSERT_SAME_DATA(LIST_WRONG_ITERATOR, e.what(), strlen(LIST_WRONG_ITERATOR));
        }
        
        a.add(1, &it1);
        a.add(2, &it1);
        a.add(3, &it1);
        
        it1.next();
        
        a.clear();
        
        try {
            a.add(1, &it1);
            TS_FAIL(ERR_NO_EXCEPTION + ": iterator points to a deleted Block");
        } catch (RingListException& e) {
            TS_ASSERT_SAME_DATA(LIST_WRONG_ITERATOR, e.what(), strlen(LIST_WRONG_ITERATOR));
        }
        
        it1.start();
        
        TS_ASSERT_THROWS_NOTHING(a.add(1, &it1));
    }
    
    
    void testGet() {
        RingList<short> a;
        RingList<short>::Iterator it(a);
        
        a.add(7, &it);
        a.add(-3, &it);
        a.add(1, &it);
        a.add(8, &it);
        a.add(-3, &it);
        
        ListIterator<short>* it1 = a.get(1);
        ListIterator<short>* it3 = a.get(-3);
        
        it1->prev();
        TS_ASSERT_EQUALS(8, it1->get());
        it3->next();
        TS_ASSERT_EQUALS(8, it3->get());
        
        try {
            ListIterator<short>* it0 = a.get(0);
            TS_FAIL(ERR_NO_EXCEPTION);
        } catch(RingListException& e) {
            TS_ASSERT_SAME_DATA(LIST_NO_ELEMENT, e.what(), strlen(LIST_NO_ELEMENT));
        }
        
        delete it1;
        delete it3;
    }
    
    
    void testDel() {
        RingList<int> a;
        RingList<int>::Iterator it(a);
        
        a.add(1, &it);
        
        TS_ASSERT_EQUALS(1, it.get());
        
        a.add(2, &it);
        a.del(&it);
        
        TS_ASSERT_EQUALS(1, a.size());
        TS_ASSERT_EQUALS(1, it.get());
    }
    
    
    void testDelExceptions() {
        RingList<int> a;
        RingList<int> b;
        RingList<int>::Iterator it1(a);
        RingList<int>::Iterator it2(b);
        
        try {
            a.del(&it2);
            TS_FAIL(ERR_NO_EXCEPTION + ": foreign iterator");
        } catch (RingListException& e) {
            TS_ASSERT_SAME_DATA(LIST_WRONG_ITERATOR, e.what(), strlen(LIST_WRONG_ITERATOR));
        }
        
        a.add(35, &it1);
        a.add(-1, &it1);
        a.add(0, &it1);
        
        it1.next();
        
        a.clear();
        
        try {
            a.del(&it1);
            TS_FAIL(ERR_NO_EXCEPTION + ": iterator points to a deleted Block");
        } catch (RingListException& e) {
            TS_ASSERT_SAME_DATA(LIST_WRONG_ITERATOR, e.what(), strlen(LIST_WRONG_ITERATOR));
        }
        
        it1.start();
        a.add(1, &it1);
        
        TS_ASSERT_THROWS_NOTHING(a.del(&it1));
        
        try {
            b.del(&it2);
            TS_FAIL(ERR_NO_EXCEPTION + ": iterator points to the buffer Block");
        } catch (RingListException& e) {
            TS_ASSERT_SAME_DATA(LIST_DEL_BUFFER, e.what(), strlen(LIST_DEL_BUFFER));
        }
    }
    
    
    void testClear() {
        RingList<double> a;
        RingList<double>::Iterator it(a);
        
        a.add(3.325, &it);
        a.add(-0.1, &it);
        a.add(0, &it);
        a.add(0.824, &it);
        
        it.next();
        
        a.clear();
        
        TS_ASSERT(a.empty());
    }
    
    
    void testOperatorAssign1() {
        RingList<int> a;
        RingList<int> b;
        RingList<int>::Iterator it(a);
        
        a.add(2, &it);
        a.add(1, &it);
        
        a = b;
        
        TS_ASSERT(a.empty());
        TS_ASSERT(b.empty());
    }
    
    
    void testOperatorAssign2() {
        RingList<int> a;
        RingList<int> b;
        RingList<int>::Iterator it1(a);
        RingList<int>::Iterator it2(b);
        
        a.add(3, &it1);
        a.add(2, &it1);
        a.add(1, &it1);
        
        b.add(-5, &it2);
        
        a = b;
        
        TS_ASSERT_EQUALS(1, a.size());
        TS_ASSERT_EQUALS(1, b.size());
        
        it1.start();
        
        TS_ASSERT_EQUALS(-5, it1.get());
        TS_ASSERT_EQUALS(-5, it2.get());
    }
    
    
    void testOperatorAssign3() {
        RingList<int> a;
        RingList<int> b;
        RingList<int>::Iterator it1(a);
        RingList<int>::Iterator it2(b);
        
        a.add(2, &it1);
        a.add(1, &it1);
        
        b.add(-5, &it2);
        
        b = a;
        
        TS_ASSERT_EQUALS(2, a.size());
        TS_ASSERT_EQUALS(2, b.size());
        
        it1.start();
        it2.start();
        
        TS_ASSERT_EQUALS(1, it1.get());
        it1.next();
        TS_ASSERT_EQUALS(2, it1.get());
        
        TS_ASSERT_EQUALS(1, it2.get());
        it2.next();
        TS_ASSERT_EQUALS(2, it2.get());
    }
    
    
    void testOperatorAssign4() {
        RingList<int> a;
        RingList<int> b;
        RingList<int>::Iterator it1(a);
        RingList<int>::Iterator it2(b);
        
        a.add(2, &it1);
        a.add(1, &it1);
        
        b = a;
        
        TS_ASSERT_EQUALS(2, a.size());
        TS_ASSERT_EQUALS(2, b.size());
        
        it1.start();
        it2.start();
        
        TS_ASSERT_EQUALS(1, it1.get());
        it1.next();
        TS_ASSERT_EQUALS(2, it1.get());
        
        TS_ASSERT_EQUALS(1, it2.get());
        it2.next();
        TS_ASSERT_EQUALS(2, it2.get());
    }
    
    
    void testOperatorMove() {
        RingList<int> a;
        RingList<int> b;
        RingList<int>::Iterator it1(a);
        RingList<int>::Iterator it2(b);
        
        a.add(3, &it1);
        a.add(2, &it1);
        a.add(1, &it1);
        
        b.add(-2, &it2);
        
        a = std::move(b);
        
        TS_ASSERT_EQUALS(1, a.size());
        TS_ASSERT_EQUALS(3, b.size());
        
        RingList<int>::Iterator it3(a);
        RingList<int>::Iterator it4(b);
        
        TS_ASSERT_EQUALS(-2, it3.get());
        
        TS_ASSERT_EQUALS(1, it4.get());
        it4.next();
        TS_ASSERT_EQUALS(2, it4.get());
        it4.next();
        TS_ASSERT_EQUALS(3, it4.get());
    }
};


#endif
