#ifndef List_hpp
#define List_hpp

#include "list_iterator.hpp"


template <typename T>
class List {
public:
    virtual ~List() = 0;
    
    virtual void add(T, ListIterator<T>*) = 0;
    virtual ListIterator<T>* get(T) = 0;
    virtual void del(ListIterator<T>*) = 0;
    
    virtual void clear() = 0;
    
    virtual bool empty() const noexcept = 0;
    virtual unsigned long long size() const noexcept = 0;
};


template <typename T>
List<T>::~List() {
}


#endif
