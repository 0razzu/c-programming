#ifndef list_iterator_hpp
#define list_iterator_hpp

#include "list.hpp"


template <typename T>
class ListIterator {
public:
    virtual ~ListIterator() = 0;
    
    virtual void start() noexcept = 0;
    
    virtual T get() const = 0;
    
    virtual void prev() = 0;
    virtual void next() = 0;
    
    virtual bool hasPrev() const noexcept = 0;
    virtual bool hasNext() const noexcept = 0;
};


template <typename T>
ListIterator<T>::~ListIterator() {
}


#endif
