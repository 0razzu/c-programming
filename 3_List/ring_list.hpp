#ifndef RingList_hpp
#define RingList_hpp

#include "list.hpp"
#include "list_iterator.hpp"
#include "exceptions/ring_list_exception.hpp"
#include "exceptions/ring_list_iterator_exception.hpp"
#include <utility>


template <typename T>
struct Block {
    T data;
    
    Block<T>* prev;
    Block<T>* next;
    
    bool belongsToList;
    
    
    Block() {
        belongsToList = 1;
    }
    
    ~Block() {
        belongsToList = 0;
    }
};


template <typename T>
class RingList: public List<T> {
public:
    class Iterator;
    
    RingList();
    RingList(const RingList&);
    RingList(RingList&&);
    virtual ~RingList();
    
    virtual void add(T, ListIterator<T>*) override;
    virtual Iterator* get(T) override;
    virtual void del(ListIterator<T>*) override;
    
    virtual void clear() noexcept override;
    
    virtual bool empty() const noexcept override;
    virtual unsigned long long size() const noexcept override;
    
    virtual RingList& operator =(const RingList&);
    virtual RingList& operator =(RingList&&);
    
    class Iterator: public ListIterator<T> {
    public:
        Iterator(RingList&);
        
        virtual void start() noexcept override;
        
        virtual T get() const override;
        
        virtual void prev() override;
        virtual void next() override;
        
        virtual bool hasPrev() const noexcept override;
        virtual bool hasNext() const noexcept override;
        
        friend class RingList<T>;
    
    private:
        Block<T>* last_;
        Block<T>* curr_;
        
        Iterator(RingList*, Block<T>*);
    };

private:
    Block<T>* last_;
    unsigned long long size_;
};


template <typename T>
RingList<T>::RingList() {
    last_ = new Block<T>;
    last_->prev = last_;
    last_->next = last_;
    
    size_ = 0;
}


template <typename T>
RingList<T>::RingList(const RingList& o) {
    last_ = new Block<T>;
    
    Block<T>* prev;
    Block<T>* curr = last_;
    Block<T>* o_curr = o.last_->next;
    
    while (o_curr != o.last_) {
        prev = curr;
        curr = new Block<T>;
        
        curr->data = o_curr->data;
        curr->prev = prev;
        prev->next = curr;
        
        o_curr = o_curr->next;
    }
    
    curr->next = last_;
    last_->prev = curr;
    
    size_ = o.size_;
}


template <typename T>
RingList<T>::RingList(RingList&& o) {
    last_ = o.last_;
    size_ = o.size_;
    
    o.last_ = nullptr;
    o.size_ = 0;
}


template <typename T>
RingList<T>::~RingList() {
    clear();
    
    if (last_ != nullptr) {
        delete last_;
        last_ = nullptr;
    }
}


template <typename T>
void RingList<T>::add(T e, ListIterator<T>* it) {
    RingList<T>::Iterator* rlit = dynamic_cast<RingList<T>::Iterator*>(it);
    Block<T>* curr = rlit->curr_;
    
    if (rlit->last_ != last_ || !curr->belongsToList)
        throw RingListException(LIST_WRONG_ITERATOR);
    
    Block<T>* block = new Block<T>;
    block->data = e;
    
    block->next = curr;
    block->prev = curr->prev;
    
    curr->prev->next = block;
    curr->prev = block;
    
    rlit->curr_ = curr->prev;
    
    size_++;
}


template <typename T>
typename RingList<T>::Iterator* RingList<T>::get(T e) {
    Block<T>* curr = last_;
    
    while (curr->next != last_) {
        curr = curr->next;
        
        if (curr->data == e)
            return new Iterator(this, curr);
    }
    
    throw RingListException(LIST_NO_ELEMENT);
}


template <typename T>
void RingList<T>::del(ListIterator<T>* it) {
    RingList<T>::Iterator* rlit = dynamic_cast<RingList<T>::Iterator*>(it);
    Block<T>* curr = rlit->curr_;
    
    if (rlit->last_ != last_ || !curr->belongsToList)
        throw RingListException(LIST_WRONG_ITERATOR);
    
    if (curr == last_)
        throw RingListException(LIST_DEL_BUFFER);
    
    curr->prev->next = curr->next;
    curr->next->prev = curr->prev;
    
    rlit->curr_ = curr->next;
    
    delete curr;
    
    size_--;
}


template <typename T>
void RingList<T>::clear() noexcept {
    if (last_ != nullptr) {
        Block<T>* curr = last_->next;
        Block<T>* next;
        
        while (curr != last_) {
            next = curr->next;
            
            delete curr;
            
            curr = next;
        }
        
        last_->prev = last_;
        last_->next = last_;
    }
    
    size_ = 0;
}


template <typename T>
bool RingList<T>::empty() const noexcept {
    return size_ == 0;
}


template <typename T>
unsigned long long RingList<T>::size() const noexcept {
    return size_;
}


template <typename T>
RingList<T>& RingList<T>::operator =(const RingList& o) {
    Block<T>* prev;
    Block<T>* curr;
    Block<T>* o_curr = o.last_->next;
    
    if (size_ >= o.size_) {
        prev = last_;
        curr = last_->next;
        
        while (o_curr != o.last_) {
            curr->data = o_curr->data;
            
            o_curr = o_curr->next;
            prev = curr;
            curr = prev->next;
        }
        
        last_->prev = prev;
        prev->next = last_;
        
        while (curr != last_) {
            prev = curr;
            curr = prev->next;
            
            delete prev;
        }
    }
    
    else {
        curr = last_;
        
        while (curr->next != last_) {
            prev = curr;
            curr = prev->next;
            
            curr->data = o_curr->data;
            
            o_curr = o_curr->next;
        }
        
        while (o_curr != o.last_) {
            prev = curr;
            curr = new Block<T>;
            
            curr->data = o_curr->data;
            curr->prev = prev;
            prev->next = curr;
            
            o_curr = o_curr->next;
        }
        
        curr->next = last_;
        last_->prev = curr;
    }
    
    size_ = o.size_;
    
    return *this;
}


template <typename T>
RingList<T>& RingList<T>::operator =(RingList&& o) {
    std::swap(last_, o.last_);
    std::swap(size_, o.size_);
    
    return *this;
}


template <typename T>
RingList<T>::Iterator::Iterator(RingList& list) {
    last_ = list.last_;
    start();
}


template <typename T>
void RingList<T>::Iterator::start() noexcept {
    curr_ = last_->next;
}


template <typename T>
T RingList<T>::Iterator::get() const {
    if (curr_ == last_)
        throw RingListIteratorException(IT_NO_ELEMENT);
    
    return curr_->data;
}


template <typename T>
void RingList<T>::Iterator::prev() {
    if (!hasPrev())
        throw RingListIteratorException(IT_NO_ELEMENT);
    
    curr_ = curr_->prev;
}


template <typename T>
void RingList<T>::Iterator::next() {
    if (!hasNext())
        throw RingListIteratorException(IT_NO_ELEMENT);
    
    curr_ = curr_->next;
}


template <typename T>
bool RingList<T>::Iterator::hasPrev() const noexcept {
    return curr_->prev != last_;
}


template <typename T>
bool RingList<T>::Iterator::hasNext() const noexcept {
    return curr_ != last_;
}


template <typename T>
RingList<T>::Iterator::Iterator(RingList* list, Block<T>* curr) {
    last_ = list->last_;
    curr_ = curr;
}


#endif
